﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VCurrAutoBc
    {
        public string SttnId { get; set; }

        public string TrainNo { get; set; }

        public string OrigSttnId { get; set; }

        public string DestSttnId { get; set; }

        public int RunDirect { get; set; }

        public int Timing { get; set; }

        public int TrainStatus { get; set; }

        public string Platform { get; set; }

        public DateTime ArrivalTime { get; set; }

        public DateTime DepartureTime { get; set; }

        public DateTime DestTime { get; set; }

        public int DelayTime { get; set; }

        public string Stop { get; set; }

        public string CmdFrom { get; set; }
    }
}
