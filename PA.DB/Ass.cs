﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Ass
    {
        public string SttnId { get; set; }

        public string AsIdp { get; set; }

        public string IdNum { get; set; }

        public string AsIdc { get; set; }

        public decimal AsStatus { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
