﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VBcAreaPointSx
    {
        public string SttnId { get; set; }

        public string SxId { get; set; }

        public string AiId { get; set; }

        public int ChNo { get; set; }

        public string PointId { get; set; }

        public string BcAreaId { get; set; }

        public string VoiceSource { get; set; }

        public string DevId { get; set; }

        public string ZoneId { get; set; }
    }
}
