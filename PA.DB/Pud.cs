﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Pud
    {
        public string SttnId { get; set; }

        public string PuId { get; set; }

        public decimal IpV { get; set; }

        public decimal IpFV { get; set; }

        public decimal OpV { get; set; }

        public decimal OpLoad { get; set; }

        public decimal IpFrequence { get; set; }

        public decimal Battery { get; set; }

        public decimal Temperature { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
