﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VBcArea
    {
        public string PointId { get; set; }

        public string SttnId { get; set; }

        public string BcAreaSttn { get; set; }

        public string BcAreaId { get; set; }

        public string AreaCname { get; set; }

        public string AreaEname { get; set; }

        public string VoiceSource { get; set; }

        public string PlatForm { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    AreaCname : AreaEname;
            }
        }
    }
}
