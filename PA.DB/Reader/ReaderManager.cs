﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PA.DB.Reader
{
    public class ReaderManager
    {
        public delegate string ReaderHandler(IDataReader dataReader, int index);

        private Dictionary<String, ICellReader> readers;

        private Dictionary<string, ReaderHandler> readerDelegate;

        public ReaderManager()
        {
            this.readerDelegate = new Dictionary<string, ReaderHandler>();
            this.readerDelegate["character"] = FromChars;
            this.readerDelegate["character varying"] = FromChars;
            this.readerDelegate["integer"] = FromInts;
            this.readerDelegate["smallint"] = FromInts;
            this.readerDelegate["numeric"] = FromInts;

            this.readers = new Dictionary<string, ICellReader>();
            this.readers["character"] = new CharacterReader();
            this.readers["character varing"] = new CharacterReader();
            this.readers["integer"] = new IntReader();
        }

        public bool TryGet(String typeName, out ICellReader cellReader)
        {
            return this.readers.TryGetValue(typeName, out cellReader);
        }

        public ICellReader GetReader(String typeName)
        {
            ICellReader cellReader;
            if (this.readers.TryGetValue(typeName, out cellReader))
            {
                return cellReader;
            }
            else
            {

                Console.WriteLine(typeName + "missing");
                return new ObjectReader();
            }
        }

        public ReaderHandler GetHandler(String typeName)
        {
            ReaderHandler handler;
            if (this.readerDelegate.TryGetValue(typeName, out handler))
            {
                return handler;
            }
            else
            {
                //Console.WriteLine(typeName + "missing");
                return new ReaderHandler(FromObjects);
            }
        }

        private String FromChars(IDataReader dataReader, int index)
        {
            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }

            return "'" + dataReader.GetString(index) + "'";
        }

        private String FromObjects(IDataReader dataReader, int index)
        {
            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }

            return "'" + ((DateTime)dataReader[index]).ToString("yyyy-MM-dd HH:mm:ss") + "'";
            //return "'" + dataReader[index].ToString() + "'";
        }

        private String FromInts(IDataReader dataReader, int index)
        {
            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }

            return dataReader[index].ToString();
        }

    }
}
