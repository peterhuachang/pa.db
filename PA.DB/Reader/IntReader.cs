﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.Reader
{
   public class IntReader:ICellReader
    {
        public string Read(System.Data.IDataReader dataReader, int index)
        {
            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }

            return  dataReader[index].ToString();
        }
    }
}
