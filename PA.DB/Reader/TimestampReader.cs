﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PA.DB.Reader
{
    public class TimestampReader : ICellReader
    {
        public String Read(IDataReader dataReader, int index)
        {

            /**
             * Sample:
             * INSERT INTO some_table (ts_column) 
             * VALUES (to_timestamp('16-05-2011 15:36:38', 'dd-mm-yyyy hh24:mi:ss'));
             */

            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }
            DateTime dt = new DateTime();
            dt.ToString("DD-MM-YYYY HH:mm:ss");

            return "to_timestamp(" + ", 'dd-mm-yyyy hh24:mi:ss')";
        }
    }
}
