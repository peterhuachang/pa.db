﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PA.DB.Reader
{
    public class ObjectReader : ICellReader
    {
        public String Read(IDataReader dataReader, int index)
        {
            if (dataReader.IsDBNull(index))
            {
                return "NULL";
            }

            return "'" + dataReader[index].ToString() + "'";
        }
    }
}
