﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace PA.DB.Reader
{
    public interface ICellReader
    {
        String Read(IDataReader dataReader, int index);
    }
}
