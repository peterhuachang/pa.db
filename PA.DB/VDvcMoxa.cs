﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VDvcMoxa
    {
        public string SttnId { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string Labeling { get; set; }

        public string Cdesc { get; set; }

        public string Edesc { get; set; }

        public string IpAddr { get; set; }

        public string PmcGrpNum { get; set; }

        public string PmcPntNum { get; set; }

        public string LocId { get; set; }

        public string MuId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }

        public string MoxaPort { get; set; }

        public string IfType { get; set; }

        public string MoxaId { get; set; }

        public String DescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cdesc : Edesc;
            }
        }
    }
}
