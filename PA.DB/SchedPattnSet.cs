﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class SchedPattnSet
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public int Pkey { get; set; }

        public string PattnId { get; set; }

        public string VoiceSource { get; set; }

        public string DvcId { get; set; }

        public string Cname { get; set; }

        public string Ename { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string EndTimeA { get; set; }

        public int Interval { get; set; }

        public int Duration { get; set; }

        public int Repeat { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string Userstamp { get; set; }

        public DateTime? Datestamp { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cname : Ename;
            }
        }
    }
}
