﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class CurrAutoBc
    {
        public string SttnId { get; set; }

        public string TrainNo { get; set; }

        public string Platform { get; set; }

        public int Timing { get; set; }

        public int TrainStatus { get; set; }

        public string CmdFrom { get; set; }
    }
}
