﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Adsw
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string AdswId { get; set; }

        public int ChannelId { get; set; }

        public string Type { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime DateStamp { get; set; }
        public string Userstamp { get; set; }

        public DateTime? Datestamp { get; set; }
    }
}
