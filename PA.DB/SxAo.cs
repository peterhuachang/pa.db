﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class SxAo
    {
        public string SttnId { get; set; }

        public string SxId { get; set; }

        public string AoId { get; set; }

        public int ChNo { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
