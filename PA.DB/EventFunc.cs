﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class EventFunc
    {
        public string SttnId { get; set; }

        public string EventName { get; set; }

        public int EventStep { get; set; }

        public string FuncName { get; set; }

        public string GoNext { get; set; }
    }
}
