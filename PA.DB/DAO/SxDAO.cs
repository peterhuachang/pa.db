﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class SxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ai_ci ";

        private NpgsqlConnection conn;

        public SxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Sx SearchPK(string sttnId, string sxId)
        {
            Sx value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<Sx> SearchSttn(string sttnId)
        {
            List<Sx> values = new List<Sx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Sx Convert(NpgsqlDataReader reader)
        {
            Sx value = new Sx();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.CrUser = reader.GetString(2);
            value.CrDate = reader.GetDateTime(3);
            value.UserStamp = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.DateStamp = reader.IsDBNull(5) ? (DateTime?)null : reader.GetDateTime(5);
            return value;
        }
    }
}
