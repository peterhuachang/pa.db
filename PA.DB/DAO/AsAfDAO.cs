﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AsAfDAO
    {
        internal const string SQL_SELECT = "select sttn_id,as_id,seq,af_id,cr_user,cr_date,userstamp,datestamp from pa_tb_as_af";

        internal const string SQL_INSERT = "insert into pa_tb_as_af(sttn_id,as_id,seq,af_id,cr_user,cr_date,userstamp,datestamp)values(:sttn_id,:as_id,:seq,:af_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_as_af set af_id=:af_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_as_af ";

        private NpgsqlConnection conn;

        public AsAfDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(AsAf value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("Sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("AS_id", value.AsId);
            cmd.Parameters.AddWithValue("SEQ", value.Seq);
            cmd.Parameters.AddWithValue("AF_id", value.AfId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("CR_DATE", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_as_af_id_SEQ')";
            // value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(AsAf value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and as_id=:as_id and seq=:seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("as_id", value.AsId);
            cmd.Parameters.AddWithValue("seq", value.Seq);
            cmd.Parameters.AddWithValue("af_id", value.AfId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            //  cmd.Parameters.AddWithValue("id", value.id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(AsAf value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and as_id=:as_id and seq=:seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("as_id", value.AsId);
            cmd.Parameters.AddWithValue("seq", value.Seq);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string asId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and as_id=:as_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string asId, int seq)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and as_id=:as_id and seq=:seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            cmd.Parameters.AddWithValue("seq", seq);
            cmd.ExecuteNonQuery();
        }

        public AsAf SearchPK(string sttnId, string asId, int seq)
        {
            AsAf value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_id=:as_id and seq=:seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            cmd.Parameters.AddWithValue("seq", seq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public AsAf SearchByAfId(string sttnId, string afId)
        {
            AsAf value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and af_id=:af_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("af_id", afId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AsAf> SearchSttn(string sttnId)
        {
            List<AsAf> values = new List<AsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AsAf> Search(string sttnId,string asId)
        {
            List<AsAf> values = new List<AsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_id=:as_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AsAf Convert(NpgsqlDataReader reader)
        {
            AsAf value = new AsAf();
            value.SttnId = reader.GetString(0);
            value.AsId = reader.GetString(1);
            value.Seq = reader.GetDecimal(2);
            value.AfId = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
