﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VBcAreaPointScdDAO
    {
        internal const string SQL_SELECT = "select sttn_id, bc_area_id, sc_id, bit_id, voice_source, dev_id from pa_v_bc_area_point_scd";

        private NpgsqlConnection conn;

        public VBcAreaPointScdDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VBcAreaPointScd SearchPK(string sttnId, string bcAreaId, string scId, int bitId, string voiceSource, string devId)
        {
            VBcAreaPointScd value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id and sc_id=:sc_id and bit_id=:bit_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            cmd.Parameters.AddWithValue("bit_id", bitId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VBcAreaPointScd> SearchBySttn(string sttnId)
        {
            List<VBcAreaPointScd> values = new List<VBcAreaPointScd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointScd> SearchByAreaId(string sttnId, string bcAreaId)
        {
            List<VBcAreaPointScd> values = new List<VBcAreaPointScd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointScd> SearchByScId(string sttnId, string scId)
        {
            List<VBcAreaPointScd> values = new List<VBcAreaPointScd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sc_id=:sc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointScd> SearchByVoiceSourceDvcId(string sttnId, string voiceSource, string devId)
        {
            List<VBcAreaPointScd> values = new List<VBcAreaPointScd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VBcAreaPointScd Convert(NpgsqlDataReader reader)
        {
            VBcAreaPointScd value = new VBcAreaPointScd();          
            value.SttnId = reader.GetString(0);
            value.BcAreaId = reader.GetString(1);
            value.ScId = reader.GetString(2);
            value.BitId = reader.GetInt32(3);
            value.VoiceSource = reader.GetString(4);
            value.DevId = reader.GetString(5);            
            return value;
        }
    }
}
