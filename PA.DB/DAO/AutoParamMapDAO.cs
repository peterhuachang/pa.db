﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoParamMapDAO
    {
        internal const string SQL_SELECT = "select context_param,phr_param from pa_tb_auto_param_map";

        private NpgsqlConnection conn;

        public AutoParamMapDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public AutoParamMap SearchPK(string contextParam, string phraseParam)
        {
            AutoParamMap value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where context_param=:context_param and phr_param=:phr_param";
            cmd.Parameters.AddWithValue("context_param", contextParam);
            cmd.Parameters.AddWithValue("phr_param", phraseParam);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public AutoParamMap SearchPhrParam(string contextParam)
        {
            AutoParamMap value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where context_param=:context_param";
            cmd.Parameters.AddWithValue("context_param", contextParam);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AutoParamMap> SearchContextParam(string phraseParam)
        {
            List<AutoParamMap> values = new List<AutoParamMap>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where phr_param=:phr_param";
            cmd.Parameters.AddWithValue("phr_param", phraseParam);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoParamMap> SearchAll()
        {
            List<AutoParamMap> values = new List<AutoParamMap>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoParamMap Convert(NpgsqlDataReader reader)
        {
            AutoParamMap value = new AutoParamMap();
            value.ContextParam = reader.GetString(0);
            value.PhrParam = reader.GetString(1);           
            return value;
        }
    }
}
