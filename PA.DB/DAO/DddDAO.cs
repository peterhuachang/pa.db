﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DddDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dd_id,sttn_id_t,dd_id_t,cnn_name,cr_user,cr_date,userstamp,datestamp from pa_tb_ddd";

        private NpgsqlConnection conn;

        public DddDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Ddd SearchPK(string sttnId, string ddId)
        {
            Ddd value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dd_id=:dd_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dd_id", ddId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Ddd> SearchSttn(string sttnId)
        {
            List<Ddd> values = new List<Ddd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Ddd Convert(NpgsqlDataReader reader)
        {
            Ddd value = new Ddd();
            value.SttnId = reader.GetString(0);
            value.DdId = reader.GetString(1);
            value.SttnIdT = reader.GetString(2);
            value.DdIdT = reader.GetString(3);
            value.CnnName = reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? null : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
