﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace PA.DB.DAO
{
    public class VSchedPattnSetDAO
    {
        internal const string SQL_SELECT = @"select p_key, sched_date, start_time, end_time, 
                                             voice_source, dvc_id, interval, duration, repeat, 
                                             sttn_id from pa_v_sched_pattn_set";

        private NpgsqlConnection conn;

        public VSchedPattnSetDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VSchedPattnSet Search(int pKey)
        {
            VSchedPattnSet value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where p_key=:p_key";
            cmd.Parameters.AddWithValue("p_key", pKey);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<VSchedPattnSet> Search(string sttnId)
        {
            List<VSchedPattnSet> values = new List<VSchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }

            reader.Close();
            return values;                        
        }

        public IList<VSchedPattnSet> SearchByStartTime(string sttnId, DateTime schedDate)
        {
            List<VSchedPattnSet> values = new List<VSchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + @" where sttn_id=:sttn_id and sched_date=:sched_date 
                                              and start_time >= '03:00:00' and start_time < '24:00:00' ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sched_date", schedDate);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }

            reader.Close();
            return values;            
        }

        public IList<VSchedPattnSet> SearchByEndTime(string sttnId, DateTime schedDate)
        {
            List<VSchedPattnSet> values = new List<VSchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + @" where sttn_id=:sttn_id and sched_date=:sched_date 
                                              and end_time > '00:00:00' and end_time <= '03:00:00' ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sched_date", schedDate);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }

            reader.Close();
            return values;                        
        }

        internal static VSchedPattnSet Convert(NpgsqlDataReader reader)
        {
            VSchedPattnSet value = new VSchedPattnSet();
            value.PKey = reader.GetDecimal(0);
            value.SchedDate = reader.GetDateTime(1);
            value.StartTime = reader.GetString(2);
            value.EndTime = reader.GetString(3);
            value.VoiceSource = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.Interval = reader.GetInt32(6);
            value.Duration = reader.GetInt32(7);
            value.Repeat = reader.GetInt32(8);
            value.SttnId = reader.GetString(9);
            return value;
        }
    }
}
