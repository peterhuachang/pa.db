﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PaCidxHistDAO
    {
        internal const string SQL_SELECT = "select update_seq,sql_text from pa_tb_pa_cidx_hist";
        internal const string SQL_INSERT = @"insert into pa_tb_pa_cidx_hist (update_seq,sql_text) 
                                            values(nextval('pa_tb_cidx_hist_update_seq_seq'),:sql_text)";
        internal const string SQL_DELETE = "delete from pa_tb_pa_cidx_hist";

        private NpgsqlConnection conn;

        public PaCidxHistDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(PaCidxHist value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sql_text", value.SqlText);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_cidx_hist_update_seq_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Delete(PaCidxHist value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where update_seq=:update_seq";
            cmd.Parameters.AddWithValue("update_seq", value.UpdateSeq);
            cmd.ExecuteNonQuery();
        }

        public void DeleteAll()
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE;            
            cmd.ExecuteNonQuery();
        }

        public PaCidxHist SearchPK(int updateSeq)
        {
            PaCidxHist value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where update_seq=:update_seq";
            cmd.Parameters.AddWithValue("update_seq", updateSeq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<PaCidxHist> SearchAll()
        {
            List<PaCidxHist> values = new List<PaCidxHist>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static PaCidxHist Convert(NpgsqlDataReader reader)
        {
            PaCidxHist value = new PaCidxHist();
            value.UpdateSeq = reader.GetInt32(0);
            value.SqlText = reader.GetString(1);
            return value;
        }
    }
}
