﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class EventFuncDAO
    {
        internal const string SQL_SELECT = "select sttn_id,event_name,event_step,func_name,go_next from pa_tb_event_func";

        private NpgsqlConnection conn;

        public EventFuncDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public EventFunc SearchPK(string sttnId, string eventName, int eventStep)
        {
            EventFunc value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and event_name=:event_name and event_step=:event_step";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("event_name", eventName);
            cmd.Parameters.AddWithValue("event_step", eventStep);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<EventFunc> SearchByVoiceSource(string sttnId, string voiceSource)
        {
            List<EventFunc> values = new List<EventFunc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and (event_name like :voice_source or event_name like 'General%') order by event_name, event_step";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource + "%");
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<EventFunc> SearchSttn(string sttnId)
        {
            List<EventFunc> values = new List<EventFunc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static EventFunc Convert(NpgsqlDataReader reader)
        {
            EventFunc value = new EventFunc();
            value.SttnId = reader.GetString(0);
            value.EventName = reader.GetString(1);
            value.EventStep = reader.GetInt16(2);
            value.FuncName = reader.GetString(3);
            value.GoNext = reader.GetString(4);
            return value;
        }
    }
}
