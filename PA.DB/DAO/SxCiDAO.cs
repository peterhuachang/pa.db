﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace PA.DB.DAO
{
    public class SxCiDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ao_id,ci_id,ci_no,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ci";

        private NpgsqlConnection conn;

        public SxCiDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<SxCi> Search()
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public SxCi SearchPK(string sttnId, string sxId, string aoId, string ciId, int ciNo)
        {
            SxCi value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ci_id=:ci_id and ci_no=:ci_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ci_id", ciId);
            cmd.Parameters.AddWithValue("ci_no", ciNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SxCi> SearchSttn(string sttnId)
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxCi> SearchSxId(string sttnId, string sxId)
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxCi> SearchAoId(string sttnId, string sxId, string aoId)
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxCi> SearchCiId(string sttnId, string sxId, string aoId, string ciId)
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ci_id=:ci_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ci_id", ciId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxCi> SearchDvcType(string sttnId, string sxId, string aoId, string ciId, string dvcType)
        {
            List<SxCi> values = new List<SxCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ci_id=:ci_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ci_id", ciId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SxCi Convert(NpgsqlDataReader reader)
        {
            SxCi value = new SxCi();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AoId = reader.GetString(2);
            value.CiId = reader.GetString(3);
            value.CiNo = reader.GetInt32(4);
            value.DvcType = reader.GetString(5);
            value.DvcId = reader.GetString(6); 
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
