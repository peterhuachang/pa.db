﻿using Npgsql;
using PA.DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PaCidxExtDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,idx_id,card_id,lang from pa_tb_pa_cidx_ext";

        internal const string SQL_INSERT = "insert into pa_tb_pa_cidx_ext(sttn_id,pa_id,idx_id,card_id,lang)values (:sttn_id,:pa_id,:idx_id,:card_id,:lang)";

        internal const string SQL_UPDATE = "update pa_tb_pa_cidx_ext set lang=:lang where sttn_id=:sttn_id AND pa_id=:pa_id AND idx_id=:idx_id";

        internal const string SQL_DELETE = "delete from pa_tb_pa_cidx_ext";

        private NpgsqlConnection conn;

        public PaCidxExtDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(PaCidxExt value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("card_id", value.CardId);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.ExecuteNonQuery();
        }

        public void Update(PaCidxExt value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(PaCidxExt value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and idx_id=:idx_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.ExecuteNonQuery();
        }

        public PaCidxExt SearchPK(string sttnId, string paId, string idxId)
        {
            PaCidxExt value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and idx_id=:idx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("idx_id", idxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<PaCidxExt> SearchBySttnId(string sttnId)
        {
            List<PaCidxExt> values = new List<PaCidxExt>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<PaCidxExt> SearchByCardId(string sttnId, string paId, string cardId)
        {
            List<PaCidxExt> values = new List<PaCidxExt>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and card_id=:card_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("card_id", cardId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static PaCidxExt Convert(NpgsqlDataReader reader)
        {
            PaCidxExt value = new PaCidxExt();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.IdxId = reader.GetString(2);
            value.CardId = reader.GetString(3);
            value.Lang = reader.GetString(4);
            return value;
        }
    }
}


/**
 *-- Function: after_delete_pa_tb_pa_cidx()

-- DROP FUNCTION after_delete_pa_tb_pa_cidx();

CREATE OR REPLACE FUNCTION after_delete_pa_tb_pa_cidx()
  RETURNS trigger AS
$BODY$
    DECLARE   
        v_pattn record;  
    BEGIN
        IF (TG_OP = 'DELETE') THEN
				
		FOR v_pattn IN 	SELECT a.STTN_ID, b.PATTN_ID
				FROM    PA_TB_SENTENCE a, PA_TB_SENTENCE_PHR b
				WHERE a.STTN_ID = b.STTN_ID
				AND   a.STTN_ID = OLD.STTN_ID
				AND   b.IDX_ID = OLD.IDX_ID 
				AND   a.PA_ID = OLD.PA_ID
				AND   a.SENTEN_ID = b.SENTEN_ID LOOP
			IF FOUND THEN 			
				DELETE FROM PA_TB_SCHED_PATTN_SET a		
				WHERE a.STTN_ID  = v_pattn.STTN_ID
				AND   a.SENTEN_ID = v_pattn.SENTEN_ID;
			END IF;
		END LOOP;
		
        END IF;
        RETURN NULL; 
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION after_delete_pa_tb_pa_cidx()
  OWNER TO pa;

 */

