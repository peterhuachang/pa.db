﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VBcPointDAO
    {
        internal const string SQL_SELECT = "select sttn_id,bc_area_id,point_id,zone_id,area_cname,area_ename,voice_source,platform,zone_cname,zone_ename,zone_bc_status from pa_v_bc_point";

        private NpgsqlConnection conn;

        public VBcPointDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VBcPoint SearchPK(string sttnId, string bcAreaId, string pointId, string zoneId)
        {
            VBcPoint value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id and point_id=:point_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VBcPoint> Search(string sttnId)
        {
            List<VBcPoint> values = new List<VBcPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcPoint> Search(string sttnId, string bcAreaId)
        {
            List<VBcPoint> values = new List<VBcPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select distinct on (zone_id) sttn_id,bc_area_id,point_id,zone_id,area_cname,area_ename,voice_source,platform,zone_cname,zone_ename,zone_bc_status from pa_v_bc_point where sttn_id=:sttn_id and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcPoint> SearchByVoiceSource(string sttnId, string voiceSource)
        {
            List<VBcPoint> values = new List<VBcPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select distinct on (bc_area_id) sttn_id,bc_area_id,point_id,zone_id,area_cname,area_ename,voice_source,platform,zone_cname,zone_ename,zone_bc_status from pa_v_bc_point WHERE sttn_id=:sttn_id and voice_source=:voice_source ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcPoint> SearchAreaZone(string sttnId, string bcAreaId, string pointId)
        {
            List<VBcPoint> values = new List<VBcPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id and point_id=:point_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VBcPoint Convert(NpgsqlDataReader reader)
        {
            VBcPoint value = new VBcPoint();
            value.SttnId = reader.GetString(0);
            value.BcAreaId = reader.GetString(1);
            value.PointId = reader.GetString(2);
            value.ZoneId = reader.GetString(3);
            value.AreaCname = reader.GetString(4);
            value.AreaEname = reader.GetString(5);
            value.VoiceSource = reader.GetString(6);
            value.PlatForm = reader.GetString(7);
            value.ZoneCname = reader.GetString(8);
            value.ZoneEname = reader.GetString(9);
            value.ZoneBcStatus = reader.GetInt32(10);
            return value;
        }
    }
}
