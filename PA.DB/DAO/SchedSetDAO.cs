﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SchedSetDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,sched_date,pattn_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sched_set";

        internal const string SQL_INSERT = "insert into pa_tb_sched_set(id,sttn_id,sched_date,pattn_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_sched_set_id_seq'),:sttn_id,:sched_date,:pattn_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_sched_set set sttn_id=:sttn_id,sched_date=:sched_date,pattn_id=:pattn_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_sched_set";

        private NpgsqlConnection conn;

        public SchedSetDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(SchedSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("sched_date", value.SchedDate);
            cmd.Parameters.AddWithValue("pattn_id", value.PattnId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_sched_set_id_seq')";
            value.Id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(SchedSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("sched_date", value.SchedDate);
            cmd.Parameters.AddWithValue("pattn_id", value.PattnId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(SchedSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and sched_date=:sched_date";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("sched_date", value.SchedDate);
            cmd.ExecuteNonQuery();
        }

        public SchedSet SearchById(int id)
        {
            SchedSet value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public SchedSet Search(string sttnId, DateTime scheduleDate)
        {
            SchedSet value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sched_date=:sched_Date";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sched_Date", scheduleDate);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<SchedSet> SearchAll()
        {
            List<SchedSet> values = new List<SchedSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SchedSet Convert(NpgsqlDataReader reader)
        {
            SchedSet value = new SchedSet();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.SchedDate = reader.GetDateTime(2);
            value.PattnId = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
