﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SchedBcAreaDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,p_key,bc_area_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sched_bc_area";

        private NpgsqlConnection conn;

        public SchedBcAreaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SchedBcArea SearchById(int id)
        {
            SchedBcArea value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SchedBcArea> SearchAll()
        {
            List<SchedBcArea> values = new List<SchedBcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SchedBcArea> SearchBySttnPKey(string sttnId, int pKey)
        {
            List<SchedBcArea> values = new List<SchedBcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and p_key=:p_key";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("p_key", pKey);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SchedBcArea Convert(NpgsqlDataReader reader)
        {
            SchedBcArea value = new SchedBcArea();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.Pkey = reader.GetDecimal(2);
            value.BcAreaId = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.Userstamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.Datestamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
