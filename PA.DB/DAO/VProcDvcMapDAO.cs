﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace PA.DB.DAO
{
    public class VProcDvcMapDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id, exe_sttn,proc_name,que_name,dvc_type,dvc_id,labeling,cdesc,edesc,ip_addr from pa_v_proc_dvc_map";

        private NpgsqlConnection conn;

        public VProcDvcMapDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VProcDvcMap Search(int id)
        {
            VProcDvcMap value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<VProcDvcMap> Search(string exeSttnId)
        {
            List<VProcDvcMap> values = new List<VProcDvcMap>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VProcDvcMap> Search(string exeSttnId, string sttnId)
        {
            List<VProcDvcMap> values = new List<VProcDvcMap>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VProcDvcMap> SearchByDvcType(string sttnId, string dvcType)
        {
            List<VProcDvcMap> values = new List<VProcDvcMap>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";            
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public VProcDvcMap Search(string sttnId, string dvcType, string dvcId)
        {
            VProcDvcMap value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public VProcDvcMap SearchByProcessName(string procName)
        {
            VProcDvcMap value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where proc_name=:proc_name";
            cmd.Parameters.AddWithValue("proc_name", procName);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        internal static VProcDvcMap Convert(NpgsqlDataReader reader)
        {
            VProcDvcMap value = new VProcDvcMap();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.ExeSttn = reader.GetString(2);
            value.ProcName = reader.GetString(3);
            value.QueName = reader.GetString(4);
            value.DvcType = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DvcId = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.Labeling = reader.GetString(7);
            value.Cdesc = reader.GetString(8);
            value.Edesc = reader.GetString(9);
            value.IpAddr = reader.IsDBNull(10) ? "" : reader.GetString(10);
            return value;
        }
    }
}
