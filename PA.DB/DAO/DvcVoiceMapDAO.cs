﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class DvcVoiceMapDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,voice_source,dev_id,cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_voice_map";

        private NpgsqlConnection conn;

        public DvcVoiceMapDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public DvcVoiceMap SearchPK(string sttnId, string dvcType, string dvcId)
        {
            DvcVoiceMap value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<DvcVoiceMap> Search(string sttnId)
        {
            List<DvcVoiceMap> values = new List<DvcVoiceMap>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcVoiceMap> SearchByDvc(string sttnId, string dvcType)
        {
            List<DvcVoiceMap> values = new List<DvcVoiceMap>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public DvcVoiceMap SearchByDvc(string sttnId, string dvcType, string dvcId)
        {
            DvcVoiceMap value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<DvcVoiceMap> SearchByVoice(string sttnId, string voiceSource)
        {
            List<DvcVoiceMap> values = new List<DvcVoiceMap>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public DvcVoiceMap SearchByVoice(string sttnId, string voiceSource, string devId)
        {
            DvcVoiceMap value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        internal static DvcVoiceMap Convert(NpgsqlDataReader reader)
        {
            DvcVoiceMap value = new DvcVoiceMap();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.VoiceSource = reader.GetString(3);
            value.DevId = reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
