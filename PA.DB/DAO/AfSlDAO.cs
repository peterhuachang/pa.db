﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AfSlDAO
    {
        internal const string SQL_SELECT = "select sttn_id,af_id,sl_id,bk_af_id, cr_user,cr_date,userstamp,datestamp from pa_tb_af_sl";        

        private NpgsqlConnection conn;

        public AfSlDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public AfSl SearchPK(string sttnId, string afId, string slId)
        {
            AfSl value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and af_id=:af_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("af_id", afId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public AfSl SearchByAfId(string sttnId, string afId)
        {
            AfSl value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and af_id=:af_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("af_id", afId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AfSl> SearchSttn(string sttnId)
        {
            List<AfSl> values = new List<AfSl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AfSl> SearchBySlId(string sttnId, string slId)
        {
            List<AfSl> values = new List<AfSl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AfSl Convert(NpgsqlDataReader reader)
        {
            AfSl value = new AfSl();
            value.SttnId = reader.GetString(0);
            value.AfId = reader.GetString(1);
            value.SlId = reader.GetString(2);
            value.BkAfId = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
