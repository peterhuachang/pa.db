﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DvcTypeDAO
    {
        internal const string SQL_SELECT = "select dvc_type,cdesc,edesc,cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_type";

        private NpgsqlConnection conn;

        public DvcTypeDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public DvcType SearchPK(string dvcType)
        {
            DvcType value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<DvcType> SearchAll()
        {
            List<DvcType> values = new List<DvcType>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static DvcType Convert(NpgsqlDataReader reader)
        {
            DvcType value = new DvcType();
            value.TypeName = reader.GetString(0);
            value.Cdesc = reader.GetString(1);
            value.Edesc = reader.GetString(2);
            value.CrUser = reader.GetString(3);
            value.CrDate = reader.GetDateTime(4);
            value.UserStamp = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DateStamp = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
            return value;
        }
    }
}
