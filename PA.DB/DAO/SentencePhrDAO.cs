﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SentencePhrDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,senten_id,seq,idx_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sentence_phr";

        internal const string SQL_INSERT = "insert into pa_tb_sentence_phr(id,sttn_id,senten_id,seq,idx_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_sentence_phr_id_seq'),:sttn_id,:senten_id,:seq,:idx_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_sentence_phr set sttn_id=:sttn_id,senten_id=:senten_id,seq=:seq,idx_id=:idx_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_sentence_phr";

        private NpgsqlConnection conn;

        public SentencePhrDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(SentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("seq", value.Seq);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_sentence_phr_id_seq')";
            //value.Id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(SentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("seq", value.Seq);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(SentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id";
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public SentencePhr SearchById(int id)
        {
            SentencePhr value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<SentencePhr> SearchSentence(string sttnId, string sentenId)
        {
            List<SentencePhr> values = new List<SentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id AND senten_id=:senten_id" + " order by id ASC";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("senten_id", sentenId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SentencePhr> SearchAll()
        {
            List<SentencePhr> values = new List<SentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " order by id ASC";
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SentencePhr Convert(NpgsqlDataReader reader)
        {
            SentencePhr value = new SentencePhr();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.SentenId = reader.GetString(2);
            value.Seq = reader.GetInt32(3);
            value.IdxId = reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.Userstamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.Datestamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
