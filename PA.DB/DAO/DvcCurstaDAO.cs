﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DvcCurstaDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,alarm_id,alarm_flag,alarm_time,cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_cursta";
        internal const string SQL_UPDATE = "update pa_tb_dvc_cursta set alarm_flag=:alarm_flag,alarm_time=:alarm_time,userstamp=:userstamp,datestamp=:datestamp";       

        private NpgsqlConnection conn;

        public DvcCurstaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<DvcCursta> Search()
        {
            List<DvcCursta> values = new List<DvcCursta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

      //  public DvcCursta SearchPK(string sttnId, string dvcType, string dvcId, string alarmId);
        public DvcCursta SearchByPK(string sttnId, string dvcType, string dvcId, string alarmId)
        {
            DvcCursta value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id and alarm_id=:alarm_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            cmd.Parameters.AddWithValue("alarm_id", alarmId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<DvcCursta> Search(string sttnId, string dvcType)
        {
            List<DvcCursta> values = new List<DvcCursta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type order by dvc_id, alarm_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcCursta> Search(string sttnId, string dvcType, int minDvcId, int maxDvcId)
        {
            List<DvcCursta> values = new List<DvcCursta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + @" where sttn_id=:sttn_id and 
                                              dvc_type=:dvc_type and 
                                              cast(dvc_id as int)>=:min_dvc_id 
                                              and cast(dvc_id as int)<=:max_dvc_id 
                                              order by dvc_id, alarm_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("min_dvc_id", minDvcId);
            cmd.Parameters.AddWithValue("max_dvc_id", maxDvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcCursta> SearchSttn(string sttnId)
        {
            List<DvcCursta> values = new List<DvcCursta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public bool Update(DvcCursta value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id and alarm_id=:alarm_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("dvc_type", value.DvcType);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("alarm_id", value.AlarmId);
            cmd.Parameters.AddWithValue("alarm_flag", value.AlarmFlag);
            cmd.Parameters.AddWithValue("alarm_time", value.AlarmTime);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public bool UpdateByDvcType(string sttnId, string dvcType, int alarmFlag, DateTime alarmTime, string userstamp)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("alarm_flag", alarmFlag);
            cmd.Parameters.AddWithValue("alarm_time", alarmTime);
            cmd.Parameters.AddWithValue("userstamp", userstamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public bool UpdateAll(string sttnId, int alarmFlag, DateTime alarmTime, string userstamp)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);            
            cmd.Parameters.AddWithValue("alarm_flag", alarmFlag);
            cmd.Parameters.AddWithValue("alarm_time", alarmTime);
            cmd.Parameters.AddWithValue("userstamp", userstamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static DvcCursta Convert(NpgsqlDataReader reader)
        {
            DvcCursta value = new DvcCursta();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.AlarmId = reader.GetString(3);
            value.AlarmFlag = reader.GetInt32(4);
            value.AlarmTime = reader.GetDateTime(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
