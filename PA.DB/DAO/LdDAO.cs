﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace PA.DB.DAO
{
    public class LdDAO
    {
        internal const string SQL_SELECT = "select sttn_id,ld_id,sl_id,ch_no,cr_user,cr_date,userstamp,datestamp from pa_tb_ld";

        private NpgsqlConnection conn;

        public LdDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<Ld> Search()
        {
            List<Ld> values = new List<Ld>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Ld SearchPK(string sttnId, string ldId, int chNo)
        {
            Ld value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and ld_id=:ld_id and ch_no=:ch_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("ld_id", ldId);
            cmd.Parameters.AddWithValue("ch_no", chNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Ld> SearchSttn(string sttnId)
        {
            List<Ld> values = new List<Ld>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Ld> SearchLdId(string sttnId, string ldId)
        {
            List<Ld> values = new List<Ld>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and ld_id=:ld_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("ld_id", ldId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Ld> SearchSlId(string sttnId, string slId)
        {
            List<Ld> values = new List<Ld>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Ld Convert(NpgsqlDataReader reader)
        {
            Ld value = new Ld();
            value.SttnId = reader.GetString(0);
            value.LdId = reader.GetString(1);
            value.SlId = reader.GetString(2);
            value.ChNo = reader.GetInt32(3);            
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
