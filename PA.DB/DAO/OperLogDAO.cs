﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class OperLogDAO
    {
        internal const string SQL_SELECT = "select id,datestamp,type,sttn_id,context,log_language from pa_tb_oper_log";
        internal const string SQL_INSERT = @"insert into pa_tb_oper_log (id,log_language,datestamp,type,sttn_id,context) 
                                            values(nextval('pa_tb_oper_log_id_seq'),:log_language,:datestamp,:type,:sttn_id,:context)";

        private NpgsqlConnection conn;

        public OperLogDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public OperLog SearchPK(int id)
        {
            OperLog value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<OperLog> SearchAll(String sttnId)
        {
            List<OperLog> values = new List<OperLog>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<OperLog> SearchLog(string sttnId, string type, string logLanguage)
        {
            List<OperLog> values = new List<OperLog>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and type=:type and log_language=:log_language";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("type", type);
            cmd.Parameters.AddWithValue("log_language", logLanguage);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<OperLog> SearchLog(string sttnId, string type)
        {
            List<OperLog> values = new List<OperLog>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and type=:type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("type", type);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public void Insert(OperLog value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("log_language", value.LogLanguage);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("type", value.Type);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("context", value.Context);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_oper_log_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        internal static OperLog Convert(NpgsqlDataReader reader)
        {
            OperLog value = new OperLog();
            value.Id = reader.GetInt32(0);
            value.DateStamp = reader.GetDateTime(1);
            value.Type = reader.GetString(2);
            value.SttnId = reader.IsDBNull(3) ? "" : reader.GetString(3);
            value.Context = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.LogLanguage = reader.IsDBNull(5) ? "" : reader.GetString(5);
            return value;
        }
    }
}
