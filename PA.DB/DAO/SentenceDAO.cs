﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SentenceDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,senten_id,pa_id,class_id,cname,ename,senten_len,cr_user,cr_date,userstamp,datestamp from pa_tb_sentence";

        internal const string SQL_INSERT = "insert into pa_tb_sentence(id,sttn_id,senten_id,pa_id,class_id,cname,ename,senten_len,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_sentence_id_seq'),:sttn_id,:senten_id,:pa_id,:class_id,:cname,:ename,:senten_len,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_sentence set sttn_id=:sttn_id,senten_id=:senten_id,pa_id=:pa_id,class_id=:class_id,cname=:cname,ename=:ename,senten_len=:senten_len,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_sentence";

        private NpgsqlConnection conn;

        public SentenceDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(Sentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("class_id", value.ClassId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("senten_len", value.SentenLen);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_sentence_id_seq')";
            //value.Id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(Sentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("class_id", value.ClassId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("senten_len", value.SentenLen);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(Sentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id";
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public Sentence SearchBySentenceId(string sentenId)
        {
            Sentence value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where senten_id=:senten_id";
            cmd.Parameters.AddWithValue("senten_id", sentenId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public Sentence SearchSentence(string sttnId,string sentenId,string paId)
        {
            Sentence value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and senten_id=:senten_id and pa_id=:pa_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("senten_id", sentenId);
            cmd.Parameters.AddWithValue("pa_id", paId);

            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public List<Sentence> SearchBySttn(string sttnId)
        {
            List<Sentence> values = new List<Sentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Sentence> Search(string sttnId, string classId)
        {
            List<Sentence> values = new List<Sentence>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and class_id=:class_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("class_id", classId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Sentence> Search(string sttnId, string paId, string classId)
        {
            List<Sentence> values = new List<Sentence>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and class_id=:class_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("class_id", classId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Sentence> SearchAll()
        {
            List<Sentence> values = new List<Sentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Sentence Convert(NpgsqlDataReader reader)
        {
            Sentence value = new Sentence();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.SentenId = reader.GetString(2);
            value.PaId = reader.GetString(3);
            value.ClassId = reader.GetString(4);
            value.Cname = reader.GetString(5);
            value.Ename = reader.GetString(6);
            value.SentenLen = reader.GetInt32(7);
            value.CrUser = reader.GetString(8);
            value.CrDate = reader.GetDateTime(9);
            value.UserStamp = reader.IsDBNull(10) ? "" : reader.GetString(10);
            value.DateStamp = reader.IsDBNull(11) ? (DateTime?)null : reader.GetDateTime(11);
            return value;
        }
    }
}
