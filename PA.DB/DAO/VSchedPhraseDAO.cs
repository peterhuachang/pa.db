﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VSchedPhraseDAO
    {
        internal const string SQL_SELECT = "select sttn_id, p_key, idx_id, time_len, senten_id, sched_date from pa_v_sched_phrase ";

        private NpgsqlConnection conn;

        public VSchedPhraseDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<VSchedPhrase> SearchBySttn(string sttnId)
        {
            List<VSchedPhrase> values = new List<VSchedPhrase>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VSchedPhrase> SearchBySchedDate(string sttnId, DateTime schedDate)
        {
            List<VSchedPhrase> values = new List<VSchedPhrase>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sched_date=:sched_date";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sched_date", schedDate);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VSchedPhrase Convert(NpgsqlDataReader reader)
        {
            VSchedPhrase value = new VSchedPhrase();
            value.SttnId = reader.GetString(0);
            value.PKey = reader.GetDecimal(1);
            value.IdxId = reader.GetString(2);
            value.TimeLen= reader.GetInt32(3);
            value.SentenId = reader.GetString(4);
            value.SchedDate = reader.GetDateTime(5);
            return value;
        }
    }
}
