﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VBcAreaPointSxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ai_id,ch_no,point_id,bc_area_id,voice_source,dev_id,zone_id from pa_v_bc_area_point_sx";

        private NpgsqlConnection conn;

        public VBcAreaPointSxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<VBcAreaPointSx> SearchBySttn(string sttnId)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointSx> SearchByAiId(string sttnId, string sxId, string aiId)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointSx> SearchByAiId(string sttnId, string sxId, string aiId, int chNo)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id and ch_no=:ch_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            cmd.Parameters.AddWithValue("ch_no", chNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointSx> SearchByVoiceSource(string sttnId, string voiceSource)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointSx> SearchByVoiceSource(string sttnId, string voiceSource, string devId)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcAreaPointSx> SearchByAreaId(string sttnId, string areaId)
        {
            List<VBcAreaPointSx> values = new List<VBcAreaPointSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", areaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VBcAreaPointSx Convert(NpgsqlDataReader reader)
        {
            VBcAreaPointSx value = new VBcAreaPointSx();          
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AiId = reader.GetString(2);
            value.ChNo = reader.GetInt32(3);
            value.PointId = reader.GetString(4);
            value.BcAreaId = reader.GetString(5);
            value.VoiceSource = reader.GetString(6);
            value.DevId = reader.GetString(7);
            value.ZoneId = reader.GetString(8);            
            
            return value;
        }
    }
}
