﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class BcAreaDAO
    {
        internal const string SQL_SELECT = "select sttn_id,bc_area_sttn,bc_area_id,cname,ename,voice_source,platform,cr_user,cr_date,userstamp,datestamp from pa_tb_bc_area";

        internal const string SQL_INSERT = "insert into pa_tb_bc_area(sttn_id,bc_area_sttn,bc_area_id,cname,ename,voice_source,platform,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_bc_area_id_seq'),:sttn_id,:bc_area_sttn,:bc_area_id,:cname,:ename,:voice_source,:platform,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_bc_area set sttn_id=:sttn_id,bc_area_sttn=:bc_area_sttn,bc_area_id=:bc_area_id,cname=:cname,ename=:ename,voice_source=:voice_source,platform=:platform,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_bc_area";

        private NpgsqlConnection conn;

        public BcAreaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(BcArea value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("voice_source", value.VoiceSource);
            cmd.Parameters.AddWithValue("platform", value.PlatForm);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_bc_area_id_seq')";
            // value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(BcArea value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("voice_source", value.VoiceSource);
            cmd.Parameters.AddWithValue("platform", value.PlatForm);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            // cmd.Parameters.AddWithValue("id", value.id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(BcArea value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            // cmd.Parameters.AddWithValue("id", value.id);
            cmd.ExecuteNonQuery();
        }

        public BcArea SearchPK(string sttnId, string bcAreaSttn, string bcAreaId)
        {
            BcArea value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<BcArea> Search(string sttnId)
        {
            List<BcArea> values = new List<BcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<BcArea> Search(string sttnId, string voiceSource)
        {
            List<BcArea> values = new List<BcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }        

        public IList<BcArea> Search(string sttnId,string bcAreaId,string voiceSource)
        {
            List<BcArea> values = new List<BcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static BcArea Convert(NpgsqlDataReader reader)
        {
            BcArea value = new BcArea();
            value.SttnId = reader.GetString(0);
            value.BcAreaSttn = reader.GetString(1);
            value.BcAreaId = reader.GetString(2);
            value.Cname = reader.GetString(3);
            value.Ename = reader.GetString(4);
            value.VoiceSource = reader.GetString(5);
            value.PlatForm = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
