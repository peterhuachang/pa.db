﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ZoneDAO
    {
        internal const string SQL_SELECT = "select sttn_id,zone_id,cname,ename,bc_status,cr_user,cr_date,userstamp,datestamp from pa_tb_zone";
        internal const string SQL_UPDATE = "update pa_tb_zone set bc_status=:bc_status,userstamp=:userstamp,datestamp=:datestamp";

        private NpgsqlConnection conn;

        public ZoneDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<Zone> Search()
        {
            List<Zone> values = new List<Zone>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Zone SearchPK(string sttnId, string zoneId)
        {
            Zone value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Zone> Search(string sttnId)
        {
            List<Zone> values = new List<Zone>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public bool Update(Zone value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("zone_id", value.ZoneId);
            cmd.Parameters.AddWithValue("bc_status", value.BcStatus);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public bool UpdateAll(string sttnId, int bcStatus, string userstamp)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_status", bcStatus);
            cmd.Parameters.AddWithValue("userstamp", userstamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static Zone Convert(NpgsqlDataReader reader)
        {
            Zone value = new Zone();

            value.SttnId = reader.GetString(0);
            value.ZoneId = reader.GetString(1);
            value.Cname = reader.GetString(2);
            value.Ename = reader.GetString(3);
            value.BcStatus = reader.IsDBNull(4) ? 0 : reader.GetInt32(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
