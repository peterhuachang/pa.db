﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ClaxxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,class_id,cname,ename,cr_user,cr_date,userstamp,datestamp from pa_tb_class";

        private NpgsqlConnection conn;

        public ClaxxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Claxx SearchPK(String sttnId , String classId)
        {
            Claxx value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and class_id=:class_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("class_id", classId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Claxx> Search(String sttnId)
        {
            List<Claxx> values = new List<Claxx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Claxx Convert(NpgsqlDataReader reader)
        {
            Claxx value = new Claxx();
            value.SttnId = reader.GetString(0);
            value.ClassId = reader.GetString(1);
            value.Cname = reader.GetString(2);
            value.Ename = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
