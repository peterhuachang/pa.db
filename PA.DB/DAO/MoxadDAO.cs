﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class MoxadDAO
    {
        internal const string SQL_SELECT = "select sttn_id,moxa_id,moxa_port,if_type,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_moxad";

        private NpgsqlConnection conn;

        public MoxadDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Moxad SearchPK(string sttnId, string moxaId, string moxaPort)
        {
            Moxad value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and moxa_id=:moxa_id and moxa_port=:moxa_port";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("moxa_id", moxaId);
            cmd.Parameters.AddWithValue("moxa_port", moxaPort);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Moxad> SearchSttn(string sttnId)
        {
            List<Moxad> values = new List<Moxad>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Moxad> SearchByMoxaId(string sttnId, string moxaId)
        {
            List<Moxad> values = new List<Moxad>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and moxa_id=:moxa_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("moxa_id", moxaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Moxad SearchByDvcType(string sttnId, string dvcType, string dvcId)
        {
            Moxad value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        internal static Moxad Convert(NpgsqlDataReader reader)
        {
            Moxad value = new Moxad();
            value.SttnId = reader.GetString(0);
            value.MoxaId = reader.GetString(1);
            value.MoxaPort = reader.GetString(2);
            value.IfType = reader.GetString(3);
            value.DvcType = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
