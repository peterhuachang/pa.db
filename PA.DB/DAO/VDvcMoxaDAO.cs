﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VDvcMoxaDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,labeling,cdesc,edesc,ip_addr,pmc_grp_num,pmc_pnt_num,loc_id,mu_id,cr_user,cr_date,userstamp,datestamp,moxa_port,if_type,moxa_id from pa_v_dvc_moxa";

        private NpgsqlConnection conn;

        public VDvcMoxaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VDvcMoxa SearchPK(string sttnId, string dvcType, string dvcId)
        {
            VDvcMoxa value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public IList<VDvcMoxa> Search(string sttnId, string dvcType)
        {
            List<VDvcMoxa> values = new List<VDvcMoxa>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VDvcMoxa> SearchSttn(string sttnId)
        {
            List<VDvcMoxa> values = new List<VDvcMoxa>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VDvcMoxa Convert(NpgsqlDataReader reader)
        {
            VDvcMoxa value = new VDvcMoxa();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.Labeling = reader.GetString(3);
            value.Cdesc = reader.GetString(4);
            value.Edesc = reader.GetString(5);
            value.IpAddr = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.PmcGrpNum = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.PmcPntNum = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.LocId = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.MuId = reader.IsDBNull(10) ? "" : reader.GetString(10);
            value.CrUser = reader.GetString(11);
            value.CrDate = reader.GetDateTime(12);
            value.UserStamp = reader.IsDBNull(13) ? "" : reader.GetString(13);
            value.DateStamp = reader.IsDBNull(14) ? (DateTime?)null : reader.GetDateTime(14);
            value.MoxaPort = reader.GetString(15);
            value.IfType = reader.GetString(16);
            value.MoxaId = reader.GetString(17);
            return value;
        }
    }
}
