﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoPhrParamDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,param,code,lang,idx_id,cr_user,cr_date,userstamp,datestamp from pa_tb_auto_phr_param";

        internal const string SQL_INSERT = "insert into pa_tb_auto_phr_param(sttn_id,pa_id,param,code,lang,idx_id,cr_user,cr_date,userstamp,datestamp)values(:sttn_id,:pa_id,:param,:code,:lang,:idx_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_auto_phr_param set pa_id=:pa_id,idx_id=:idx_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_auto_phr_param";

        private NpgsqlConnection conn;

        public AutoPhrParamDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(AutoPhrParam value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("param", value.Param);
            cmd.Parameters.AddWithValue("code", value.Code);
            cmd.Parameters.AddWithValue("lang", value.Lang);            
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public void Update(AutoPhrParam value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param and code=:code and lang=:lang";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("param", value.Param);
            cmd.Parameters.AddWithValue("code", value.Code);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public void Delete(AutoPhrParam value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param and code=:code and lang=:lang";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("param", value.Param);
            cmd.Parameters.AddWithValue("code", value.Code);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string paId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string paId, string param)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("param", param);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string paId, string param, string code, string lang)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param and code=:code and lang=:lang";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("param", param);
            cmd.Parameters.AddWithValue("code", code);
            cmd.Parameters.AddWithValue("lang", lang);
            cmd.ExecuteNonQuery();
        }

        public AutoPhrParam SearchPK(string sttnId, string paId, string param, string code, string lang)
        {
            AutoPhrParam value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param and code=:code and lang=:lang";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("param", param);
            cmd.Parameters.AddWithValue("code", code);
            cmd.Parameters.AddWithValue("lang", lang);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AutoPhrParam> Search(string sttnId, string paId, string param)
        {
            List<AutoPhrParam> values = new List<AutoPhrParam>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param order by code, lang asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("param", param);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoPhrParam> Search(string sttnId, string paId, string param, string code)
        {
            List<AutoPhrParam> values = new List<AutoPhrParam>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and param=:param and code=:code order by lang asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("param", param);
            cmd.Parameters.AddWithValue("code", code);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoPhrParam> SearchSttn(string sttnId)
        {
            List<AutoPhrParam> values = new List<AutoPhrParam>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id order by pa_id, param, code, lang asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoPhrParam> SearchPaId(string sttnId, string paId)
        {
            List<AutoPhrParam> values = new List<AutoPhrParam>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id order by param, code, lang asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoPhrParam Convert(NpgsqlDataReader reader)
        {
            AutoPhrParam value = new AutoPhrParam();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.Param = reader.GetString(2);
            value.Code = reader.GetString(3);
            value.Lang = reader.GetString(4);            
            value.IdxId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
