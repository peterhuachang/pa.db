﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DvcAlarmListDAO
    {
        internal const string SQL_SELECT = "select dvc_type,alarm_id,alarm_flag,cdesc,edesc,cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_alarm_list";

        private NpgsqlConnection conn;

        public DvcAlarmListDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public DvcAlarmList SearchPK(string dvcType,string alarmId,int alarmFlag)
        {
            DvcAlarmList value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where dvc_type=:dvc_type and alarm_id=:alarm_id and alarm_flag=:alarm_flag";
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("alarm_id", alarmId);
            cmd.Parameters.AddWithValue("alarm_flag", alarmFlag);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<DvcAlarmList> SearchAll()
        {
            List<DvcAlarmList> values = new List<DvcAlarmList>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static DvcAlarmList Convert(NpgsqlDataReader reader)
        {
            DvcAlarmList value = new DvcAlarmList();
            value.DvcType = reader.GetString(0);
            value.AlarmId = reader.GetString(1);
            value.AlarmFlag = reader.GetInt32(2);
            value.Cdesc = reader.IsDBNull(3) ? "" : reader.GetString(3);
            value.Edesc = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
