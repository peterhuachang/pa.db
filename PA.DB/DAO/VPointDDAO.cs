﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VPointDDAO
    {
        internal const string SQL_SELECT = "select sttn_id,point_id from pa_v_pointd";

        private NpgsqlConnection conn;

        public VPointDDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VPointD SearchPK(string sttnId, string pointId)
        {
            VPointD value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and point_id=:point_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VPointD> SearchSttn(string sttnId)
        {
            List<VPointD> values = new List<VPointD>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VPointD Convert(NpgsqlDataReader reader)
        {
            VPointD value = new VPointD();
            value.SttnId = reader.GetString(0);
            value.PointId = reader.GetString(1);
            return value;
        }
    }
}
