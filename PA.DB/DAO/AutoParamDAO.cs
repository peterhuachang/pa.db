﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoParamDAO
    {
        internal const string SQL_SELECT = "select param,cdesc,edesc from pa_tb_auto_param";

        private NpgsqlConnection conn;

        public AutoParamDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public AutoParam SearchByParam(string param)
        {
            AutoParam value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where param=:param";
            cmd.Parameters.AddWithValue("param", param);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AutoParam> SearchAll()
        {
            List<AutoParam> values = new List<AutoParam>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoParam Convert(NpgsqlDataReader reader)
        {
            AutoParam value = new AutoParam();
            value.Param = reader.GetString(0);
            value.Cdesc = reader.GetString(1);
            value.Edesc = reader.GetString(2);           
            return value;
        }
    }
}
