﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PA.DB;

namespace PA.DB.DAO
{
    public class VBcAreaDAO
    {
        internal const string SQL_SELECT = "select point_id, sttn_id, bc_area_sttn, bc_area_id, area_cname, area_ename, voice_source, platform from pa_v_bc_area";

        private NpgsqlConnection conn;

        public VBcAreaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VBcArea SearchPK(string pointId, string bcAreaSttn, string bcAreaId)
        {
            VBcArea value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where point_id=:point_id and bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("point_id", pointId);
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VBcArea> SearchBySttn(string sttnId)
        {
            List<VBcArea> values = new List<VBcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcArea> Search(string sttnId, string bcAreaSttn, string bcAreaId)
        {
            List<VBcArea> values = new List<VBcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcArea> SearchByPointId(string sttnId, string bcAreaSttn, string pointId)
        {
            List<VBcArea> values = new List<VBcArea>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_sttn=:bc_area_sttn and point_id=:point_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("point_id", pointId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VBcArea Convert(NpgsqlDataReader reader)
        {
            VBcArea value = new VBcArea();
            value.PointId = reader.GetString(0);
            value.SttnId = reader.GetString(1);
            value.BcAreaSttn = reader.GetString(2);
            value.BcAreaId = reader.GetString(3);
            value.AreaCname = reader.GetString(4);
            value.AreaEname = reader.GetString(5);
            value.VoiceSource = reader.GetString(6);
            value.PlatForm = reader.GetString(7);
            return value;
        }
    }
}
