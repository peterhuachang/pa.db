﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PattnDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,pattn_id,ename,cname,color_def,cr_user,cr_date,userstamp,datestamp from pa_tb_pattn";

        private NpgsqlConnection conn;

        public PattnDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Pattn SearchById(int id)
        {
            Pattn value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public Pattn SearchPattn(string sttnId,string pattnId)
        {
            Pattn value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pattn_id=:pattn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pattn_id", pattnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;

        }

        public IList<Pattn> SearchAll()
        {
            List<Pattn> values = new List<Pattn>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Pattn Convert(NpgsqlDataReader reader)
        {
            Pattn value = new Pattn();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.PattnId = reader.GetString(2);
            value.Ename = reader.GetString(3);
            value.Cname = reader.GetString(4);
            value.ColorDef = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.Userstamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.Datestamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);

            return value;
        }
    }
}
