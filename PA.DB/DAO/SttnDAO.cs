﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SttnDAO
    {
        internal const string SQL_SELECT = "select sttn_id,cname,ename,type,takeover_flag,cr_user,cr_date,userstamp,datestamp from pa_tb_sttn";
        internal const string SQL_UPDATE = "update pa_tb_sttn set takeover_falg=:takeover_falg, userstamp=:userstamp, datestamp=:datestamp where sttn_id=:sttn_id";

        private NpgsqlConnection conn;

        public SttnDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<Sttn> Search()
        {
            List<Sttn> values = new List<Sttn>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Sttn SearchPK(string sttnId)
        {
            Sttn value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public bool UpdateTakeOverFlag(Sttn value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("takeover_falg", value.TakeroverFlag);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static Sttn Convert(NpgsqlDataReader reader)
        {
            Sttn value = new Sttn();
            value.SttnId = reader.GetString(0);
            value.Cname = reader.GetString(1);
            value.Ename = reader.GetString(2);
            value.Type = reader.GetString(3);
            value.TakeroverFlag = reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
