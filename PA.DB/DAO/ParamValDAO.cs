﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ParamValDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,param_type,param_id,param_val,cdesc,edesc,cr_user,cr_date,userstamp,datestamp from pa_tb_param_val";

        private NpgsqlConnection conn;

        public ParamValDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public ParamVal SearchPK(int id)
        {
            ParamVal value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public ParamVal SearchPK(string sttnId, string paramType, string paramId)
        {
            ParamVal value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and param_type=:param_type and param_id=:param_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("param_type", paramType);
            cmd.Parameters.AddWithValue("param_id", paramId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set uniquekey error");
            }
            reader.Close();
            return value;
        }

        public ParamVal SearchParamTypeId(string paramType, string paramId)
        {
            ParamVal value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where param_type=:param_type and param_id=:param_id";
            cmd.Parameters.AddWithValue("param_type", paramType);
            cmd.Parameters.AddWithValue("param_id", paramId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set uniquekey error");
            }
            reader.Close();
            return value;
        }

        public IList<ParamVal> SearchSttn(string sttnId)
        {
            List<ParamVal> values = new List<ParamVal>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<ParamVal> SearchSttnParamType(string sttnId, string paramType)
        {
            List<ParamVal> values = new List<ParamVal>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and param_type=:param_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("param_type", paramType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static ParamVal Convert(NpgsqlDataReader reader)
        {
            ParamVal value = new ParamVal();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.ParamType = reader.GetString(2);
            value.ParamId = reader.GetString(3);
            value.ValueName = reader.GetString(4);
            value.Cdesc = reader.GetString(5);
            value.Edesc = reader.GetString(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
