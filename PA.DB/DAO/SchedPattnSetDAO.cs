﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SchedPattnSetDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,p_key,pattn_id,voice_source,dvc_id,cname,ename,start_time,end_time,end_timea,interval,duration,repeat,cr_user,cr_date,userstamp,datestamp from pa_tb_sched_pattn_set";

        internal const string SQL_INSERT = "insert into pa_tb_sched_pattn_set(id,sttn_id,p_key,pattn_id,voice_source,dvc_id,cname,ename,start_time,end_time,end_timea,interval,duration,repeat,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_sched_pattn_set_id_seq'),:sttn_id,CURRVAL('pa_tb_sched_pattn_set_id_seq'),:pattn_id,:voice_source,:dvc_id,:cname,:ename,:start_time,:end_time,:end_timea,:interval,:duration,:repeat,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_sched_pattn_set set sttn_id=:sttn_id,p_key=:p_key,pattn_id=:pattn_id,voice_source=:voice_source,dvc_id=:dvc_id,cname=:cname,ename=:ename,start_time=:start_time,end_time=:end_time,end_timea=:end_timea,interval=:interval,duration=:duration,repeat=:repeat,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp ";

        internal const string SQL_DELETE = "delete from pa_tb_sched_pattn_set";

        private NpgsqlConnection conn;

        public SchedPattnSetDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(SchedPattnSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            //cmd.Parameters.AddWithValue("id", value.Id);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            //cmd.Parameters.AddWithValue("p_key", value.Pkey);
            cmd.Parameters.AddWithValue("pattn_id", value.PattnId);
            cmd.Parameters.AddWithValue("voice_source", value.VoiceSource);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("start_time", value.StartTime);
            cmd.Parameters.AddWithValue("end_time", value.EndTime);
            cmd.Parameters.AddWithValue("end_timea", value.EndTimeA);
            cmd.Parameters.AddWithValue("interval", value.Interval);
            cmd.Parameters.AddWithValue("duration", value.Duration);
            cmd.Parameters.AddWithValue("repeat", value.Repeat);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_sched_pattn_set_id_seq')";
            value.Pkey = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(SchedPattnSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and p_key=:p_key";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("p_key", value.Pkey);
            cmd.Parameters.AddWithValue("pattn_id", value.PattnId);
            cmd.Parameters.AddWithValue("voice_source", value.VoiceSource);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("start_time", value.StartTime);
            cmd.Parameters.AddWithValue("end_time", value.EndTime);
            cmd.Parameters.AddWithValue("end_timea", value.EndTimeA);
            cmd.Parameters.AddWithValue("interval", value.Interval);
            cmd.Parameters.AddWithValue("duration", value.Duration);
            cmd.Parameters.AddWithValue("repeat", value.Repeat);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(SchedPattnSet value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and p_key=:p_key";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("p_key", value.Pkey);
            cmd.ExecuteNonQuery();
        }

        public SchedPattnSet SearchByPkey(string sttnId, int pKey)
        {
            SchedPattnSet value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and p_key=:p_key";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("p_key", pKey);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }


        public SchedPattnSet SearchById(int id)
        {
            SchedPattnSet value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SchedPattnSet> SearchByPattnId(string sttnId, string pattnId)
        {
            List<SchedPattnSet> values = new List<SchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pattn_id=:pattn_id order by start_time, p_key";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pattn_id", pattnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SchedPattnSet> Search(string sttnId, string pattnId, string voiceSource, string dvcId)
        {
            List<SchedPattnSet> values = new List<SchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pattn_id=:pattn_id and voice_source=:voice_source and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pattn_id", pattnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SchedPattnSet> SearchAllPattern(string sttnId)
        {
            List<SchedPattnSet> values = new List<SchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select pattn_id from pa_tb_sched_pattn_set where sttn_id=:sttn_id group by pattn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(ConvertPattern(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SchedPattnSet> SearchAll()
        {
            List<SchedPattnSet> values = new List<SchedPattnSet>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SchedPattnSet ConvertPattern(NpgsqlDataReader reader)
        {
            SchedPattnSet value = new SchedPattnSet();
            value.PattnId = reader.GetString(0);
            return value;
        }

        internal static SchedPattnSet Convert(NpgsqlDataReader reader)
        {
            SchedPattnSet value = new SchedPattnSet();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.Pkey = (int)reader.GetDecimal(2);
            value.PattnId = reader.GetString(3);
            value.VoiceSource = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.Cname = reader.GetString(6);
            value.Ename = reader.GetString(7);
            value.StartTime = reader.GetString(8);
            value.EndTime = reader.GetString(9);
            value.EndTimeA = reader.GetString(10);
            value.Interval = reader.GetInt32(11);
            value.Duration = reader.GetInt32(12);
            value.Repeat = reader.GetInt32(13);
            value.CrUser = reader.GetString(14);
            value.CrDate = reader.GetDateTime(15);
            value.Userstamp = reader.IsDBNull(16) ? "" : reader.GetString(16);
            value.Datestamp = reader.IsDBNull(17) ? (DateTime?)null : reader.GetDateTime(17);
            return value;
        }
    }
}
