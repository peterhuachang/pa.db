﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DtmfDAO
    {
        internal const string SQL_SELECT = "select sttn_id,adsw_id,dtmf_id,l_sttn_id,cr_user,cr_date,userstamp,datestamp from pa_tb_dtmf";

        private NpgsqlConnection conn;

        public DtmfDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Dtmf SearchPK(string sttnId, string adswId, string dtmfId, string lsttnId)
        {
            Dtmf value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and adsw_id=:adsw_id and dtmf_id=:dtmf_id and l_sttn_id=:l_sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("adsw_id", adswId);
            cmd.Parameters.AddWithValue("dtmf_id", dtmfId);
            cmd.Parameters.AddWithValue("l_sttn_id", lsttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Dtmf> SearchSttn(string sttnId)
        {
            List<Dtmf> values = new List<Dtmf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Dtmf Convert(NpgsqlDataReader reader)
        {
            Dtmf value = new Dtmf();
            value.SttnId = reader.GetString(0);
            value.AdswId = reader.GetString(1);
            value.DtmfId = reader.GetString(2);
            value.LSttnId = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
