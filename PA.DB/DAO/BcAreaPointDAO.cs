﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class BcAreaPointDAO
    {
        internal const string SQL_SELECT = "select bc_area_sttn,bc_area_id,point_id,cr_user,cr_date,userstamp,datestamp from pa_tb_bc_area_point";

        internal const string SQL_INSERT = "insert into pa_tb_bc_area_point(bc_area_sttn,bc_area_id,point_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_bc_area_point_id_seq'),:bc_area_sttn,:bc_area_id,:point_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_bc_area_point set bc_area_sttn=:bc_area_sttn,bc_area_id=:bc_area_id,point_id=:point_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id and point_id=:point_id";

        internal const string SQL_DELETE = "delete from pa_tb_bc_area_point";

        private NpgsqlConnection conn;

        public BcAreaPointDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(BcAreaPoint value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_bc_area_point_id_SEQ')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(BcAreaPoint value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public void Delete(BcAreaPoint value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id and point_id=:point_id";
            cmd.Parameters.AddWithValue("bc_area_sttn", value.BcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.ExecuteNonQuery();
        }

        public BcAreaPoint SearchPK(string bcAreaSttn, string bcAreaId, string pointId)
        {
            BcAreaPoint value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id and point_id=:point_id";
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<BcAreaPoint> Search(string bcAreaSttn, string bcAreaId)
        {
            List<BcAreaPoint> values = new List<BcAreaPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where bc_area_sttn=:bc_area_sttn and bc_area_id=:bc_area_id ";
            cmd.Parameters.AddWithValue("bc_area_sttn", bcAreaSttn);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<BcAreaPoint> SearchSttn(string sttnId)
        {
            List<BcAreaPoint> values = new List<BcAreaPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where bc_area_sttn=:bc_area_sttn ";
            cmd.Parameters.AddWithValue("bc_area_sttn", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static BcAreaPoint Convert(NpgsqlDataReader reader)
        {
            BcAreaPoint value = new BcAreaPoint();
            value.BcAreaSttn = reader.GetString(0);
            value.BcAreaId = reader.GetString(1);
            value.PointId = reader.GetString(2);
            value.CrUser = reader.GetString(3);
            value.CrDate = reader.GetDateTime(4);
            value.UserStamp = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DateStamp = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
            return value;
        }
    }
}
