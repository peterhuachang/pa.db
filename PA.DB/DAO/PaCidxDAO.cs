﻿using Npgsql;
using PA.DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PaCidxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,idx_id,name,card_id,cdesc,edesc,time_len,standard,cr_user,cr_date,userstamp,datestamp from pa_tb_pa_cidx";

        internal const string SQL_INSERT = "insert into pa_tb_pa_cidx(sttn_id,pa_id,idx_id,name,card_id,cdesc,edesc,time_len,standard,cr_user,cr_date,userstamp,datestamp)values (:sttn_id,:pa_id,:idx_id,:name,:card_id,:cdesc,:edesc,:time_len,:standard,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_pa_cidx set cdesc=:cdesc,edesc=:edesc,standard=:standard,userstamp=:userstamp,datestamp=:datestamp where sttn_id=:sttn_id AND pa_id=:pa_id AND idx_id=:idx_id";

        internal const string SQL_DELETE = "delete from pa_tb_pa_cidx";

        private NpgsqlConnection conn;

        public PaCidxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(PaCidx value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.Parameters.AddWithValue("name", value.Name);
            cmd.Parameters.AddWithValue("card_id", value.CardId);
            cmd.Parameters.AddWithValue("cdesc", value.Cdesc);
            cmd.Parameters.AddWithValue("edesc", value.Edesc);
            cmd.Parameters.AddWithValue("time_len", value.TimeLen);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("standard", value.Standard);
            cmd.ExecuteNonQuery();
        }

        public void Update(PaCidx value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("cdesc", value.Cdesc);
            cmd.Parameters.AddWithValue("edesc", value.Edesc);
            cmd.Parameters.AddWithValue("standard", value.Standard);
            cmd.Parameters.AddWithValue("userStamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(PaCidx value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and idx_id=:idx_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("idx_id", value.IdxId);
            cmd.ExecuteNonQuery();
        }

        public PaCidx SearchPK(string sttnId, string paId, string idxId)
        {
            PaCidx value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and idx_id=:idx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("idx_id", idxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<PaCidx> Search(string sttnId)
        {
            List<PaCidx> values = new List<PaCidx>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<PaCidx> Search(string sttnId, string paId, string cardId)
        {
            List<PaCidx> values = new List<PaCidx>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and card_id=:card_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("card_id", cardId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IDictionary<string, IList<PaCidx>> Search(string sttnId, string paId)
        {
            Dictionary<string, IList<PaCidx>> values = new Dictionary<string, IList<PaCidx>>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                PaCidx cidx = Convert(reader);
                IList<PaCidx> result;
                if (!values.TryGetValue(cidx.CardId, out result))
                {
                    result = new List<PaCidx>();
                    values[cidx.CardId] = result;
                }
                result.Add(cidx);
            }
            reader.Close();

            return values;
        }

        internal static PaCidx Convert(NpgsqlDataReader reader)
        {
            PaCidx value = new PaCidx();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.IdxId = reader.GetString(2);
            value.Name = reader.GetString(3);
            value.CardId = reader.GetString(4);
            value.Cdesc = reader.GetString(5);
            value.Edesc = reader.GetString(6);
            value.TimeLen = reader.GetInt32(7);
            value.Standard = reader.GetString(8);
            value.CrUser = reader.GetString(9);
            value.CrDate = reader.GetDateTime(10);
            value.UserStamp = reader.IsDBNull(11) ? "" : reader.GetString(11);
            value.DateStamp = reader.IsDBNull(12) ? (DateTime?)null : reader.GetDateTime(12);
            return value;
        }
    }
}


/**
 *-- Function: after_delete_pa_tb_pa_cidx()

-- DROP FUNCTION after_delete_pa_tb_pa_cidx();

CREATE OR REPLACE FUNCTION after_delete_pa_tb_pa_cidx()
  RETURNS trigger AS
$BODY$
    DECLARE   
        v_pattn record;  
    BEGIN
        IF (TG_OP = 'DELETE') THEN
				
		FOR v_pattn IN 	SELECT a.STTN_ID, b.PATTN_ID
				FROM    PA_TB_SENTENCE a, PA_TB_SENTENCE_PHR b
				WHERE a.STTN_ID = b.STTN_ID
				AND   a.STTN_ID = OLD.STTN_ID
				AND   b.IDX_ID = OLD.IDX_ID 
				AND   a.PA_ID = OLD.PA_ID
				AND   a.SENTEN_ID = b.SENTEN_ID LOOP
			IF FOUND THEN 			
				DELETE FROM PA_TB_SCHED_PATTN_SET a		
				WHERE a.STTN_ID  = v_pattn.STTN_ID
				AND   a.SENTEN_ID = v_pattn.SENTEN_ID;
			END IF;
		END LOOP;
		
        END IF;
        RETURN NULL; 
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION after_delete_pa_tb_pa_cidx()
  OWNER TO pa;

 */

