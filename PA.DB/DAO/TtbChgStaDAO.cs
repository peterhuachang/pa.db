﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class TtbChgStaDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,grp_addr,pnt_num,datestamp,alarm_id,status,proc_flag,proc_date from pa_tb_ttb_chg_sta";

        private NpgsqlConnection conn;

        public TtbChgStaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public TtbChgSta SearchById(int id)
        {
            TtbChgSta value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public TtbChgSta SearchPKey(string sttn_id, string grp_addr, string pnt_num)
        {
            TtbChgSta value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and grp_addr=:grp_addr and pnt_num=:pnt_num";
            cmd.Parameters.AddWithValue("sttn_id", sttn_id);
            cmd.Parameters.AddWithValue("grp_addr", grp_addr);
            cmd.Parameters.AddWithValue("pnt_num", pnt_num);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<TtbChgSta> SearchAll()
        {
            List<TtbChgSta> values = new List<TtbChgSta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static TtbChgSta Convert(NpgsqlDataReader reader)
        {
            TtbChgSta value = new TtbChgSta();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.GrpAddr = reader.GetString(2);
            value.PntNum = reader.GetString(3);
            value.Datestamp = reader.GetDateTime(4);
            value.AlarmId = reader.GetString(5);
            value.Status = reader.GetInt32(6);
            value.ProcFlag = reader.GetString(7);
            value.ProcDate = reader.GetDateTime(8);
            return value;
        }
    }
}
