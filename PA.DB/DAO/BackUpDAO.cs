﻿using Npgsql;
using PA.DB.Reader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class BackUpDAO
    {
        private ReaderManager mgr;

        private NpgsqlConnection conn;

        public BackUpDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
            this.mgr = new ReaderManager();
        }

        public void ExecuteCommand(string command)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = command;
            cmd.ExecuteNonQuery();
        }

        public List<string> SearchAllTable()
        {
            List<string> table = new List<string>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "SELECT table_name FROM information_schema.tables where table_schema='public'";
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                table.Add(reader.GetString(0));
            }
            table.Remove("pa_tb_dvc_alarm_hist");
            table.Remove("pa_tb_oper_log");
            table.Remove("pa_tb_curr_bc");
            table.Remove("pa_tb_card_index");
            table.Remove("pa_tb_sched_bc_area");
            table.Remove("pa_tb_sched_bc_pa");
            table.Remove("pa_v_af_sl_zone");
            table.Remove("pa_v_as_af");
            table.Remove("pa_v_bc_area");
            table.Remove("pa_v_bc_area_point_scd");
            table.Remove("pa_v_bc_area_point_scd_dtmf");
            table.Remove("pa_v_bc_area_point_sx");
            table.Remove("pa_v_bc_point");
            table.Remove("pa_v_bc_sx");
            table.Remove("pa_v_card_index_sentence_phr");
            table.Remove("pa_v_curr_auto_bc");
            table.Remove("pa_v_device");
            table.Remove("pa_v_dvc_moxa");
            table.Remove("pa_v_dvc_status");
            table.Remove("pa_v_param_type_value");
            table.Remove("pa_v_pointd");
            table.Remove("pa_v_proc_dvc_map");
            table.Remove("pa_tb_sched_set");
            table.Remove("pa_v_scd_point");
            table.Remove("pa_v_scd_point_dtmf");
            table.Remove("pa_v_sched_pattn_set");
            table.Remove("pa_v_sched_phrase");
            table.Remove("pa_v_spkline");
            table.Remove("pa_v_as_af_2");
            table.Remove("pa_v_pa_cidx");

            reader.Close();
            return table;
        }

        public List<String> Generate(String tableName)
        {
            List<String> inserts = new List<string>();
            IList<ColumnDefinition> columnns = new List<ColumnDefinition>();

            String sqlColumns = "";

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "SELECT COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name =" + "'" + tableName + "'" + " ORDER BY ordinal_position";
            NpgsqlDataReader npgReader = cmd.ExecuteReader();
            while (npgReader.Read())
            {
                ColumnDefinition c = new ColumnDefinition();
                c.Name = npgReader.GetString(0);
                c.DataType = npgReader.GetString(1);
                sqlColumns += "" + c.Name + ",";
                columnns.Add(c);
            }
            sqlColumns = sqlColumns.Remove(sqlColumns.Count() - 1);
            npgReader.Close();


            NpgsqlCommand cmd2;
            cmd2 = this.conn.CreateCommand();
            cmd2.CommandText = "SELECT * FROM " + tableName + ";";
            NpgsqlDataReader reader = cmd2.ExecuteReader();
            while (reader.Read())
            {
                String sqlValues = "";
                for (int i = 0; i < columnns.Count; i++)
                {
                    ColumnDefinition cd = columnns[i];
                    //ICellReader cellReader = this.mgr.GetReader(cd.DataType);
                    //String value = cellReader.Read(reader, i);
                    PA.DB.Reader.ReaderManager.ReaderHandler readerHandler = this.mgr.GetHandler(cd.DataType);
                    string value2 = readerHandler.Invoke(reader, i);
                    sqlValues += value2 + ",";
                }
                sqlValues = sqlValues.Remove(sqlValues.Count() - 1);
                String sql = "insert into " + tableName + " (" + sqlColumns + ") values (" + sqlValues + ")";
                inserts.Add(sql);
            }
            reader.Close();

            return inserts;
        }
    }

    class ColumnDefinition
    {
        public String Name { get; set; }

        public String DataType { get; set; }
    }
}
