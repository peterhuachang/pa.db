﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class CurrBcDAO
    {
        internal const string SQL_SELECT = @"select id,sttn_id,voice,bc_area_id,cmd_from,datestamp,
                                            bc_sttn_id,src_que,senten_cnt,senten_id from pa_tb_curr_bc";
        internal const string SQL_INSERT = @"insert into pa_tb_curr_bc (id,sttn_id,voice,bc_area_id,cmd_from,
                                            datestamp,bc_sttn_id,src_que,senten_cnt,senten_id) 
                                            values(nextval('pa_tb_curr_bc_id_seq'),:sttn_id,:voice,
                                            :bc_area_id,:cmd_from,:datestamp,:bc_sttn_id,:src_que,
                                            :senten_cnt,:senten_id)";
        internal const string SQL_DELETE = "delete from pa_tb_curr_bc";

        private NpgsqlConnection conn;

        public CurrBcDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(CurrBc value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("voice", value.Voice);
            cmd.Parameters.AddWithValue("bc_area_id", value.BcAreaId);
            cmd.Parameters.AddWithValue("cmd_from", value.CmdFrom);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.Parameters.AddWithValue("bc_sttn_id", value.BcSttnId);
            cmd.Parameters.AddWithValue("src_que", value.SrcQue);
            cmd.Parameters.AddWithValue("senten_cnt", value.SentenCnt);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_curr_bc_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Delete(CurrBc value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id";
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string voice)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and voice=:voice";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice", voice);
            cmd.ExecuteNonQuery();
        }

        public void DeleteByBCSttnId(string sttnId, string bcSttnId, string voice)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and bc_sttn_id=:bc_sttn_id and voice=:voice";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_sttn_id", bcSttnId);
            cmd.Parameters.AddWithValue("voice", voice);
            cmd.ExecuteNonQuery();
        }

        public void DeleteByBCSttnId(string sttnId, string bcSttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and bc_sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_sttn_id", bcSttnId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id or bc_sttn_id=:bc_sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void DeleteAll()
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE;
            cmd.ExecuteNonQuery();
        }

        public CurrBc SearchPK(int id)
        {
            CurrBc value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<CurrBc> SearchSttn(string sttnId)
        {
            List<CurrBc> values = new List<CurrBc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<CurrBc> SearchCurrBCData(string bcSttnId)
        {
            List<CurrBc> values = new List<CurrBc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where bc_sttn_id=:bc_sttn_id order by sttn_id, src_que, voice, bc_area_id";
            cmd.Parameters.AddWithValue("bc_sttn_id", bcSttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static CurrBc Convert(NpgsqlDataReader reader)
        {
            CurrBc value = new CurrBc();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.Voice = reader.GetString(2);
            value.BcAreaId = reader.GetString(3);
            value.CmdFrom = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.DateStamp = reader.IsDBNull(5) ? (DateTime?)null : reader.GetDateTime(5);
            value.BcSttnId = reader.GetString(6);
            value.SrcQue = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.SentenCnt = reader.GetInt32(8);
            value.SentenId = reader.IsDBNull(9) ? "" : reader.GetString(9);
            return value;
        }
    }
}
