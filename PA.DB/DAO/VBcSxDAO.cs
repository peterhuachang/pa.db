﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;

namespace PA.DB.DAO
{
    public class VBcSxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,bc_area_id,zone_id,voice_source,dev_id,sp_id,sl_id,ss_id,bc_port_id,sx_id,ai_id,ai_ch_no,ao_id,ao_ch_no from pa_v_bc_sx";

        private NpgsqlConnection conn;

        public VBcSxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VBcSx SearchPK(string sttnId, string zoneId, string voiceSource, string devId, string slId)
        {
            VBcSx value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id and voice_source=:voice_source and dev_id=:dev_id and sl_id=:sl_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VBcSx> Search(string sttnId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> Search(string sttnId, string bcAreaId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByVoiceSource(string sttnId, string voiceSource)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByVoiceSource(string sttnId, string voiceSource, string devId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByAiId(string sttnId, string sxId, string aiId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByAiIdChNo(string sttnId, string sxId, string aiId, int aiChNo)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id and ai_ch_no=:ai_ch_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            cmd.Parameters.AddWithValue("ai_ch_no", aiChNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByAoId(string sttnId, string sxId, string aoId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByAoIdChNo(string sttnId, string sxId, string aoId, int aoChNo)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ao_ch_no=:ao_ch_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ao_ch_no", aoChNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }
        /*
        public IList<VBcSx> SearchByAoIdCiNo(string sttnId, string sxId, string aoId, int aoCiNo)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ao_ci_no=:ao_ci_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ao_ci_no", aoCiNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }
        */
        public IList<VBcSx> SearchByAreaZone(string sttnId, string bcAreaId, string zoneId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id and zond_id=:zond_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            cmd.Parameters.AddWithValue("zond_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByAreaId(string sttnId, string bcAreaId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bc_area_id=:bc_area_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_area_id", bcAreaId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VBcSx> SearchByZoneId(string sttnId, string zoneId)
        {
            List<VBcSx> values = new List<VBcSx>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zond_id=:zond_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zond_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VBcSx Convert(NpgsqlDataReader reader)
        {
            VBcSx value = new VBcSx();
            value.SttnId = reader.GetString(0);            
            value.BcAreaId = reader.GetString(1);          
            value.ZoneId = reader.GetString(2);
            value.VoiceSource = reader.GetString(3);
            value.DevId = reader.GetString(4);
            value.SpId = reader.GetString(5);
            value.SlId = reader.GetString(6);
            value.SsId = reader.GetString(7);
            value.BcPortId = reader.GetInt32(8);
            value.SxId = reader.GetString(9);
            value.AiId = reader.GetString(10);
            value.AiChNo = reader.GetInt32(11);
            value.AoId = reader.GetString(12);
            value.AoChNo = reader.GetInt32(13);
            //value.AoCiNo = reader.GetInt32(14);
            return value;
        }
    }
}
