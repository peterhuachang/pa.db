﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class TitleSentenceDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,p_key,senten_seq,senten_id,cr_user,cr_date,userstamp,datestamp from pa_tb_title_sentence";

        internal const string SQL_INSERT = "insert into pa_tb_title_sentence(id,sttn_id,p_key,senten_seq,senten_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_title_sentence_id_seq'),:sttn_id,:p_key,:senten_seq,:senten_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_title_sentence set sttn_id=:sttn_id,p_key=:p_key,senten_seq=:senten_seq,senten_id=:senten_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_title_sentence";

        private NpgsqlConnection conn;

        public TitleSentenceDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(TitleSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("p_key", value.PKey);
            cmd.Parameters.AddWithValue("senten_seq", value.SentenSeq);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.ExecuteNonQuery();
        }

        public void Delete(TitleSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and p_key=:p_key";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("p_key", value.PKey);
            cmd.ExecuteNonQuery();
        }

        public void Update(TitleSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where id=:id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("p_key", value.PKey);
            cmd.Parameters.AddWithValue("senten_seq", value.SentenSeq);
            cmd.Parameters.AddWithValue("senten_id", value.SentenId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public TitleSentence SearchById(int id)
        {
            TitleSentence value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public TitleSentence SearchByPkey(string sttnId, int pKey, int sentenSeq)
        {
            TitleSentence value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and p_key=:p_key and senten_seq=:senten_seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("p_key", pKey);
            cmd.Parameters.AddWithValue("senten_seq", sentenSeq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public List<TitleSentence> SearchBySttnId(string sttnId)
        {
            List<TitleSentence> values = new List<TitleSentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public List<TitleSentence> SearchBySttnPKey(string sttnId, int pKey)
        {
            List<TitleSentence> values = new List<TitleSentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and p_key=:p_key order by senten_seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("p_key", pKey);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static TitleSentence Convert(NpgsqlDataReader reader)
        {
            TitleSentence value = new TitleSentence();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.PKey = (int)reader.GetDecimal(2);
            value.SentenSeq = reader.GetInt32(3);
            value.SentenId = reader.GetString(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.Userstamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.Datestamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
