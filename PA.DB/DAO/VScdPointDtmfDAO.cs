﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VScdPointDtmfDAO
    {
        internal const string SQL_SELECT = "select sttn_id, voice_source, dev_id, sc_id, bit_id, point_id, bit_type from pa_v_scd_point_dtmf";

        private NpgsqlConnection conn;

        public VScdPointDtmfDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VScdPoint SearchPK(string sttnId, string voiceSource, string devId, string scId, int bitId, string pointId)
        {
            VScdPoint value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id and sc_id=:sc_id and bit_id=:bit_id and point_id=:point_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            cmd.Parameters.AddWithValue("bit_id", bitId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<VScdPoint> SearchBySttn(string sttnId)
        {
            List<VScdPoint> values = new List<VScdPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VScdPoint> SearchByScId(string sttnId, string scId)
        {
            List<VScdPoint> values = new List<VScdPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sc_id=:sc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VScdPoint> SearchByScId(string sttnId, string scId, string bitType)
        {
            List<VScdPoint> values = new List<VScdPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sc_id=:sc_id and bit_type=:bit_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            cmd.Parameters.AddWithValue("bit_type", bitType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VScdPoint> SearchByDvcId(string sttnId, string devId)
        {
            List<VScdPoint> values = new List<VScdPoint>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VScdPoint Convert(NpgsqlDataReader reader)
        {
            VScdPoint value = new VScdPoint();          
            value.SttnId = reader.GetString(0);
            value.VoiceSource = reader.GetString(1);
            value.DevId = reader.GetString(2);
            value.ScId = reader.GetString(3);
            value.BitId = reader.GetInt32(4);            
            value.PointId = reader.GetString(5);
            return value;
        }
    }
}
