﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PaCardDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,card_id,cdesc,edesc,meno,cr_user,cr_date,userstamp,datestamp from pa_tb_pa_card";

        private NpgsqlConnection conn;

        public PaCardDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public PaCard SearchPK(string sttnId, string paId, string cardId)
        {
            PaCard value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and card_id=:card_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("card_id", cardId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<PaCard> SearchSttn(string sttnId)
        {
            List<PaCard> values = new List<PaCard>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static PaCard Convert(NpgsqlDataReader reader)
        {
            PaCard value = new PaCard();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.CardId = reader.GetString(2);
            value.Cdesc = reader.GetString(3);
            value.Edesc = reader.GetString(4);
            value.Meno = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
