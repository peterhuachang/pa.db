﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VDeviceDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,labeling,dlt_cdesc,dtl_edesc, ip_addr, pmc_grp_num, pmc_pnt_num, loc_id,mu_id,type_cdesc,type_edesc from pa_v_device";

        private NpgsqlConnection conn;

        public VDeviceDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VDevice SearchPK(string sttn_id, string dvc_type, string dvc_id)
        {
            VDevice value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttn_id);
            cmd.Parameters.AddWithValue("dvc_type", dvc_type);
            cmd.Parameters.AddWithValue("dvc_id", dvc_id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public IList<VDevice> SearchBySttn(string sttnId)
        {
            List<VDevice> values = new List<VDevice>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VDevice> Search(string sttnId, string dvcType)
        {
            List<VDevice> values = new List<VDevice>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VDevice Convert(NpgsqlDataReader reader)
        {
            VDevice value = new VDevice();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.Labeling = reader.GetString(3);
            value.DtlCdesc = reader.GetString(4);
            value.DtlEdesc = reader.GetString(5);
            value.IpAddr = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.PmcGrpNum = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.PmcPntNum = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.LocId = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.MuId = reader.IsDBNull(10) ? "" : reader.GetString(10);
            value.TypeCdesc = reader.GetString(11);
            value.TypeEdesc = reader.GetString(12);
            return value;
        }
    }
}
