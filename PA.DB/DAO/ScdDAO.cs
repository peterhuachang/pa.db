﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ScdDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sc_id,bit_id,bit_type,dvc_type,dvc_id,point_id,cr_user,cr_date,userstamp,datestamp from pa_tb_scd";

        private NpgsqlConnection conn;

        public ScdDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Scd SearchPK(string sttnId, string scId, int bitId, string bitType)
        {
            Scd value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sc_id=:sc_id and bit_id=:bit_id and bit_type=:bit_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            cmd.Parameters.AddWithValue("bit_id", bitId);
            cmd.Parameters.AddWithValue("bit_type", bitType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<Scd> SearchSttn(string sttnId)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Scd> SearchBitType(string sttnId, string bitType)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bit_type=:bit_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bit_type", bitType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Scd> SearchBitDeviceType(string sttnId, string bitType, string dvcType)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bit_type=:bit_type and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bit_type", bitType);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Scd> SearchBitTypeScId(string sttnId, string bitType, string scId)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bit_type=:bit_type and sc_id=:sc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bit_type", bitType);
            cmd.Parameters.AddWithValue("sc_id", scId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Scd> SearchDvcTypeScId(string sttnId, string dvcType, string scId)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and sc_id=:sc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("sc_id", scId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Scd> Search(string sttnId, string scId)
        {
            List<Scd> values = new List<Scd>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sc_id=:sc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sc_id", scId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Scd Convert(NpgsqlDataReader reader)
        {
            Scd value = new Scd();
            value.SttnId = reader.GetString(0);
            value.ScId = reader.GetString(1);
            value.BitId = reader.GetInt32(2);
            value.BitType = reader.GetString(3);
            value.DvcType = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.DvcId = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.PointId = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
