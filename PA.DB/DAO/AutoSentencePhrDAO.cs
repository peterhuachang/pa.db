﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoSentencePhrDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,timing,lang,sentence_seq,phrase_seq,phrase_type,phrase,cr_user,cr_date,userstamp,datestamp from pa_tb_auto_sentence_phr";

        internal const string SQL_INSERT = "insert into pa_tb_auto_sentence_phr(sttn_id,pa_id,timing,lang,sentence_seq,phrase_seq,phrase_type,phrase,cr_user,cr_date,userstamp,datestamp)values(:sttn_id,:pa_id,:timing,:lang,:sentence_seq,:phrase_seq,:phrase_type,:phrase,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_auto_sentence_phr set phrase_type=:phase_type,phrase=:phrase,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_auto_sentence_phr";

        private NpgsqlConnection conn;

        public AutoSentencePhrDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(AutoSentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.Parameters.AddWithValue("phrase_seq", value.PhraseSeq);
            cmd.Parameters.AddWithValue("phrase_type", value.PhraseType);
            cmd.Parameters.AddWithValue("phrase", value.Phrase);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("CR_DATE", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public void Update(AutoSentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and lang=:lang and sentence_seq=:sentence_seq and phrase_seq=:phrase_seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.Parameters.AddWithValue("phrase_seq", value.PhraseSeq);
            cmd.Parameters.AddWithValue("phrase_type", value.PhraseType);
            cmd.Parameters.AddWithValue("phrase", value.Phrase);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);            
            cmd.ExecuteNonQuery();
        }

        public void Delete(AutoSentencePhr value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and lang=:lang and sentence_seq=:sentence_seq and phrase_seq=:phrase_seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.Parameters.AddWithValue("phrase_seq", value.PhraseSeq);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, int timing)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and timing=:timing";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string PaId, int timing, string lang, int sentenceSeq, int phraseSeq)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and lang=:lang and sentence_seq=:sentence_seq and phrase_seq=:phrase_seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.Parameters.AddWithValue("lang", lang);
            cmd.Parameters.AddWithValue("sentence_seq", sentenceSeq);
            cmd.Parameters.AddWithValue("phrase_seq", phraseSeq);
            cmd.ExecuteNonQuery();
        }

        public AutoSentencePhr SearchPK(string sttnId, string PaId, int timing, string lang, int sentenceSeq, int phraseSeq)
        {
            AutoSentencePhr value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and lang=:lang and sentence_seq=:sentence_seq and phrase_seq=:phrase_seq";

            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.Parameters.AddWithValue("lang", lang);
            cmd.Parameters.AddWithValue("sentence_seq", sentenceSeq);
            cmd.Parameters.AddWithValue("phrase_seq", phraseSeq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AutoSentencePhr> Search(string sttnId, string PaId, int timing)
        {
            List<AutoSentencePhr> values = new List<AutoSentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing order by sentence_seq, lang, phrase_seq asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoSentencePhr> Search(string sttnId, string PaId, int timing, int sentenceSeq)
        {
            List<AutoSentencePhr> values = new List<AutoSentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and sentence_seq=:sentence_seq order by lang, phrase_seq asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.Parameters.AddWithValue("sentence_seq", sentenceSeq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoSentencePhr> Search(string sttnId, string PaId, int timing, int sentenceSeq ,string lang)
        {
            List<AutoSentencePhr> values = new List<AutoSentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and sentence_seq=:sentence_seq and lang=:lang order by lang, phrase_seq asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.Parameters.AddWithValue("sentence_seq", sentenceSeq);
            cmd.Parameters.AddWithValue("lang", lang);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoSentencePhr> SearchSttn(string sttnId)
        {
            List<AutoSentencePhr> values = new List<AutoSentencePhr>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id order by timing, sentence_seq, lang, phrase_seq asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoSentencePhr Convert(NpgsqlDataReader reader)
        {
            AutoSentencePhr value = new AutoSentencePhr();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.Timing = reader.GetInt32(2);
            value.Lang = reader.GetString(3);
            value.SentenceSeq = reader.GetInt32(4);
            value.PhraseSeq = reader.GetInt32(5);
            value.PhraseType = reader.GetInt32(6);
            value.Phrase = reader.GetString(7);
            value.CrUser = reader.GetString(8);
            value.CrDate = reader.GetDateTime(9);
            value.UserStamp = reader.IsDBNull(10) ? "" : reader.GetString(10);
            value.DateStamp = reader.IsDBNull(11) ? (DateTime?)null : reader.GetDateTime(11);
            return value;
        }
    }
}
