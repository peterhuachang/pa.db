﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VParamTypeValueDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,param_type,param_id,param_val,type_cdesc,type_edesc,value_cdesc,value_edesc from pa_v_param_type_value";

        private NpgsqlConnection conn;

        public VParamTypeValueDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VParamTypeValue SearchId(int id)
        {
            VParamTypeValue value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id ";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public VParamTypeValue SearchPK(int id, string sttn_id, string param_type)
        {
            VParamTypeValue value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id and sttn_id=:sttn_id and param_type=:param_type";
            cmd.Parameters.AddWithValue("id", id);
            cmd.Parameters.AddWithValue("sttn_id", sttn_id);
            cmd.Parameters.AddWithValue("param_type", param_type);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public VParamTypeValue Search(string sttn_id, string param_type, string param_id)
        {
            VParamTypeValue value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and param_type=:param_type and param_id=:param_id";
            cmd.Parameters.AddWithValue("sttn_id", sttn_id);
            cmd.Parameters.AddWithValue("param_type", param_type);
            cmd.Parameters.AddWithValue("param_id", param_id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public IList<VParamTypeValue> Search(string sttn_id, string param_type)
        {
            List<VParamTypeValue> values = new List<VParamTypeValue>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and param_type=:param_type";
            cmd.Parameters.AddWithValue("sttn_id", sttn_id);
            cmd.Parameters.AddWithValue("param_type", param_type);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VParamTypeValue> SearchSttn(string sttnId)
        {
            List<VParamTypeValue> values = new List<VParamTypeValue>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VParamTypeValue Convert(NpgsqlDataReader reader)
        {
            VParamTypeValue value = new VParamTypeValue();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.ParamType = reader.GetString(2);
            value.ParamId = reader.GetString(3);
            value.ParamVal = reader.GetString(4);
            value.TypeCdesc = reader.GetString(5);
            value.TypeEdesc = reader.GetString(6);
            value.ValueCdesc = reader.GetString(7);
            value.TypeEdesc = reader.GetString(8);
            return value;
        }
    }
}
