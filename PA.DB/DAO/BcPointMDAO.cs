﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class BcPointMDAO
    {
        internal const string SQL_SELECT = "select sttn_id,point_id,voice_source,cdesc,edesc,dev_id,cr_user,cr_date,userstamp,datestamp from pa_tb_bc_pointm";

        internal const string SQL_INSERT = "insert into pa_tb_bc_pointm(id,sttn_id,point_id,voice_source,cdesc,edesc,dev_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_bc_pointm_id_seq'),:sttn_id,:point_id,:voice_source,:cdesc,:edesc,:dev_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_bc_pointm set sttn_id=:sttn_id,point_id=:point_id,voice_source=:voice_source,cdesc=:cdesc,edesc=:edesc,dev_id=:dev_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_bc_pointm";

        private NpgsqlConnection conn;

        public BcPointMDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(BcPointM value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("Sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("POINT_id", value.PointId);
            cmd.Parameters.AddWithValue("VOICE_SOURCE", value.VoiceSource);
            cmd.Parameters.AddWithValue("CDESC", value.Cdesc);
            cmd.Parameters.AddWithValue("EDESC", value.Edesc);
            cmd.Parameters.AddWithValue("DEV_id", value.DevId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_bc_pointm_id_SEQ')";
            //value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(BcPointM value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("Sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("POINT_id", value.PointId);
            cmd.Parameters.AddWithValue("VOICE_SOURCE", value.VoiceSource);
            cmd.Parameters.AddWithValue("CDESC", value.Cdesc);
            cmd.Parameters.AddWithValue("EDESC", value.Edesc);
            cmd.Parameters.AddWithValue("DEV_id", value.DevId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public void Delete(BcPointM value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and point_id=:point_id and voice_source =:voice_source dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("voice_source", value.VoiceSource);
            cmd.Parameters.AddWithValue("dev_id", value.DevId);
            cmd.ExecuteNonQuery();
        }

        public BcPointM SearchPK(string sttnId, string pointId, string voiceSource, string devId)
        {
            BcPointM value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and point_id=:point_id and voice_source =:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<BcPointM> Search(string sttnId, string voiceSource)
        {
            List<BcPointM> values = new List<BcPointM>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<BcPointM> SearchSttn(string sttnId)
        {
            List<BcPointM> values = new List<BcPointM>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<BcPointM> SearchDevArea(string voiceSource,string devId)
        {
            IList<BcPointM> values = new List<BcPointM>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where dev_id=:dev_id and voice_source=:voice_source";
            cmd.Parameters.AddWithValue("dev_id", devId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static BcPointM Convert(NpgsqlDataReader reader)
        {
            BcPointM value = new BcPointM();
            value.SttnId = reader.GetString(0);
            value.PointId = reader.GetString(1);
            value.VoiceSource = reader.GetString(2);
            value.Cdesc = reader.GetString(3);
            value.Edesc = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.DevId = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
