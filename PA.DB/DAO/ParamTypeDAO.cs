﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PA.DB;

namespace PA.DB.DAO
{
    public class ParamTypeDAO
    {
        internal const string SQL_SELECT = "select sttn_id,param_type,cdesc,edesc,cr_user,cr_date,userstamp,datestamp from pa_tb_param_type";

        private NpgsqlConnection conn;

        public ParamTypeDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public ParamType SearchPK(string sttnId, string paramType)
        {
            ParamType value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and param_type=:param_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("param_type", paramType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<ParamType> Search(string sttnId)
        {
            List<ParamType> values = new List<ParamType>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public ParamType SearchType(string paramType)
        {
            ParamType value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where param_type=:param_type";
            cmd.Parameters.AddWithValue("param_type", paramType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        internal static ParamType Convert(NpgsqlDataReader reader)
        {
            ParamType value = new ParamType();
            value.SttnId = reader.GetString(0);
            value.TypeName = reader.GetString(1);
            value.Cdesc = reader.GetString(2);
            value.Edesc = reader.GetString(3);
            value.CrUser = reader.GetString(4);
            value.CrDate = reader.GetDateTime(5);
            value.UserStamp = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DateStamp = reader.IsDBNull(7) ? (DateTime?)null : reader.GetDateTime(7);
            return value;
        }
    }
}
