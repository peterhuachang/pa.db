﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VCurrAutoBcDAO
    {
        internal const string SQL_SELECT = @"select sttn_id,train_no,orig_sttn_id,dest_sttn_id,run_direct,timing,train_status,platform,
                                             arrival_time,departure_time,dest_time,delay_time,stop,cmd_from from pa_v_curr_auto_bc ";
        private NpgsqlConnection conn;

        public VCurrAutoBcDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VCurrAutoBc SearchPK(string sttnId, string trainNo)
        {
            VCurrAutoBc value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<VCurrAutoBc> SearchSttn(string sttnId)
        {
            List<VCurrAutoBc> values = new List<VCurrAutoBc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VCurrAutoBc Convert(NpgsqlDataReader reader)
        {
            VCurrAutoBc value = new VCurrAutoBc();
            value.SttnId = reader.GetString(0);
            value.TrainNo = reader.GetString(1);
            value.OrigSttnId = reader.GetString(2);
            value.DestSttnId = reader.GetString(3);
            value.RunDirect = reader.GetInt32(4);
            value.Timing = reader.GetInt32(5);
            value.TrainStatus = reader.GetInt32(6);
            value.Platform = reader.GetString(7);
            value.ArrivalTime = reader.GetDateTime(8);
            value.DepartureTime = reader.GetDateTime(9);
            value.DestTime = reader.GetDateTime(10);
            value.DelayTime = reader.GetInt32(11);
            value.Stop = reader.GetString(12);
            value.CmdFrom = reader.GetString(13);
            return value;
        }
    }
}
