﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AssExtDAO
    {
        internal const string SQL_SELECT = "select sttn_id,as_id,min_channel,max_channel,bk_status,cr_uesr,cr_date,userstamp,datestamp from pa_tb_ass_ext";
        internal const string SQL_UPDATE = "update pa_tb_ass_ext set bk_status=:bk_status,userstamp=:userstamp,datestamp=:datestamp";

        private NpgsqlConnection conn;

        public AssExtDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public bool Update(AssExt value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and as_id=:as_id";
            cmd.Parameters.AddWithValue("bk_status", value.BkStatus);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("as_id", value.AsId);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public AssExt SearchPK(string sttnId, string asId)
        {
            AssExt value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_id=:as_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AssExt> SearchSttn(string sttnId)
        {
            List<AssExt> values = new List<AssExt>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id order by as_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AssExt Convert(NpgsqlDataReader reader)
        {
            AssExt value = new AssExt();
            value.SttnId = reader.GetString(0);
            value.AsId = reader.GetString(1);
            value.MinChannel = reader.GetInt32(2);
            value.MaxChannel = reader.GetInt32(4);
            value.BkStatus = reader.GetInt32(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
