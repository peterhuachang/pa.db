﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ProcessDAO
    {
        internal const string SQL_SELECT = "select id,exe_sttn,proc_name,que_name,proc_id,sttn_id,dvc_type,dvc_id,exe_seq,exe_file,run_flag,init_flag from pa_tb_process";
        internal const string SQL_UPDATE = "update pa_tb_process set proc_id=:proc_id,run_flag=:run_flag";

        private NpgsqlConnection conn;

        public ProcessDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Process SearchPK(int id)
        {
            Process value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Process> Search(string exeSttnId)
        {
            List<Process> values = new List<Process>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn order by sttn_id, exe_seq asc";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Process> Search(string exeSttnId, string sttnId)
        {
            List<Process> values = new List<Process>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and sttn_id=:sttn_id order by exe_seq asc";            
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Process> SearchProcess(string exeSttnId, bool runFlag, bool initFlag)
        {
            string run = runFlag ? "Y" : "N";
            string init = initFlag ? "Y" : "N";
            List<Process> values = new List<Process>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and run_flag=:run_flag and init_flag=:init_flag order by exe_seq asc";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            cmd.Parameters.AddWithValue("run_flag", run);
            cmd.Parameters.AddWithValue("init_flag", init);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Process> SearchRunningProcess(string exeSttnId)
        {
            List<Process> values = new List<Process>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and proc_id <> '' order by exe_seq asc";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Process SearchProcess(string exeSttnId, string dvcType, string dvcId)
        {
            Process value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public Process SearchProcess(string exeSttnId, string procName)
        {
            Process value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where exe_sttn=:exe_sttn and proc_name=:proc_name";
            cmd.Parameters.AddWithValue("exe_sttn", exeSttnId);
            cmd.Parameters.AddWithValue("proc_name", procName);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public bool UpdateProcess(Process value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where id=:id";
            cmd.Parameters.AddWithValue("proc_id", value.ProcId);
            cmd.Parameters.AddWithValue("run_flag", value.RunFlag);
            cmd.Parameters.AddWithValue("id", value.Id);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static Process Convert(NpgsqlDataReader reader)
        {
            Process value = new Process();
            value.Id = reader.GetInt32(0);
            value.ExeSttn = reader.GetString(1);
            value.ProcName = reader.GetString(2);
            value.QueName = reader.GetString(3);
            value.ProcId = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.SttnId = reader.GetString(5);
            value.DvcType = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.DvcId = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.ExeSeq = reader.GetInt32(8);
            value.ExeFile = reader.GetString(9);
            value.RunFlag = reader.GetString(10);
            value.InitFlag = reader.GetString(11);
            return value;
        }
    }
}
