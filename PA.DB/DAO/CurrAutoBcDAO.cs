﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class CurrAutoBcDAO
    {
        internal const string SQL_SELECT = "select sttn_id,train_no,platform,timing,train_status,cmd_from from pa_tb_curr_auto_bc ";
        internal const string SQL_INSERT = @"insert into pa_tb_curr_auto_bc (sttn_id,train_no,platform,timing,train_status,cmd_from) 
                                            values(:sttn_id,:train_no,:platform,:timing,:train_status,:cmd_from) ";
        internal const string SQL_UPDATE = "update pa_tb_curr_auto_bc set platform=:platform,timing=:timing,train_status=:train_status,cmd_from=:cmd_from ";
        internal const string SQL_DELETE = "delete from pa_tb_curr_auto_bc ";

        private NpgsqlConnection conn;

        public CurrAutoBcDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(CurrAutoBc value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.Parameters.AddWithValue("platform", value.Platform);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("train_status", value.TrainStatus);
            cmd.Parameters.AddWithValue("cmd_from", value.CmdFrom);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_curr_bc_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public bool Update(CurrAutoBc value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public void Delete(CurrAutoBc value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string trainNo)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and train_no=:train_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void DeleteAll()
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE;
            cmd.ExecuteNonQuery();
        }

        public CurrAutoBc SearchPK(string sttnId, string trainNo)
        {
            CurrAutoBc value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<CurrAutoBc> SearchSttn(string sttnId)
        {
            List<CurrAutoBc> values = new List<CurrAutoBc>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static CurrAutoBc Convert(NpgsqlDataReader reader)
        {
            CurrAutoBc value = new CurrAutoBc();
            value.SttnId = reader.GetString(0);
            value.TrainNo = reader.GetString(1);
            value.Platform = reader.GetString(2);
            value.Timing = reader.GetInt32(3);
            value.TrainStatus = reader.GetInt32(4);
            value.CmdFrom = reader.IsDBNull(5) ? "" : reader.GetString(5);
            return value;
        }
    }
}
