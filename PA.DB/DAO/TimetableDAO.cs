﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class TimetableDAO
    {
        internal const string SQL_SELECT = @"select sttn_id,train_no,orig_sttn_id,dest_sttn_id,run_direct,timing,train_status,platform,
                                             arrival_time,departure_time,dest_time,delay_time,stop from pa_tb_timetable ";
        internal const string SQL_INSERT = @"insert into pa_tb_timetable (sttn_id,train_no,orig_sttn_id,dest_sttn_id,run_direct,
                                             timing,train_status,platform,arrival_time,departure_time,dest_time,delay_time,stop) 
                                            values(:sttn_id,:train_no,:orig_sttn_id,:dest_sttn_id,:run_direct,:timing,:train_status,
                                                   :platform,:arrival_time,:departure_time,:dest_time,:delay_time,:stop) ";
        internal const string SQL_UPDATE = @"update pa_tb_timetable set orig_sttn_id=:orig_sttn_id,dest_sttn_id=:dest_sttn_id,
                                             run_direct=:run_direct,timing=:timing,train_status=:train_status,platform=:platform,
                                             arrival_time=:arrival_time,departure_time=:departure_time,dest_time=:dest_time,
                                             delay_time=:delay_time,stop=:stop ";
        internal const string SQL_DELETE = "delete from pa_tb_timetable ";

        private NpgsqlConnection conn;

        public TimetableDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(Timetable value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.Parameters.AddWithValue("orig_sttn_id", value.OrigSttnId);
            cmd.Parameters.AddWithValue("dest_sttn_id", value.DestSttnId);
            cmd.Parameters.AddWithValue("run_direct", value.RunDirect);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("train_status", value.TrainStatus);
            cmd.Parameters.AddWithValue("platform", value.Platform);
            cmd.Parameters.AddWithValue("arrival_time", value.ArrivalTime);
            cmd.Parameters.AddWithValue("departure_time", value.DepartureTime);
            cmd.Parameters.AddWithValue("dest_time", value.DestTime);
            cmd.Parameters.AddWithValue("delay_time", value.DelayTime);
            cmd.Parameters.AddWithValue("stop", value.Stop);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_curr_bc_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public bool Update(Timetable value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.Parameters.AddWithValue("orig_sttn_id", value.OrigSttnId);
            cmd.Parameters.AddWithValue("dest_sttn_id", value.DestSttnId);
            cmd.Parameters.AddWithValue("run_direct", value.RunDirect);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("train_status", value.TrainStatus);
            cmd.Parameters.AddWithValue("platform", value.Platform);
            cmd.Parameters.AddWithValue("arrival_time", value.ArrivalTime);
            cmd.Parameters.AddWithValue("departure_time", value.DepartureTime);
            cmd.Parameters.AddWithValue("dest_time", value.DestTime);
            cmd.Parameters.AddWithValue("delay_time", value.DelayTime);
            cmd.Parameters.AddWithValue("stop", value.Stop);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public void Delete(Timetable value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string trainNo)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and train_no=:train_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void DeleteAll()
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE;
            cmd.ExecuteNonQuery();
        }

        public Timetable SearchPK(string sttnId, string trainNo)
        {
            Timetable value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Timetable> SearchSttn(string sttnId)
        {
            List<Timetable> values = new List<Timetable>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Timetable> SearchNonDepartureTrain(string sttnId)
        {
            List<Timetable> values = new List<Timetable>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and timing < 6 and train_status > 0 order by arrival_time, departure_time ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Timetable Convert(NpgsqlDataReader reader)
        {
            Timetable value = new Timetable();
            value.SttnId = reader.GetString(0);
            value.TrainNo = reader.GetString(1);
            value.OrigSttnId = reader.GetString(2);
            value.DestSttnId = reader.GetString(3);
            value.RunDirect = reader.GetInt32(4);
            value.Timing = reader.GetInt32(5);
            value.TrainStatus = reader.GetInt32(6);
            value.Platform = reader.GetString(7);
            value.ArrivalTime = reader.GetDateTime(8);
            value.DepartureTime = reader.GetDateTime(9);
            value.DestTime = reader.GetDateTime(10);
            value.DelayTime = reader.GetInt32(11);
            value.Stop = reader.GetString(12);
            return value;
        }
    }
}
