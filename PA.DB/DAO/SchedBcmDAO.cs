﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SchedBcmDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,p_key,start_time,end_time,end_timea,cname,ename,voice_source,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sched_bcm";

        private NpgsqlConnection conn;

        public SchedBcmDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SchedBcm SearchById(int id)
        {
            SchedBcm value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SchedBcm> SearchAll()
        {
            List<SchedBcm> values = new List<SchedBcm>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SchedBcm Convert(NpgsqlDataReader reader)
        {
            SchedBcm value = new SchedBcm();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.Pkey = reader.GetInt32(2);
            value.StartTime = reader.GetDateTime(3);
            value.EndTime = reader.GetDateTime(4);
            value.EndTimeA = reader.GetDateTime(5);
            value.Cname = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.Ename = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.VoiceSource = reader.GetString(8);
            value.DvcId = reader.GetString(9);
            value.CrUser = reader.GetString(10);
            value.CrDate = reader.GetDateTime(11);
            value.Userstamp = reader.IsDBNull(12) ? "" : reader.GetString(12);
            value.Datestamp = reader.IsDBNull(13) ? (DateTime?)null : reader.GetDateTime(13);
            return value;
        }
    }
}
