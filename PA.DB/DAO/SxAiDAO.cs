﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class SxAiDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ai_id,ch_no,voice_source,dev_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ai";

        private NpgsqlConnection conn;

        public SxAiDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SxAi SearchPK(string sttnId, string voiceSource, string devId)
        {
            SxAi value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + @" where sttn_id=:sttn_id and voice_source=:voice_source and dev_id=:dev_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("voice_source", voiceSource);
            cmd.Parameters.AddWithValue("dev_id", devId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<SxAi> SearchSttn(string sttnId)
        {
            List<SxAi> values = new List<SxAi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAi> SearchBySxId(string sttnId, string sxId)
        {
            List<SxAi> values = new List<SxAi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAi> SearchByAiId(string sttnId, string sxId, string aiId)
        {
            List<SxAi> values = new List<SxAi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAi> SearchByAiNo(string sttnId, string sxId, string aiId, string chNo)
        {
            List<SxAi> values = new List<SxAi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id and ch_no=:ch_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            cmd.Parameters.AddWithValue("ch_no", chNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SxAi Convert(NpgsqlDataReader reader)
        {
            SxAi value = new SxAi();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AiId = reader.GetString(2);
            value.ChNo = reader.GetInt32(3);
            value.VoiceSource = reader.GetString(4);
            value.DevId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
