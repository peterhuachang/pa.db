﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SchedBcPaDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,p_key,idx_id,seq,cr_user,cr_date,userstamp,datestamp from pa_tb_sched_bc_pa";

        private NpgsqlConnection conn;

        public SchedBcPaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SchedBcPa SearchById(int id)
        {
            SchedBcPa value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SchedBcPa> SearchAll()
        {
            List<SchedBcPa> values = new List<SchedBcPa>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SchedBcPa Convert(NpgsqlDataReader reader)
        {
            SchedBcPa value = new SchedBcPa();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.Pkey = reader.GetInt32(2);
            value.IdxId = reader.GetString(3);
            value.Seq = reader.GetInt32(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.Userstamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.Datestamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
