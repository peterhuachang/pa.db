﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
   public class PlatformDAO
    {
        internal const string SQL_SELECT = "select sttn_id,platform_no,voice_source,dev_id from pa_tb_platform";

        private NpgsqlConnection conn;

        public PlatformDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Platform SearchPK(string sttnId, string platformNo)
        {
            Platform value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and platform_no=:platform_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("platform_no", platformNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Platform> Search(string sttnId)
        {
            List<Platform> values = new List<Platform>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT +" where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Platform Convert(NpgsqlDataReader reader)
        {
            Platform value = new Platform();
            value.SttnId = reader.GetString(0);
            value.PlatformNo = reader.GetString(1);
            value.VoiceSource = reader.GetString(2);
            value.DevId = reader.GetString(3);
            return value;
        }
    }
}
