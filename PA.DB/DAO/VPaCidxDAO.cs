﻿using Npgsql;
using PA.DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VPaCidxDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,idx_id,name,card_id,cdesc,edesc,time_len,standard,lang,cr_user,cr_date,userstamp,datestamp from pa_v_pa_cidx";

        private NpgsqlConnection conn;

        public VPaCidxDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VPaCidx SearchPK(string sttnId, string paId, string idxId)
        {
            VPaCidx value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and idx_id=:idx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("idx_id", idxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<VPaCidx> Search(string sttnId)
        {
            List<VPaCidx> values = new List<VPaCidx>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VPaCidx> Search(string sttnId, string paId, string cardId)
        {
            List<VPaCidx> values = new List<VPaCidx>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and card_id=:card_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            cmd.Parameters.AddWithValue("card_id", cardId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IDictionary<string, IList<VPaCidx>> Search(string sttnId, string paId)
        {
            Dictionary<string, IList<VPaCidx>> values = new Dictionary<string, IList<VPaCidx>>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", paId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                VPaCidx cidx = Convert(reader);
                IList<VPaCidx> result;
                if (!values.TryGetValue(cidx.CardId, out result))
                {
                    result = new List<VPaCidx>();
                    values[cidx.CardId] = result;
                }
                result.Add(cidx);
            }
            reader.Close();

            return values;
        }

        internal static VPaCidx Convert(NpgsqlDataReader reader)
        {
            VPaCidx value = new VPaCidx();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.IdxId = reader.GetString(2);
            value.Name = reader.GetString(3);
            value.CardId = reader.GetString(4);
            value.Cdesc = reader.GetString(5);
            value.Edesc = reader.GetString(6);
            value.TimeLen = reader.GetInt32(7);
            value.Standard = reader.GetString(8);
            value.Lang = reader.GetString(9);
            value.CrUser = reader.GetString(10);
            value.CrDate = reader.GetDateTime(11);
            value.UserStamp = reader.IsDBNull(12) ? "" : reader.GetString(12);
            value.DateStamp = reader.IsDBNull(13) ? (DateTime?)null : reader.GetDateTime(13);
            return value;
        }
    }
}


/**
 *-- Function: after_delete_pa_tb_pa_cidx()

-- DROP FUNCTION after_delete_pa_tb_pa_cidx();

CREATE OR REPLACE FUNCTION after_delete_pa_tb_pa_cidx()
  RETURNS trigger AS
$BODY$
    DECLARE   
        v_pattn record;  
    BEGIN
        IF (TG_OP = 'DELETE') THEN
				
		FOR v_pattn IN 	SELECT a.STTN_ID, b.PATTN_ID
				FROM    PA_TB_SENTENCE a, PA_TB_SENTENCE_PHR b
				WHERE a.STTN_ID = b.STTN_ID
				AND   a.STTN_ID = OLD.STTN_ID
				AND   b.IDX_ID = OLD.IDX_ID 
				AND   a.PA_ID = OLD.PA_ID
				AND   a.SENTEN_ID = b.SENTEN_ID LOOP
			IF FOUND THEN 			
				DELETE FROM PA_TB_SCHED_PATTN_SET a		
				WHERE a.STTN_ID  = v_pattn.STTN_ID
				AND   a.SENTEN_ID = v_pattn.SENTEN_ID;
			END IF;
		END LOOP;
		
        END IF;
        RETURN NULL; 
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION after_delete_pa_tb_pa_cidx()
  OWNER TO pa;

 */

