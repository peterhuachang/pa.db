﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DvcAlarmHistDAO
    {
        internal const string SQL_SELECT = @"select id,sttn_id,dvc_type,dvc_id,alarm_id,alarm_flag,alarm_time,
                                             cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_alarm_hist";
        internal const string SQL_INSERT = @"insert into pa_tb_dvc_alarm_hist (id,sttn_id,dvc_type,dvc_id,alarm_id,
                                             alarm_flag,alarm_time,cr_user,cr_date,userstamp,datestamp) 
                                             values(nextval('pa_tb_dvc_alarm_hist_id_seq'),:sttn_id,:dvc_type,:dvc_id,:alarm_id,
                                             :alarm_flag,:alarm_time,:cr_user,:cr_date,:userstamp,:datestamp)";

        private NpgsqlConnection conn;

        public DvcAlarmHistDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(DvcAlarmHist value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("dvc_type", value.DvcType);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("alarm_id", value.AlarmId);
            cmd.Parameters.AddWithValue("alarm_flag", value.AlarmFlag);
            cmd.Parameters.AddWithValue("alarm_time", value.AlarmTime);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", DateTime.Now);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_dvc_alarm_hist_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public DvcAlarmHist SearchPK(int id)
        {
            DvcAlarmHist value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<DvcAlarmHist> Search(String sttnId)
        {
            List<DvcAlarmHist> values = new List<DvcAlarmHist>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static DvcAlarmHist Convert(NpgsqlDataReader reader)
        {
            DvcAlarmHist value = new DvcAlarmHist();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.DvcType = reader.GetString(2);
            value.DvcId = reader.GetString(3);
            value.AlarmId = reader.GetString(4);
            value.AlarmFlag = reader.GetInt32(5);
            value.AlarmTime = reader.GetDateTime(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
