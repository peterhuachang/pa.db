﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class UserDAO
    {
        internal const string SQL_SELECT = "select user_id,pwd,cname,ename,grade,language,zone,cr_user,cr_date,userstamp,datestamp from pa_tb_user";

        private NpgsqlConnection conn;

        public UserDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public User SearchPKey(string user_id)
        {
            User value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where user_id=:user_id";
            cmd.Parameters.AddWithValue("user_id", user_id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<User> SearchAll()
        {
            List<User> values = new List<User>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static User Convert(NpgsqlDataReader reader)
        {
            User value = new User();
            value.UserId = reader.GetString(0);
            value.Pwd = reader.GetString(1);
            value.Cname = reader.GetString(2);
            value.Ename = reader.GetString(3);
            value.Grade = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.Language = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.Zone = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.Userstamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.Datestamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
