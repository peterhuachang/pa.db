﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VAsAfDAO
    {
        internal const string SQL_SELECT = "select sttn_id,as_idp,as_idc,idnum,as_status,as_id,seq,min_channel,max_channel,bk_status,sx_id,ao_id,co_no,status from pa_v_as_af";

        private NpgsqlConnection conn;

        public VAsAfDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VAsAf SearchPK(string sttnId, string asId, int seq)
        {
            VAsAf value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_id=:as_id and seq=:seq";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            cmd.Parameters.AddWithValue("seq", seq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<VAsAf> SearchSttn(string sttnId)
        {
            List<VAsAf> values = new List<VAsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAsAf> SearchByAsId(string sttnId, string asId)
        {
            List<VAsAf> values = new List<VAsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_id=:as_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_id", asId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAsAf> SearchBySxId(string sttnId, string sxId)
        {
            List<VAsAf> values = new List<VAsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAsAf> SearchByAoId(string sttnId, string sxId, string aoId)
        {
            List<VAsAf> values = new List<VAsAf>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VAsAf Convert(NpgsqlDataReader reader)
        {
            VAsAf value = new VAsAf();
            value.SttnId = reader.GetString(0);
            value.AsIdp = reader.GetString(1);
            value.AsIdc = reader.GetString(2);
            value.IdNum = reader.GetString(3);
            value.AsStatus = reader.GetDecimal(4);           
            value.AsId = reader.GetString(5);
            value.Seq = reader.GetDecimal(6);
            value.MinChannel = reader.GetInt32(7);
            value.MaxChannel = reader.GetInt32(8);
            value.BkStatus = reader.GetInt32(9);
            value.SxId = reader.GetString(10);
            value.AoId = reader.GetString(11);
            value.CoNo = reader.GetInt32(12);
            value.Status = reader.GetInt32(13);
            return value;
        }
    }
}
