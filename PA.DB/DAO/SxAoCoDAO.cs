﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SxAoCoDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ao_id,co_no,dvc_type,dvc_id,status,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ao_co ";
        internal const string SQL_UPDATE = "update pa_tb_sx_ao_co set status=:status,userstamp=:userstamp,datestamp=datestamp";

        private NpgsqlConnection conn;

        public SxAoCoDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SxAoCo SearchPK(string sttnId, string sxId, string aoId, int coNo)
        {
            SxAoCo value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and co_no=:co_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("co_no", coNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<SxAoCo> SearchSttn(string sttnId)
        {
            List<SxAoCo> values = new List<SxAoCo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCo> SearchBySxId(string sttnId, string sxId)
        {
            List<SxAoCo> values = new List<SxAoCo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCo> SearchByAoId(string sttnId, string sxId, string aoId)
        {
            List<SxAoCo> values = new List<SxAoCo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCo> SearchByDvcType(string sttnId, string dvcType)
        {
            List<SxAoCo> values = new List<SxAoCo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type order by dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCo> SearchByDevice(string sttnId, string dvcType, string dvcId)
        {
            List<SxAoCo> values = new List<SxAoCo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public bool Update(SxAoCo value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and co_no=:co_no";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("sx_id", value.SxId);
            cmd.Parameters.AddWithValue("ao_id", value.AoId);
            cmd.Parameters.AddWithValue("co_no", value.CoNo);
            cmd.Parameters.AddWithValue("status", value.Status);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public bool Update(string sttnId, string sxId, string aoId, int coNo, int status)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and co_no=:co_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("co_no", coNo);
            cmd.Parameters.AddWithValue("status", status);
            cmd.Parameters.AddWithValue("userstamp", "Admin");
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static SxAoCo Convert(NpgsqlDataReader reader)
        {
            SxAoCo value = new SxAoCo();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AoId = reader.GetString(2);
            value.CoNo = reader.GetInt32(3);
            value.DvcType = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.Status = reader.GetInt32(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
