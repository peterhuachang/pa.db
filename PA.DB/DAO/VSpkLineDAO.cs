﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VSpkLineDAO
    {
        internal const string SQL_SELECT = "select sttn_id,zone_id,sp_id,sl_id,sttn_cname,sttn_ename,zone_cname,zone_ename,zone_bc_status,ss_id,cb_id,bc_port_id,sl_bc_status from pa_v_spkline";

        private NpgsqlConnection conn;

        public VSpkLineDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VSpkLine SearchPK(string sttnId, string slId)
        {
            VSpkLine value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public VSpkLine SearchPK(string sttnId, string zoneId, string spId)
        {
            VSpkLine value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id and sp_id=:sp_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            cmd.Parameters.AddWithValue("sp_id", spId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();

            return value;
        }

        public IList<VSpkLine> SearchAll()
        {
            List<VSpkLine> values = new List<VSpkLine>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VSpkLine> Search(string sttnId, string zoneId)
        {
            List<VSpkLine> values = new List<VSpkLine>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VSpkLine> SearchBySttnId(string sttnId)
        {
            List<VSpkLine> values = new List<VSpkLine>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VSpkLine Convert(NpgsqlDataReader reader)
        {
            VSpkLine value = new VSpkLine();
            value.SttnId = reader.GetString(0);
            value.ZoneId = reader.GetString(1);
            value.SpId = reader.GetString(2);
            value.SlId = reader.GetString(3);
            value.SttnCname = reader.GetString(4);
            value.SttnEname = reader.GetString(5);
            value.ZoneCname = reader.GetString(6);
            value.ZoneEname = reader.GetString(7);
            value.ZoneBcStatus = reader.GetInt32(8);
            value.SsId = reader.GetString(9);
            value.CbId = reader.GetString(10);
            value.BcPortId = reader.GetInt32(11);
            value.SlBcStatus = reader.GetInt32(12);
            return value;
        }
    }
}
