﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Data;

namespace PA.DB.DAO
{
    public class PASession : IDisposable
    {
        private NpgsqlConnection conn;
        private string connectionString;
        private Dictionary<int, NpgsqlTransaction> trans;

        public PASession()
        {
            // "Server=192.168.0.100;Port=5432;User id=pa;Password=pa;Database=padb;";
            this.conn = new NpgsqlConnection(ConfigurationManager.AppSettings["PA.DB.CONN"]);
            this.trans = new Dictionary<int, NpgsqlTransaction>();
            conn.Open();
        }

        public PASession(string dbServer, string dbName, string dbUser, string dbPwd)
        {
            this.connectionString = String.Format("Server={0};Port=5432;User id={1};Password={2};Database={3};", dbServer, dbUser, dbPwd, dbName);
            this.conn = new NpgsqlConnection(connectionString);
            this.trans = new Dictionary<int, NpgsqlTransaction>();
            conn.Open();
        }

        public bool Open()
        {
            try
            {
                if (this.conn.State == ConnectionState.Closed || this.conn.State == ConnectionState.Broken)
                {
                    this.conn.Open();
                }

                return true;
            }
            catch (Exception ex)
            {
                //this.conn = null;
                throw new Exception(ex.Message);
            }
        }

        public void Close()
        {
            try
            {
                this.conn.Close();
            }
            catch (Exception ex)
            {
                //this.conn = null;
                throw new Exception(ex.Message);
            }
        }

        public int BeginTrans()
        {
            NpgsqlTransaction nt = this.conn.BeginTransaction();            
            int key = 0;
            Random rm = new Random();
            while (this.trans.ContainsKey(key))
            {
                key = rm.Next(0, 100);
            }

            this.trans.Add(key, nt);
            return key;
        }

        public void Commit(int id)
        {
            if (!IsDbOpened())
            {
                return;
            }

            if (this.trans.ContainsKey(id))
            {
                this.trans[id].Commit();
                this.trans[id].Dispose();
                this.trans.Remove(id);
            }
        }

        public void Rollback(int id)
        {
            if (!IsDbOpened())
            {
                return;
            }

            if (this.trans.ContainsKey(id))
            {
                this.trans[id].Rollback();
                this.trans[id].Dispose();
                this.trans.Remove(id);
            }
        }

        public bool IsDbOpened()
        {
            if (this.conn.State == ConnectionState.Closed || this.conn.State == ConnectionState.Broken || this.conn.State == ConnectionState.Connecting)
            {
                return false;
            }

            return true;
        }

        public T CreateDao<T>(string name)
        {
            //string className = "PA.DB.DAO." + name + "DAO,PA.DB";      
            string className = "PA.DB.DAO." + name + "DAO";  
            Type drv = Type.GetType(className);
            if (drv == null)
            {
                throw new PaDbException("Dao:" + name + " not found.");
            }
            try
            {
                return (T)Activator.CreateInstance(drv, this.conn);
            }
            catch (Exception ex)
            {
                throw new PaDbException("Dao:" + name + " create instance failure. " + typeof(T), ex);
            }

        }

        public void Dispose()
        {
            Close();
        }
    }
}
