﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class RtsDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,dvc_type,dvc_id,area_id,idx_id,cr_user,cr_date,userstamp,datestamp from pa_tb_rts";

        private NpgsqlConnection conn;

        public RtsDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Rts SearchById(int id)
        {
            Rts value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Rts> SearchAll()
        {
            List<Rts> values = new List<Rts>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Rts Convert(NpgsqlDataReader reader)
        {
            Rts value = new Rts();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.DvcType = reader.GetString(2);
            value.DvcId = reader.GetString(3);
            value.AreaId = reader.GetString(4);
            value.IdxId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.Userstamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.Datestamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
