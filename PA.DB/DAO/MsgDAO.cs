﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class MsgDAO
    {
        internal const string SQL_SELECT = "select msg_id,cdesc,edesc,cr_user,cr_date,userstamp,datestamp from pa_tb_msg";

        private NpgsqlConnection conn;

        public MsgDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Msg SearchPK(string msgId)
        {
            Msg value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where msg_id=:msg_id";
            cmd.Parameters.AddWithValue("msg_id", msgId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Msg> SearchAll()
        {
            List<Msg> values = new List<Msg>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Msg Convert(NpgsqlDataReader reader)
        {
            Msg value = new Msg();
            value.MsgId = reader.GetString(0);
            value.Cdesc = reader.GetString(1);
            value.Edesc = reader.IsDBNull(2) ? "" : reader.GetString(2);
            value.CrUser = reader.GetString(3);
            value.CrDate = reader.GetDateTime(4);
            value.UserStamp = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DateStamp = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
            return value;
        }
    }
}
