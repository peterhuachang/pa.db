﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SlBstaDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sl_id,ss_id,sp_id,cb_id,bc_port_id,bc_status,cr_user,cr_date,userstamp,datestamp from pa_tb_sl_bsta";
        internal const string SQL_UPDATE = "update pa_tb_sl_bsta set bc_status=:bc_status,userstamp=:userstamp,datestamp=datestamp";

        private NpgsqlConnection conn;

        public SlBstaDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public IList<SlBsta> Search()
        {
            List<SlBsta> values = new List<SlBsta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public SlBsta SearchPK(string sttnId, string slId)
        {
            SlBsta value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<SlBsta> SearchSttn(string sttnId)
        {
            List<SlBsta> values = new List<SlBsta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SlBsta> SearchSPId(string sttnId, string spId)
        {
            List<SlBsta> values = new List<SlBsta>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sp_id=:sp_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sp_id", spId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public bool Update(SlBsta value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("sl_id", value.SlId);
            cmd.Parameters.AddWithValue("bc_status", value.BcStatus);            
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public bool UpdateAll(string sttnId, int status, string userstamp)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bc_status", status);
            cmd.Parameters.AddWithValue("userstamp", userstamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        internal static SlBsta Convert(NpgsqlDataReader reader)
        {
            SlBsta value = new SlBsta();
            value.SttnId = reader.GetString(0);
            value.SlId = reader.GetString(1);
            value.SsId = reader.GetString(2);
            value.SpId = reader.GetString(3);
            value.CbId = reader.GetString(4);
            value.BcPointId = reader.GetInt32(5);
            value.BcStatus = reader.GetInt32(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
