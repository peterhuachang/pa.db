﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PudDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pu_id,ip_v,ip_f_v,op_v,op_load,ip_frequence,battery,temperature,cr_user,cr_date,userstamp,datestamp from pa_tb_pud";
        internal const string SQL_UPDATE = @"update pa_tb_pud set ip_v=:ip_v,ip_f_v=:ip_f_v,op_v=:op_v,op_load=:op_load,
                                            ip_frequence=:ip_frequence,battery=:battery,temperature=:temperature,
                                            userstamp=:userstamp,datestamp=:datestamp";

        private NpgsqlConnection conn;

        public PudDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Pud SearchPK(string sttnId, string puId)
        {
            Pud value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pu_id=:pu_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pu_id", puId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Pud> SearchSttn(string sttnId)
        {
            List<Pud> values = new List<Pud>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public void Update(Pud value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and pu_id=:pu_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pu_id", value.PuId);
            cmd.Parameters.AddWithValue("ip_v", value.IpV);
            cmd.Parameters.AddWithValue("ip_f_v", value.IpFV);
            cmd.Parameters.AddWithValue("op_v", value.OpV);
            cmd.Parameters.AddWithValue("op_load", value.OpLoad);
            cmd.Parameters.AddWithValue("ip_frequence", value.IpFrequence);
            cmd.Parameters.AddWithValue("battery", value.Battery);
            cmd.Parameters.AddWithValue("temperature", value.Temperature);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);

            cmd.ExecuteNonQuery();
        }

        internal static Pud Convert(NpgsqlDataReader reader)
        {
            Pud value = new Pud();
            value.SttnId = reader.GetString(0);
            value.PuId = reader.GetString(1);
            value.IpV = reader.GetDecimal(2);
            value.IpFV = reader.GetDecimal(3);
            value.OpV = reader.GetDecimal(4);
            value.OpLoad = reader.GetDecimal(5);
            value.IpFrequence = reader.GetDecimal(6);
            value.Battery = reader.GetDecimal(7);
            value.Temperature = reader.GetDecimal(8);
            value.CrUser = reader.GetString(9);
            value.CrDate = reader.GetDateTime(10);
            value.UserStamp = reader.IsDBNull(11) ? "" : reader.GetString(11);
            value.DateStamp = reader.IsDBNull(12) ? (DateTime?)null : reader.GetDateTime(12);
            return value;
        }
    }
}
