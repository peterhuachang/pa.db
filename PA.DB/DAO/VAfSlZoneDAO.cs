﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class VAfSlZoneDAO
    {        
        internal const string SQL_SELECT = @"select sttn_id,af_id,af_sx_id,af_ao_id,af_ao_ci_no,
                                            bk_af_id,bk_af_sx_id,bk_af_ao_id,bk_af_ao_ci_no,
                                            sl_id,sx_id,ao_id,ao_ch_no,ld_id,ld_ch_no,
                                            ci_sx_id,ci_ao_id,ci_ci_id,ci_ci_no,
                                            sp_id,zone_id,bc_port_id,sl_bc_status,zone_status from pa_v_af_sl_zone";        
        private NpgsqlConnection conn;

        public VAfSlZoneDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public VAfSlZone SearchPK(string sttnId, string afId, string slId)
        {
            VAfSlZone value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and af_id=:af_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("af_id", afId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }     

        public IList<VAfSlZone> SearchSttn(string sttnId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAfSlZone> SearchBySlId(string sttnId, string slId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sl_id=:sl_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sl_id", slId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAfSlZone> SearchByZoneId(string sttnId, string zoneId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAfSlZone> SearchBySpId(string sttnId, string spId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sp_id=:sp_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", spId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAfSlZone> SearchByAfId(string sttnId, string afId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and af_id=:af_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("af_id", afId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VAfSlZone> SearchByBkAfId(string sttnId, string bkAfId)
        {
            List<VAfSlZone> values = new List<VAfSlZone>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and bk_af_id=:bk_af_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("bk_af_id", bkAfId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VAfSlZone Convert(NpgsqlDataReader reader)
        {
            VAfSlZone value = new VAfSlZone();
            value.SttnId = reader.GetString(0);
            value.AfId = reader.GetString(1);
            value.AfSxId = reader.GetString(2);
            value.AfAoId = reader.GetString(3);
            value.AfAoCiNo = reader.GetInt32(4);           
            value.BkAfId = reader.GetString(5);
            value.BkAfSxId = reader.GetString(6);
            value.BkAfAoId = reader.GetString(7);
            value.BkAfAoCiNo = reader.GetInt32(8); 
            value.SlId = reader.GetString(9);
            value.SxId = reader.GetString(10);
            value.AoId = reader.GetString(11);
            value.AoChNo = reader.GetInt32(12);
            value.LdId = reader.GetString(13);
            value.LdChNo = reader.GetInt32(14);
            value.CiSxId = reader.GetString(15);
            value.CiAoId = reader.GetString(16);
            value.CiCiId = reader.GetString(17);
            value.CiCiNo = reader.GetInt32(18);
            value.SpId = reader.GetString(19);
            value.ZoneId = reader.GetString(20);
            value.BcPortId = reader.GetInt32(21);
            value.SlBcStatus = reader.GetInt32(22);
            value.ZoneStatus = reader.GetInt32(23);
            return value;
        }
    }
}
