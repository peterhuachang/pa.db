﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class IpDAO
    {
        internal const string SQL_SELECT = "select server_id,server_type,seq,ip,alarm_id,cr_user,cr_date,userstamp,datestamp from pa_tb_ip";

        private NpgsqlConnection conn;

        public IpDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public Ip SearchPK(string serverId, string serverType, int seq)
        {
            Ip value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where server_id=:server_id and server_Type=:server_Type and seq=:seq";
            cmd.Parameters.AddWithValue("server_id", serverId);
            cmd.Parameters.AddWithValue("server_type", serverType);
            cmd.Parameters.AddWithValue("seq", seq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Ip> SearchType(string serverId, string serverType)
        {
            List<Ip> values = new List<Ip>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where server_id=:server_id and server_type=:server_type";
            cmd.Parameters.AddWithValue("server_id", serverId);
            cmd.Parameters.AddWithValue("server_type", serverType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Ip> Search(string serverId)
        {
            List<Ip> values = new List<Ip>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where server_id=:server_id";
            cmd.Parameters.AddWithValue("server_id", serverId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Ip> SearchAll()
        {
            List<Ip> values = new List<Ip>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT;
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static Ip Convert(NpgsqlDataReader reader)
        {
            Ip value = new Ip();
            value.ServerId = reader.GetString(0);
            value.ServerType = reader.GetString(1);
            value.Seq = reader.GetInt32(2);
            value.IP = reader.GetString(3);
            value.AlarmId = reader.IsDBNull(4) ? "" : reader.GetString(4) ;
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
