﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoSentenceDAO
    {
        internal const string SQL_SELECT = "select sttn_id,pa_id,timing,lang,cname,ename,sentence_seq,cr_user,cr_date,userstamp,datestamp from pa_tb_auto_sentence";

        internal const string SQL_INSERT = "insert into pa_tb_auto_sentence(sttn_id,pa_id,timing,lang,cname,ename,sentence_seq,cr_user,cr_date,userstamp,datestamp)values(:sttn_id,:pa_id,:timing,:lang,:cname,:ename,:sentence_seq,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_auto_sentence set lang=:lang,cname=:cname,ename=:ename,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_auto_sentence";

        private NpgsqlConnection conn;

        public AutoSentenceDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(AutoSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);       
            cmd.ExecuteNonQuery();
        }

        public void Update(AutoSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and sentence_seq=:sentence_seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("lang", value.Lang);
            cmd.Parameters.AddWithValue("cname", value.Cname);
            cmd.Parameters.AddWithValue("ename", value.Ename);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);                 
            cmd.ExecuteNonQuery();
        }

        public void Delete(AutoSentence value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and sentence_seq=:sentence_seq";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("pa_id", value.PaId);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("sentence_seq", value.SentenceSeq);
            cmd.ExecuteNonQuery();
        }

        public AutoSentence SearchPK(string sttnId, string PaId, int timing,  int sentenceSeq)
        {
            AutoSentence value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing and sentence_seq=:sentence_seq";

            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            cmd.Parameters.AddWithValue("sentence_seq", sentenceSeq);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<AutoSentence> Search(string sttnId, string PaId, int timing)
        {
            List<AutoSentence> values = new List<AutoSentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pa_id=:pa_id and timing=:timing order by sentence_seq asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pa_id", PaId);
            cmd.Parameters.AddWithValue("timing", timing);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoSentence> SearchSttn(string sttnId)
        {
            List<AutoSentence> values = new List<AutoSentence>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id order by timing, sentence_seq, lang asc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoSentence Convert(NpgsqlDataReader reader)
        {
            AutoSentence value = new AutoSentence();
            value.SttnId = reader.GetString(0);
            value.PaId = reader.GetString(1);
            value.Timing = reader.GetInt32(2);
            value.Lang = reader.GetString(3);
            value.Cname = reader.GetString(4);
            value.Ename = reader.GetString(5);
            value.SentenceSeq = reader.GetInt32(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.UserStamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.DateStamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
