﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class DvcDtlDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,labeling,cdesc,edesc,ip_addr,pmc_grp_num,pmc_pnt_num,loc_id,mu_id,cr_user,cr_date,userstamp,datestamp from pa_tb_dvc_dtl";

        internal const string SQL_SELECT_PA = "select a.sttn_id,a.dvc_type,a.dvc_id,a.labeling,a.cdesc,a.edesc,a.ip_addr,a.pmc_grp_num,a.pmc_pnt_num,a.loc_id,a.mu_id,a.cr_user,a.cr_date,a.userstamp,a.datestamp from pa_tb_dvc_dtl a, (select sttn_id, dev_id from pa_tb_bc_pointm where voice_source='PA' group by sttn_id,dev_id) b where a.dvc_id=b.dev_id and a.sttn_id=b.sttn_id and dvc_type='PA' order by a.dvc_id ";
        private NpgsqlConnection conn;

        public DvcDtlDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public DvcDtl SearchPK(string sttnId, string dvcType, string dvcId)
        {
            DvcDtl value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<DvcDtl> Search(string sttnId, string dvcType)
        {
            List<DvcDtl> values = new List<DvcDtl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcDtl> SearchManualPAs(string sttnId)
        {
            List<DvcDtl> values = new List<DvcDtl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT_PA ;
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcDtl> SearchSttn(string sttnId)
        {
            List<DvcDtl> values = new List<DvcDtl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<DvcDtl> SearchPmcDevice(string sttnId)
        {
            List<DvcDtl> values = new List<DvcDtl>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pmc_grp_num is not null and pmc_grp_num <> '' order by dvc_type, dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static DvcDtl Convert(NpgsqlDataReader reader)
        {
            DvcDtl value = new DvcDtl();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.Labeling = reader.GetString(3);
            value.Cdesc = reader.GetString(4);
            value.Edesc = reader.GetString(5);
            value.IpAddr = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.PmcGrpNum = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.PmcPntNum = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.LocId = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.MuId = reader.IsDBNull(10)? "" : reader.GetString(10);
            value.CrUser = reader.GetString(11);
            value.CrDate = reader.GetDateTime(12);
            value.UserStamp = reader.IsDBNull(13) ? "" : reader.GetString(13);
            value.DateStamp = reader.IsDBNull(14) ? (DateTime?)null : reader.GetDateTime(14);
            return value;
        }
    }
}
