﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SxAiCiDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ai_id,ci_no,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ai_ci ";

        private NpgsqlConnection conn;

        public SxAiCiDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SxAiCi SearchPK(string sttnId, string sxId, string aiId, int ciNo)
        {
            SxAiCi value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id and ci_no=:ci_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            cmd.Parameters.AddWithValue("ci_no", ciNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<SxAiCi> SearchSttn(string sttnId)
        {
            List<SxAiCi> values = new List<SxAiCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAiCi> SearchBySxId(string sttnId, string sxId)
        {
            List<SxAiCi> values = new List<SxAiCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAiCi> SearchByAiId(string sttnId, string sxId, string aiId)
        {
            List<SxAiCi> values = new List<SxAiCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ai_id=:ai_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ai_id", aiId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAiCi> SearchByDvcType(string sttnId, string dvcType)
        {
            List<SxAiCi> values = new List<SxAiCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type order by dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAiCi> SearchByDevice(string sttnId, string dvcType, string dvcId)
        {
            List<SxAiCi> values = new List<SxAiCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SxAiCi Convert(NpgsqlDataReader reader)
        {
            SxAiCi value = new SxAiCi();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AiId = reader.GetString(2);
            value.CiNo = reader.GetInt32(3);
            value.DvcType = reader.IsDBNull(4) ? "" : reader.GetString(4);
            value.DvcId = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
