﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AutoBcHistDAO
    {
        internal const string SQL_SELECT = @"select id,sttn_id,train_no,orig_sttn_id,dest_sttn_id,run_direct,timing,train_status,platform,
                                             arrival_time,departure_time,dest_time,delay_time,stop,cmd_from,bc_status,userstamp,datestamp 
                                             from pa_tb_auto_bc_hist ";
        internal const string SQL_INSERT = @"insert into pa_tb_auto_bc_hist (sttn_id,train_no,orig_sttn_id,dest_sttn_id,run_direct,
                                             timing,train_status,platform,arrival_time,departure_time,dest_time,delay_time,stop,
                                             cmd_from,bc_status,userstamp,datestamp) 
                                            values(nextval('pa_tb_curr_auto_bc_id_seq'),:sttn_id,:train_no,:orig_sttn_id,:dest_sttn_id,
                                                   :run_direct,:timing,:train_status,:platform,:arrival_time,:departure_time,:dest_time,
                                                   :delay_time,:stop,:cmd_from,:bc_status,:userstamp,:datestamp) ";
        internal const string SQL_DELETE = "delete from pa_tb_auto_bc_hist ";

        private NpgsqlConnection conn;

        public AutoBcHistDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(AutoBcHist value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("train_no", value.TrainNo);
            cmd.Parameters.AddWithValue("orig_sttn_id", value.OrigSttnId);
            cmd.Parameters.AddWithValue("dest_sttn_id", value.DestSttnId);
            cmd.Parameters.AddWithValue("run_direct", value.RunDirect);
            cmd.Parameters.AddWithValue("timing", value.Timing);
            cmd.Parameters.AddWithValue("train_status", value.TrainStatus);
            cmd.Parameters.AddWithValue("platform", value.Platform);
            cmd.Parameters.AddWithValue("arrival_time", value.ArrivalTime);
            cmd.Parameters.AddWithValue("departure_time", value.DepartureTime);
            cmd.Parameters.AddWithValue("dest_time", value.DestTime);
            cmd.Parameters.AddWithValue("delay_time", value.DelayTime);
            cmd.Parameters.AddWithValue("stop", value.Stop);
            cmd.Parameters.AddWithValue("cmd_from", value.CmdFrom);
            cmd.Parameters.AddWithValue("bc_status", value.BcStatus);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.ExecuteNonQuery();

            //cmd = this.conn.CreateCommand();
            //cmd.CommandText = "select currval('pa_tb_curr_bc_id_seq')";
            //  value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Delete(AutoBcHist value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id ";
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId, string trainNo)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and train_no=:train_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            cmd.ExecuteNonQuery();
        }

        public void Delete(string sttnId)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.ExecuteNonQuery();
        }

        public void DeleteAll()
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE;
            cmd.ExecuteNonQuery();
        }

        public AutoBcHist SearchPK(int id)
        {
            AutoBcHist value = new AutoBcHist();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=:id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<AutoBcHist> Search(string sttnId, string trainNo)
        {
            List<AutoBcHist> values = new List<AutoBcHist>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and train_no=:train_no ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("train_no", trainNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<AutoBcHist> SearchSttn(string sttnId)
        {
            List<AutoBcHist> values = new List<AutoBcHist>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static AutoBcHist Convert(NpgsqlDataReader reader)
        {
            AutoBcHist value = new AutoBcHist();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.TrainNo = reader.GetString(2);
            value.OrigSttnId = reader.GetString(3);
            value.DestSttnId = reader.GetString(4);
            value.RunDirect = reader.GetInt32(5);
            value.Timing = reader.GetInt32(6);
            value.TrainStatus = reader.GetInt32(7);
            value.Platform = reader.GetString(8);
            value.ArrivalTime = reader.GetDateTime(9);
            value.DepartureTime = reader.GetDateTime(10);
            value.DestTime = reader.GetDateTime(11);
            value.DelayTime = reader.GetInt32(12);
            value.Stop = reader.GetString(13);
            value.CmdFrom = reader.GetString(14);
            value.BcStatus = reader.GetInt32(15);
            value.Userstamp = reader.IsDBNull(16) ? "" : reader.GetString(16);
            value.Datestamp = reader.IsDBNull(17) ? (DateTime?)null : reader.GetDateTime(17);
            return value;
        }
    }
}
