﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class BcPointDDAO
    {
        internal const string SQL_SELECT = "select sttn_id,point_id,zone_id,cr_user,cr_date,userstamp,datestamp from pa_tb_bc_pointd";

        internal const string SQL_INSERT = "insert into pa_tb_bc_pointd(sttn_id,point_id,zone_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_bc_pointd_id_seq'),:sttn_id,:point_id,:zone_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_bc_pointd set sttn_id=:sttn_id,point_id=:point_id,zone_id=:zone_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where sttn_id=:sttn_id and point_id=:point_id and zone_id=:zone_id";

        internal const string SQL_DELETE = "delete from pa_tb_bc_pointd";

        private NpgsqlConnection conn;

        public BcPointDDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(BcPointD value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("zone_id", value.ZoneId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_bc_pointd_id_SEQ')";
            //value.id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(BcPointD value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("zone_id", value.ZoneId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            // cmd.Parameters.AddWithValue("id", value.id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(BcPointD value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where sttn_id=:sttn_id and point_id=:point_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("point_id", value.PointId);
            cmd.Parameters.AddWithValue("zone_id", value.ZoneId);
            cmd.ExecuteNonQuery();
        }

        public BcPointD SearchPK(string sttnId, string pointId, string zoneId)
        {
            BcPointD value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and point_id=:point_id and zone_id=:zone_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("point_id", pointId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();
            return value;
        }

        public IList<BcPointD> Search(string sttnId, string zoneId)
        {
            List<BcPointD> values = new List<BcPointD>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and zone_id=:zone_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("zone_id", zoneId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<BcPointD> SearchSttn(string sttnId)
        {
            List<BcPointD> values = new List<BcPointD>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static BcPointD Convert(NpgsqlDataReader reader)
        {
            BcPointD value = new BcPointD();
            value.SttnId = reader.GetString(0);
            value.PointId = reader.GetString(1);
            value.ZoneId = reader.GetString(2);
            value.CrUser = reader.GetString(3);
            value.CrDate = reader.GetDateTime(4);
            value.UserStamp = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DateStamp = reader.IsDBNull(6) ? (DateTime?)null : reader.GetDateTime(6);
            return value;
        }
    }
}
