﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class SxAoCiDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ao_id,ci_no,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ao_ci ";

        private NpgsqlConnection conn;

        public SxAoCiDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SxAoCi SearchPK(string sttnId, string sxId, string aoId, int ciNo)
        {
            SxAoCi value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id and ci_no=:ci_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ci_no", ciNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<SxAoCi> SearchSttn(string sttnId)
        {
            List<SxAoCi> values = new List<SxAoCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCi> SearchBySxId(string sttnId, string sxId)
        {
            List<SxAoCi> values = new List<SxAoCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCi> SearchByAoId(string sttnId, string sxId, string aoId)
        {
            List<SxAoCi> values = new List<SxAoCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCi> SearchByDvcType(string sttnId, string dvcType)
        {
            List<SxAoCi> values = new List<SxAoCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type order by dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAoCi> SearchByDevice(string sttnId, string dvcType, string dvcId)
        {
            List<SxAoCi> values = new List<SxAoCi>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type and dvc_id=:dvc_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            cmd.Parameters.AddWithValue("dvc_id", dvcId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SxAoCi Convert(NpgsqlDataReader reader)
        {
            SxAoCi value = new SxAoCi();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AoId = reader.GetString(2);
            value.CiNo = reader.GetInt32(3);
            value.DvcType = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
