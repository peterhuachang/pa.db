﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class ZoneLoopDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sp_id,zone_id,cr_user,cr_date,userstamp,datestamp from pa_tb_zone_loop";

        private NpgsqlConnection conn;

        public ZoneLoopDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public ZoneLoop SearchPK(string sttnId, string spId)
        {
            ZoneLoop value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sp_id=:sp_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sp_id", spId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<ZoneLoop> Search(string sttnId)
        {
            List<ZoneLoop> values = new List<ZoneLoop>();

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id ";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static ZoneLoop Convert(NpgsqlDataReader reader)
        {
            ZoneLoop value = new ZoneLoop();
            value.SttnId = reader.GetString(0);
            value.SpId = reader.GetString(1);
            value.ZoneId = reader.GetString(2);
            value.CrUser = reader.GetString(3);
            value.CrDate = reader.GetDateTime(4);
            value.UserStamp = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DateStamp =reader.IsDBNull(6) ? (DateTime?)null: reader.GetDateTime(6);
            return value;
        }
    }
}
