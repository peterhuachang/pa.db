﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AdswDAO
    {
        internal const string SQL_SELECT = "select id,sttn_id,adsw_id,channel_id,type,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_adsw";

        internal const string SQL_INSERT = "insert into pa_tb_adsw(id,sttn_id,adsw_id,channel_id,type,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp)values(nextval('pa_tb_adsw_id_seq'),:sttn_id,:adsw_id,:channel_id,:type,:dvc_type,:dvc_id,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_adsw set sttn_id=:sttn_id,adsw_id=:adsw_id,channel_id=:channel_id,type=:type,dvc_type=:dvc_type,dvc_id=:dvc_id,cr_user=:cr_user,cr_date=:cr_date,userstamp=:userstamp,datestamp=:datestamp where id=:id";

        internal const string SQL_DELETE = "delete from pa_tb_adsw";

        private NpgsqlConnection conn;

        public AdswDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(Adsw value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("adsw_id", value.AdswId);
            cmd.Parameters.AddWithValue("channel_id", value.ChannelId);
            cmd.Parameters.AddWithValue("type", value.Type);
            cmd.Parameters.AddWithValue("dvc_type", value.DvcType);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.ExecuteNonQuery();

            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select currval('pa_tb_adsw_id_seq')";
            //value.Id = (int)(long)cmd.ExecuteScalar();
        }

        public void Update(Adsw value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("adsw_id", value.AdswId);
            cmd.Parameters.AddWithValue("channel_id", value.ChannelId);
            cmd.Parameters.AddWithValue("type", value.Type);
            cmd.Parameters.AddWithValue("dvc_type", value.DvcType);
            cmd.Parameters.AddWithValue("dvc_id", value.DvcId);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.Userstamp);
            cmd.Parameters.AddWithValue("datestamp", value.Datestamp);
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public void Delete(Adsw value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public Adsw SearchById(int id)
        {
            Adsw value = null;

            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where id=id";
            cmd.Parameters.AddWithValue("id", id);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Adsw> SearchAll()
        {
            List<Adsw> values = new List<Adsw>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = "select * from pa_tb_adsw";
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Adsw> SearchBySttnId(string sttnId)
        {
            List<Adsw> values = new List<Adsw>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<Adsw> SearchByDvcType(string sttnId, string dvcType)
        {
            List<Adsw> values = new List<Adsw>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", dvcType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Dictionary<string, string> SearchQueue2Channel(string sttnId)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = @"select que_name, channel_id from pa_tb_process a, pa_tb_adsw b
				                where a.dvc_type=b.dvc_type and a.dvc_id=b.dvc_id 
                                and b.dvc_type='CC' and b.type='I' and b.sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(reader.GetString(0), reader.GetInt32(1).ToString().PadLeft(4, '0'));
            }
            reader.Close();
            return values;
        }

        internal static Adsw Convert(NpgsqlDataReader reader)
        {
            Adsw value = new Adsw();
            value.Id = reader.GetInt32(0);
            value.SttnId = reader.GetString(1);
            value.AdswId = reader.GetString(2);
            value.ChannelId = reader.GetInt32(3);
            value.Type = reader.GetString(4);
            value.DvcType = reader.IsDBNull(5) ? "" : reader.GetString(5);
            value.DvcId = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.CrUser = reader.GetString(7);
            value.CrDate = reader.GetDateTime(8);
            value.Userstamp = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.Datestamp = reader.IsDBNull(10) ? (DateTime?)null : reader.GetDateTime(10);
            return value;
        }
    }
}
