﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class AssDAO
    {
        internal const string SQL_SELECT = "select sttn_id,as_idp,idnum,as_idc,as_status,cr_user,cr_date,userstamp,datestamp from pa_tb_ass";

        internal const string SQL_INSERT = "insert into pa_tb_ass(sttn_id,as_idp,idnum,as_idc,as_status,cr_user,cr_date,userstamp,datestamp)values(:sttn_id,:as_idp,:idnum,:as_idc,:as_status,:cr_user,:cr_date,:userstamp,:datestamp)";

        internal const string SQL_UPDATE = "update pa_tb_ass set as_status=:as_status,userstamp=:userstamp,datestamp=:datestamp";

        internal const string SQL_DELETE = "delete from pa_tb_ass";

        private NpgsqlConnection conn;

        public AssDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public void Insert(Ass value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_INSERT;
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("as_idp", value.AsIdp);
            cmd.Parameters.AddWithValue("idnum", value.IdNum);
            cmd.Parameters.AddWithValue("as_idc", value.AsIdc);
            cmd.Parameters.AddWithValue("as_status", value.AsStatus);
            cmd.Parameters.AddWithValue("cr_user", value.CrUser);
            cmd.Parameters.AddWithValue("cr_date", value.CrDate);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", value.DateStamp);
            cmd.ExecuteNonQuery();
        }

        public bool Update(Ass value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_UPDATE + " where sttn_id=:sttn_id and as_idp=:as_idp and idnum=:idnum";
            cmd.Parameters.AddWithValue("as_status", value.AsStatus);
            cmd.Parameters.AddWithValue("userstamp", value.UserStamp);
            cmd.Parameters.AddWithValue("datestamp", DateTime.Now);
            cmd.Parameters.AddWithValue("sttn_id", value.SttnId);
            cmd.Parameters.AddWithValue("as_idp", value.AsIdp);
            cmd.Parameters.AddWithValue("idnum", value.IdNum);
            return cmd.ExecuteNonQuery() == 0 ? true : false;
        }

        public void Delete(Adsw value)
        {
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_DELETE + " where id=:id";
            cmd.CommandText = SQL_DELETE;
            cmd.Parameters.AddWithValue("id", value.Id);
            cmd.ExecuteNonQuery();
        }

        public Ass SearchPK(string sttnId, string asIdp, string idNum)
        {
            Ass value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_idp=:as_idp and idnum=:idnum";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_idp", asIdp);
            cmd.Parameters.AddWithValue("idnum", idNum);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        public IList<Ass> SearchSttn(string sttnId)
        {
            List<Ass> values = new List<Ass>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id order by as_idp,idnum";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public Ass SearchDvcId(string sttnId, string asIdc)
        {
            Ass value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and as_idc=:as_idc";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("as_idc", asIdc);           
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            reader.Close();

            return value;
        }

        internal static Ass Convert(NpgsqlDataReader reader)
        {
            Ass value = new Ass();
            value.SttnId = reader.GetString(0);
            value.AsIdp = reader.GetString(1);
            value.IdNum = reader.GetString(2);
            value.AsIdc = reader.GetString(3);
            value.AsStatus = reader.GetDecimal(4);
            value.CrUser = reader.GetString(5);
            value.CrDate = reader.GetDateTime(6);
            value.UserStamp = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.DateStamp = reader.IsDBNull(8) ? (DateTime?)null : reader.GetDateTime(8);
            return value;
        }
    }
}
