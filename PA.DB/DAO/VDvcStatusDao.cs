﻿using Npgsql;
using PA.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class VDvcStatusDAO
    {
        internal const string SQL_SELECT = "select sttn_id,dvc_type,dvc_id,labeling,cdesc,edesc,ip_addr,pmc_grp_num,pmc_pnt_num,loc_id,mu_id,alarm_id,alarm_flag,status_cname,status_ename,alarm_time from pa_v_dvc_status";

        private NpgsqlConnection conn;

        public VDvcStatusDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }


        public IList<VDvcStatus> SearchGrp(string sttnId, string pmcGrpNum)
        {
            List<VDvcStatus> values = new List<VDvcStatus>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and pmc_grp_num=:pmc_grp_num";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("pmc_grp_num", pmcGrpNum);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VDvcStatus> SearchBySttn(string sttnId)
        {
            List<VDvcStatus> values = new List<VDvcStatus>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<VDvcStatus> SearchSttnDevType(string sttnId,string deviceType)
        {
            List<VDvcStatus> values = new List<VDvcStatus>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and dvc_type=:dvc_type";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("dvc_type", deviceType);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static VDvcStatus Convert(NpgsqlDataReader reader)
        {
            VDvcStatus value = new VDvcStatus();
            value.SttnId = reader.GetString(0);
            value.DvcType = reader.GetString(1);
            value.DvcId = reader.GetString(2);
            value.Labeling = reader.GetString(3);
            value.Cdesc = reader.GetString(4);
            value.Edesc = reader.GetString(5);
            value.IpAddr = reader.IsDBNull(6) ? "" : reader.GetString(6);
            value.PmcGrpNum = reader.IsDBNull(7) ? "" : reader.GetString(7);
            value.PmcPntNum = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.LocId = reader.IsDBNull(9) ? "" : reader.GetString(9);
            value.MuId = reader.IsDBNull(10) ? "" : reader.GetString(10);
            value.AlarmId = reader.GetString(11);
            value.AlarmFlag = reader.GetInt32(12);
            value.StatusCname = reader.GetString(13);
            value.StatusEname = reader.GetString(14);
            value.AlarmTime = reader.GetDateTime(15);
            return value;
        }
    }
}
