﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Npgsql;
using PA.DB;

namespace PA.DB.DAO
{
    public class SxAoDAO
    {
        internal const string SQL_SELECT = "select sttn_id,sx_id,ao_id,ch_no,dvc_type,dvc_id,cr_user,cr_date,userstamp,datestamp from pa_tb_sx_ao";

        private NpgsqlConnection conn;

        public SxAoDAO(NpgsqlConnection conn)
        {
            this.conn = conn;
        }

        public SxAo SearchPK(string sttnId, string aoId, int chNo)
        {
            SxAo value = null;
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + @" where sttn_id=:sttn_id and ao_id=:ao_id and ch_no=:ch_no";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            cmd.Parameters.AddWithValue("ch_no", chNo);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                value = Convert(reader);
            }
            else
            {
                throw new PaDbException("set pkey error");
            }
            reader.Close();
            return value;
        }

        public IList<SxAo> SearchSttn(string sttnId)
        {
            List<SxAo> values = new List<SxAo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAo> SearchBySxId(string sttnId, string sxId)
        {
            List<SxAo> values = new List<SxAo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        public IList<SxAo> SearchByAoId(string sttnId, string sxId, string aoId)
        {
            List<SxAo> values = new List<SxAo>();
            NpgsqlCommand cmd;
            cmd = this.conn.CreateCommand();
            cmd.CommandText = SQL_SELECT + " where sttn_id=:sttn_id and sx_id=:sx_id and ao_id=:ao_id";
            cmd.Parameters.AddWithValue("sttn_id", sttnId);
            cmd.Parameters.AddWithValue("sx_id", sxId);
            cmd.Parameters.AddWithValue("ao_id", aoId);
            NpgsqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                values.Add(Convert(reader));
            }
            reader.Close();
            return values;
        }

        internal static SxAo Convert(NpgsqlDataReader reader)
        {
            SxAo value = new SxAo();
            value.SttnId = reader.GetString(0);
            value.SxId = reader.GetString(1);
            value.AoId = reader.GetString(2);
            value.ChNo = reader.GetInt32(3);
            value.DvcType = reader.GetString(4);
            value.DvcId = reader.GetString(5);
            value.CrUser = reader.GetString(6);
            value.CrDate = reader.GetDateTime(7);
            value.UserStamp = reader.IsDBNull(8) ? "" : reader.GetString(8);
            value.DateStamp = reader.IsDBNull(9) ? (DateTime?)null : reader.GetDateTime(9);
            return value;
        }
    }
}
