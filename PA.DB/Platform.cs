﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
   public class Platform
    {
       public string SttnId { get; set; }

       public string PlatformNo { get; set; }

       public string VoiceSource { get; set; }

       public string DevId { get; set; }
    }
}
