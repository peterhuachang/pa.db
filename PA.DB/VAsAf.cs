﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VAsAf
    {
        public string SttnId { get; set; }

        public string AsIdp { get; set; }

        public string AsIdc { get; set; }

        public string IdNum { get; set; }

        public decimal AsStatus { get; set; }

        public string AsId { get; set; }

        public decimal Seq { get; set; }

        public int MinChannel { get; set; }

        public int MaxChannel { get; set; }

        public int BkStatus { get; set; }

        public string SxId { get; set; }

        public string AoId { get; set; }

        public int CoNo { get; set; }

        public int Status { get; set; }
    }
}
