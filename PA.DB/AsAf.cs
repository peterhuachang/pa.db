﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class AsAf
    {
        public string SttnId { get; set; }

        public string AsId { get; set; }

        public decimal Seq { get; set; }

        public string AfId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
