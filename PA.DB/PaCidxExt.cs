﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class PaCidxExt
    {
        public string SttnId { get; set; }

        public string PaId { get; set; }

        public string IdxId { get; set; }

        public string CardId { get; set; }

        public string Lang { get; set; }
    }
}
