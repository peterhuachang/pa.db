﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VProcDvcMap
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string ExeSttn { get; set; }

        public string ProcName { get; set; }

        public string QueName { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string Labeling { get; set; }

        public string Cdesc { get; set; }

        public string Edesc { get; set; }

        public string IpAddr { get; set; }
    }
}
