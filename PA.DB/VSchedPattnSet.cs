﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VSchedPattnSet
    {
        public decimal PKey;
        public DateTime SchedDate;
        public string StartTime;
        public string EndTime;
        public string VoiceSource;
        public string DvcId;
        public int Interval;
        public int Duration;
        public int Repeat;
        public string SttnId;
    }
}
