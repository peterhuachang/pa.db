﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class CurrBc
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string Voice { get; set; }

        public string BcAreaId { get; set; }

        public string CmdFrom { get; set; }

        public DateTime? DateStamp { get; set; }

        public string BcSttnId { get; set; }

        public string SrcQue { get; set; }

        public int SentenCnt { get; set; }

        public string SentenId { get; set; }
    }
}
