﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VSpkLine
    {
        public string SttnId { get; set; }

        public string ZoneId { get; set; }

        public string SpId { get; set; }

        public string SlId { get; set; }

        public string SttnCname { get; set; }

        public string SttnEname { get; set; }

        public string ZoneCname { get; set; }

        public string ZoneEname { get; set; }

        public int ZoneBcStatus { get; set; }

        public string SsId { get; set; }

        public string CbId { get; set; }

        public int BcPortId { get; set; }

        public int SlBcStatus { get; set; }

        public String SttnNameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    SttnCname : SttnEname;
            }
        }

        public String ZoneNameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    ZoneCname : ZoneEname;
            }
        }
    }
}
