﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class AfSl
    {
        public string SttnId { get; set; }

        public string AfId { get; set; }

        public string SlId { get; set; }

        public string BkAfId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
