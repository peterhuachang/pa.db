﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Ip
    {
        public string ServerId { get; set; }

        public string ServerType { get; set; }

        public int Seq { get; set; }

        public string IP { get; set; }

        public string AlarmId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
