﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Process
    {
        public int Id { get; set; }

        public string ExeSttn { get; set; }

        public string ProcName { get; set; }

        public string QueName { get; set; }

        public string ProcId { get; set; }

        public string SttnId { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public int ExeSeq { get; set; }

        public string ExeFile { get; set; }

        public string RunFlag { get; set; }

        public string InitFlag { get; set; }

    }
}
