﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VScdPoint
    {
        public string SttnId { get; set; }

        public string VoiceSource { get; set; }

        public string DevId { get; set; }
        
        public string ScId { get; set; }

        public int BitId { get; set; }

        public string PointId { get; set; }

        public string BitType { get; set; }
    }
}
