﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Scd
    {
        public string SttnId { get; set; }

        public string ScId { get; set; }

        public int BitId { get; set; }

        public string BitType { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string PointId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
