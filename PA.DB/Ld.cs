﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Ld
    {
        public string SttnId { get; set; }

        public string LdId { get; set; }

        public string SlId { get; set; }

        public int ChNo { get; set; }
        
        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
