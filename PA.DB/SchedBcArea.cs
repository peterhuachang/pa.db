﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class SchedBcArea
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public decimal Pkey { get; set; }

        public string BcAreaId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string Userstamp { get; set; }

        public DateTime? Datestamp { get; set; }
    }
}
