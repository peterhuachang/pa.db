﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VParamTypeValue
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string ParamType { get; set; }

        public string ParamId { get; set; }

        public string ParamVal { get; set; }

        public string TypeCdesc { get; set; }

        public string TypeEdesc { get; set; }

        public string ValueCdesc { get; set; }

        public string ValueEdesc { get; set; }

        public String TypeDescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    TypeCdesc : TypeEdesc;
            }
        }

        public String ValueDescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    ValueCdesc : ValueEdesc;
            }
        }
    }
}
