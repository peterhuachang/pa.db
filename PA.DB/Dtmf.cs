﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Dtmf
    {
        public string SttnId { get; set; }

        public string AdswId { get; set; }

        public string DtmfId { get; set; }

        public string LSttnId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
