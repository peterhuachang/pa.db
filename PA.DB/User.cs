﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class User
    {
        public string UserId { get; set; }

        public string Pwd { get; set; }

        public string Cname { get; set; }

        public string Ename { get; set; }

        public string Grade { get; set; }

        public string Language { get; set; }

        public string Zone { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string Userstamp { get; set; }

        public DateTime? Datestamp { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cname : Ename;
            }
        }
    }
}
