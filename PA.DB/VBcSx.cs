﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VBcSx
    {
        public string SttnId { get; set; }

        public string BcAreaId { get; set; }

        public string ZoneId { get; set; }

        public string VoiceSource { get; set; }

        public string DevId { get; set; }

        public string SpId { get; set; }

        public string SlId { get; set; }

        public string SsId { get; set; }

        public int BcPortId { get; set; }

        public string SxId { get; set; }

        public string AiId { get; set; }

        public int AiChNo { get; set; }

        public string AoId { get; set; }

        public int AoChNo { get; set; }

        //public int AoCiNo { get; set; }
    }
}
