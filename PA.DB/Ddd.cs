﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Ddd
    {
        public string SttnId { get; set; }

        public string DdId { get; set; }

        public string SttnIdT { get; set; }

        public string DdIdT { get; set; }

        public string CnnName { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
