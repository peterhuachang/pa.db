﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VSchedPhrase
    {
        public string SttnId { get; set; }

        public decimal PKey { get; set; }

        public string IdxId { get; set; }

        public int TimeLen { get; set; }

        public string SentenId { get; set; }

        public DateTime SchedDate { get; set; }
    }
}
