﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VDevice
    {
        public string SttnId { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string Labeling { get; set; }

        public string DtlCdesc { get; set; }

        public string DtlEdesc { get; set; }

        public string IpAddr { get; set; }

        public string PmcGrpNum { get; set; }

        public string PmcPntNum { get; set; }

        public string LocId { get; set; }

        public string MuId { get; set; }

        public string TypeCdesc { get; set; }

        public string TypeEdesc { get; set; }

        public String DescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    DtlCdesc : DtlEdesc;
            }
        }

        public String TypeDescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    TypeCdesc : TypeEdesc;
            }
        }
    }
}
