﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class OperLog
    {
        public int Id { get; set; }

        public string LogLanguage { get; set; }

        public DateTime DateStamp { get; set; }

        public string Type { get; set; }

        public string SttnId { get; set; }

        public string Context { get; set; }
    }
}
