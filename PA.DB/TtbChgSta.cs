﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class TtbChgSta
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string GrpAddr { get; set; }

        public string PntNum { get; set; }

        public DateTime Datestamp { get; set; }

        public string AlarmId { get; set; }

        public int Status { get; set; }

        public string ProcFlag { get; set; }

        public DateTime ProcDate { get; set; }
    }
}
