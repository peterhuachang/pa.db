﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class SchedSet
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public DateTime SchedDate { get; set; }

        public string PattnId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
