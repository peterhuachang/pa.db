﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VBcPoint
    {
        public string SttnId { get; set; }

        public string BcAreaId { get; set; }

        public string PointId { get; set; }

        public string ZoneId { get; set; }

        public string AreaCname { get; set; }

        public string AreaEname { get; set; }

        public string VoiceSource { get; set; }

        public string PlatForm { get; set; }

        public string ZoneCname { get; set; }

        public string ZoneEname { get; set; }

        public int ZoneBcStatus { get; set; }

        public String AreaNameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    AreaCname : AreaEname;
            }
        }

        public String ZoneNameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    ZoneCname : ZoneEname;
            }
        }
    }
}
