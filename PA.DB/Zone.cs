﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class Zone
    {
        public string SttnId { get; set; }

        public string ZoneId { get; set; }

        public string Cname { get; set; }

        public string Ename { get; set; }

        public int BcStatus { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }

        public String NameI18N
        {
            get
            {
                return "ZH-TW".Equals(Thread.CurrentThread.CurrentCulture.Name.ToUpper()) ?
                    Cname : Ename;
            }
        }
    }
}
