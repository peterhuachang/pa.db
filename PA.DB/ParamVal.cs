﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class ParamVal
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public string ParamType { get; set; }

        public string ParamId { get; set; }

        public string ValueName { get; set; }

        public string Cdesc { get; set; }

        public string Edesc { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }

        public String DescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cdesc : Edesc;
            }
        }
    }
}
