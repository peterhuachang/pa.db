﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class AutoSentencePhr
    {
        public string SttnId { get; set; }

        public string PaId { get; set; }

        public int Timing { get; set; }

        public string Lang { get; set; }

        public int SentenceSeq { get; set; }

        public int PhraseSeq { get; set; }

        public int PhraseType { get; set; }

        public string Phrase { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
