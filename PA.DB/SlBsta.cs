﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class SlBsta
    {
        public string SttnId { get; set; }

        public string SlId { get; set; }

        public string SsId { get; set; }

        public string SpId { get; set; }

        public string CbId { get; set; }

        public int BcPointId { get; set; }

        public int BcStatus { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }

    }
}
