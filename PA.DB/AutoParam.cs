﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class AutoParam
    {
        public string Param { get; set; }

        public string Cdesc { get; set; }

        public string Edesc { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cdesc : Edesc;
            }
        }
    }
}
