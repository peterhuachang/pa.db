﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class AutoPhrParam
    {
        public string SttnId { get; set; }

        public string PaId { get; set; }

        public string Param { get; set; }

        public string Code { get; set; }

        public string Lang { get; set; }

        public string IdxId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
