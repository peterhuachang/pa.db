﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class VAfSlZone
    {
        public string SttnId { get; set; }

        public string AfId { get; set; }

        public string AfSxId { get; set; }

        public string AfAoId { get; set; }

        public int AfAoCiNo { get; set; }

        public string BkAfId { get; set; }

        public string BkAfSxId { get; set; }

        public string BkAfAoId { get; set; }

        public int BkAfAoCiNo { get; set; }

        public string SlId { get; set; }

        public string SxId { get; set; }

        public string AoId { get; set; }

        public int AoChNo { get; set; }

        public string LdId { get; set; }

        public int LdChNo { get; set; }

        public string CiSxId { get; set; }

        public string CiAoId { get; set; }

        public string CiCiId { get; set; }

        public int CiCiNo { get; set; }

        public string SpId { get; set; }

        public string ZoneId { get; set; }

        public int BcPortId { get; set; }

        public int SlBcStatus { get; set; }

        public int ZoneStatus { get; set; }
    }
}
