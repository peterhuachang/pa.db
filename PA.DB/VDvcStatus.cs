﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class VDvcStatus
    {
        public string SttnId { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string Labeling { get; set; }

        public string Cdesc { get; set; }

        public string Edesc { get; set; }

        public string IpAddr { get; set; }

        public string PmcGrpNum { get; set; }

        public string PmcPntNum { get; set; }

        public string LocId { get; set; }

        public string MuId { get; set; }

        public string AlarmId { get; set; }

        public int AlarmFlag { get; set; }

        public string StatusCname { get; set; }

        public string StatusEname { get; set; }

        public DateTime AlarmTime { get; set; }

        public String DescriptionI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cdesc : Edesc;
            }
        }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    StatusCname : StatusEname;
            }
        }
    }
}
