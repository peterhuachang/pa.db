﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class AutoSentence
    {
        public string SttnId { get; set; }

        public string PaId { get; set; }

        public int Timing { get; set; }

        public string Lang { get; set; }

        public string Cname { get; set; }

        public string Ename { get; set; }

        public int SentenceSeq { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cname : Ename;
            }
        }
    }
}
