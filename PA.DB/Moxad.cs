﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class Moxad
    {
        public string SttnId { get; set; }

        public string MoxaId { get; set; }

        public string MoxaPort { get; set; }

        public string IfType { get; set; }

        public string DvcType { get; set; }

        public string DvcId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string UserStamp { get; set; }

        public DateTime? DateStamp { get; set; }
    }
}
