﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB
{
    public class SchedBcm
    {
        public int Id { get; set; }

        public string SttnId { get; set; }

        public int Pkey { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime EndTimeA { get; set; }

        public string Cname { get; set; }

        public string Ename { get; set; }

        public string VoiceSource { get; set; }

        public string DvcId { get; set; }

        public string CrUser { get; set; }

        public DateTime CrDate { get; set; }

        public string Userstamp { get; set; }

        public DateTime? Datestamp { get; set; }

        public String NameI18N
        {
            get
            {
                return "zh-TW".Equals(Thread.CurrentThread.CurrentCulture.Name) ?
                    Cname : Ename;
            }
        }
    }
}
