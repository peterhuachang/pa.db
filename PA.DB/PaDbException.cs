﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB
{
    public class PaDbException : Exception
    {
        public PaDbException(string message)
            : base(message)
        {
        }

        public PaDbException(string message, Exception innerEx)
            : base(message, innerEx)
        {

        }
    }
}
