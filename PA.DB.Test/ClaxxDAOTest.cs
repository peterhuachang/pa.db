﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ClaxxDAOTest : ConnectionTest
    {
        private ClaxxDAO dao;

        public ClaxxDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<ClaxxDAO>("Class");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Claxx value)
        {
            Console.WriteLine(value.SttnId + " " + value.Cname + " " + value.Ename + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Claxx> values = dao.Search("S295");
            foreach (Claxx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Claxx value = dao.SearchPK("S295","0001");
            PrintResult(value);
        }
    }
}
