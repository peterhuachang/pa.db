﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PA.DB.DAO;
using PA.DB;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ZoneLoopDAOTest : ConnectionTest
    {
        private ZoneLoopDAO dao;

        // protected PASession session;

        public ZoneLoopDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<ZoneLoopDAO>("ZoneLoop");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            List<ZoneLoop> ad = new List<ZoneLoop>();
            ad.AddRange(dao.Search("S295"));
            foreach (ZoneLoop z in ad)
            {

                Console.Write(z.SttnId + " " +z.SpId+ " " + z.ZoneId + " " + z.CrUser + " " + z.CrDate + " " + z.UserStamp + " " + z.DateStamp);
                Console.WriteLine();
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            ZoneLoop z = dao.SearchPK("S295", "0006");
            Console.Write(z.SttnId + " " + z.SpId + " " + z.ZoneId + " " + z.CrUser + " " + z.CrDate + " " + z.UserStamp + " " + z.DateStamp);
            Console.WriteLine();
        }
    }
}