﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VPaCidxDAOTest : ConnectionTest
    {
        private VPaCidxDAO dao;

        public VPaCidxDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VPaCidxDAO>("VPaCidx");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(VPaCidx value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.IdxId + " " + value.Name + " " + value.CardId + " " + value.Cdesc + " " + value.Edesc + " " + value.TimeLen + " " + value.Standard + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp + " " + value.Lang);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VPaCidx value = dao.SearchPK("S270", "0001", "A079");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchCard()
        {
            IList<VPaCidx> values = dao.Search("S270", "0001", "A");
            foreach (VPaCidx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<VPaCidx> values = dao.Search("S270");
            foreach (VPaCidx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPaId()
        {
            IDictionary<string, IList<VPaCidx>> result = dao.Search("S270", "0003");

            foreach (KeyValuePair<string, IList<VPaCidx>> kvp in result)
            {
                Console.WriteLine(kvp.Key);
                foreach (VPaCidx value in kvp.Value)
                {
                    PrintResult(value);
                }
            }
        }
    }
}
