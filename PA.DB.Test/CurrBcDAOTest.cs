﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class CurrBcDAOTest : ConnectionTest
    {
        private CurrBcDAO dao;

        public CurrBcDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<CurrBcDAO>("CurrBc");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(CurrBc value)
        {
            Console.WriteLine(value.Id + " " + value.SttnId + " " + value.Voice + " " + value.BcAreaId + " " + value.CmdFrom + " " + value.DateStamp + " " + value.BcSttnId + " " + value.SrcQue + " " + value.SentenCnt + " " + value.SentenId);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<CurrBc> values = dao.SearchSttn("S295");
            foreach (CurrBc value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchId()
        {
            //  CurrBc value = dao.SearchByPK(1);
            ////  CurrBc value = dao.SearchPK(1);
            //  PrintResult(value);ˇ不
        }
    }
}
