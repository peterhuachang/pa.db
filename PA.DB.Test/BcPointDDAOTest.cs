﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class BcPointDDAOTest : ConnectionTest
    {
        private BcPointDDAO dao;

        public BcPointDDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<BcPointDDAO>("BcPointD");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(BcPointD value)
        {
            Console.WriteLine(value.SttnId + " " + value.PointId + " " + value.ZoneId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            BcPointD value = dao.SearchPK("S295", "01", "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchZone()
        {
            IList<BcPointD> values = dao.Search("S295", "0001");
            foreach (BcPointD value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<BcPointD> values = dao.SearchSttn("S295");
            foreach (BcPointD value in values)
            {
                PrintResult(value);
            }
        }
    }
}
