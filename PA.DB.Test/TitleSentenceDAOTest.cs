﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class TitleSentenceDAOTest : ConnectionTest
    {
        private TitleSentenceDAO titleDao;

        public TitleSentenceDAOTest()
        {
            this.titleDao = this.session.CreateDao<TitleSentenceDAO>("TitleSentence");
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            TitleSentence value = this.titleDao.SearchByPkey("S295", 1, 1);
            Console.WriteLine(value.SentenSeq);
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            TitleSentence sentence = new TitleSentence();
            sentence.Id = 100;
            sentence.SttnId = "S295";
            sentence.PKey = 100;
            sentence.SentenSeq = 1;
            sentence.SentenId = "";
            sentence.CrUser = "paadm";
            sentence.CrDate = DateTime.Now;
            sentence.CrUser = "paadm";
            sentence.Datestamp = DateTime.Now;
            this.titleDao.Insert(sentence);
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            TitleSentence s = new TitleSentence();
            s.SttnId = "S295";
            s.PKey = 1;
            this.titleDao.Delete(s);
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
           TitleSentence sentence=  this.titleDao.SearchById(5);

           
           sentence.SttnId = "S295";
           sentence.PKey = 2;
           sentence.SentenSeq = 1;
           sentence.SentenId = "4242";
           sentence.CrUser = "paadm";
           sentence.CrDate = DateTime.Now;
           sentence.CrUser = "";
           sentence.Datestamp = DateTime.Now;
           this.titleDao.Update(sentence);
        }
    }
}
