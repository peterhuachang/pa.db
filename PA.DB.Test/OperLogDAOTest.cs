﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    class OperLogDAOTest : ConnectionTest
    {
        private OperLogDAO dao;

        public OperLogDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<OperLogDAO>("OperLog");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(OperLog value)
        {
            Console.WriteLine(value.Id + " " + value.LogLanguage + " " + value.DateStamp + " " + value.Type + " " + value.SttnId + " " + value.Context);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            OperLog value = dao.SearchPK(50690);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<OperLog> values = dao.SearchAll("S270");
            foreach (OperLog value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchLog()
        {
            IList<OperLog> valuesEn = dao.SearchLog("S270", "O", "0");
            foreach (OperLog value in valuesEn)
            {
                PrintResult(value);
            }

            IList<OperLog> values = dao.SearchLog("S270", "O");

            foreach (OperLog value in values)
            {
                PrintResult(value);
            }
        }
    }
}
