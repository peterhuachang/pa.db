﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PaCidxDAOTest : ConnectionTest
    {
        private PaCidxDAO dao;

        public PaCidxDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PaCidxDAO>("PaCidx");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(PaCidx value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.IdxId + " " + value.Name + " " + value.CardId + " " + value.Cdesc + " " + value.Edesc + " " + value.TimeLen + " " + value.Standard + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            PaCidx value = dao.SearchPK("S270", "0001", "A079");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchCard()
        {
            IList<PaCidx> values = dao.Search("S295", "0001", "A");
            foreach (PaCidx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<PaCidx> values = dao.Search("S295");
            foreach (PaCidx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPaId()
        {
            IDictionary<string, IList<PaCidx>> result = dao.Search("S295", "0003");

            foreach (KeyValuePair<string, IList<PaCidx>> kvp in result)
            {
                Console.WriteLine(kvp.Key);
                foreach (PaCidx value in kvp.Value)
                {
                    PrintResult(value);
                }
            }
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            PaCidx pa = new PaCidx();
            pa.SttnId = "123";
            pa.PaId = "321";
            pa.IdxId = "123";
            pa.Name = "";
            pa.CardId = "A";
            pa.Cdesc = "ccc";
            pa.Edesc = "BBB";
            pa.TimeLen = 453;
            pa.CrUser = "321";
            pa.CrDate = DateTime.Now;
            pa.UserStamp = "123";
            pa.DateStamp = DateTime.Now;
            pa.Standard = "1";
            dao.Insert(pa);
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            PaCidx pa = dao.SearchPK("S295","0001","A001");
            pa.Cdesc = "測試123";
            pa.UserStamp = "user00";
            pa.DateStamp = DateTime.Now;

            dao.Update(pa);
            IList<PaCidx> values = dao.Search("S295", "0001", "A");
            foreach (PaCidx value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            PaCidx value = dao.SearchPK("123","321","123");
            dao.Delete(value);
        }
    }
}
