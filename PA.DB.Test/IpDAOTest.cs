﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
     [NUnit.Framework.TestFixture]
    public class IpDAOTest : ConnectionTest
    {private IpDAO dao;

        public IpDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<IpDAO>("Ip");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Ip value)
        {
            Console.WriteLine(value.ServerId + " " + value.ServerType + " " + value.Seq + " "+value.IP+" "+value.AlarmId+" " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<Ip> values = dao.SearchAll();
            foreach (Ip value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchId()
        {
            IList<Ip> values = dao.Search("S295");
            foreach (Ip value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Ip value = dao.SearchPK("S295", "CN",2);
            PrintResult(value);
        }
    }
}
