﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ZoneDAOTest : ConnectionTest
    {
        private ZoneDAO dao;

        // protected PASession session;

        public ZoneDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<ZoneDAO>("Zone");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            List<Zone> ad = new List<Zone>();
            ad.AddRange(dao.Search("S295"));
            foreach (Zone z in ad)
            {

                Console.Write(z.SttnId + " " + z.ZoneId + " " + z.Cname + " " + z.Ename + " " + z.BcStatus + " " + z.CrUser + " " + z.CrDate + " " + z.UserStamp + " " + z.DateStamp);
                Console.WriteLine();
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Zone z = dao.SearchPK("S295", "0006");
            Console.Write(z.SttnId + " " + z.ZoneId + " " + z.Cname + " " + z.Ename + " " + z.BcStatus + " " + z.CrUser + " " + z.CrDate + " " + z.UserStamp + " " + z.DateStamp);
        }
    }
}
