﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VBcPointDAOTest : ConnectionTest
    {
        private VBcPointDAO dao;

        public VBcPointDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VBcPointDAO>("VBcPoint");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(VBcPoint value)
        {
            Console.WriteLine(value.SttnId + " " + value.BcAreaId + " " + value.PointId + " " + value.ZoneId + " " + value.AreaCname + " " + value.AreaEname + " " + value.VoiceSource + " " + value.PlatForm + " " + value.ZoneCname + " " + value.ZoneEname + " " + value.ZoneBcStatus);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VBcPoint value = dao.SearchPK("S295", "0201", "05", "0008");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VBcPoint> values = dao.Search("S295");
            foreach (VBcPoint value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchArea()
        {
            IList<VBcPoint> values = dao.Search("S270", "0201");
            foreach (VBcPoint value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchVoiceSource()
        {
            IList<VBcPoint> values = dao.SearchByVoiceSource("S295", "GM");
            foreach (VBcPoint value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchAreaZone()
        {
            IList<VBcPoint> values = dao.SearchAreaZone("S295","0205","05");
            foreach (VBcPoint value in values)
            {
                PrintResult(value);
            }
        }
    }
}
