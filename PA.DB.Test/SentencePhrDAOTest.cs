﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SentencePhrDAOTest : ConnectionTest
    {
        private SentencePhrDAO dao;

        public SentencePhrDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<SentencePhrDAO>("SentencePhr");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(SentencePhr value)
        {
            Console.WriteLine(value.Id + " " + value.SttnId + " " + value.SentenId + " " + value.Seq + " " + value.IdxId + " " +  value.CrUser + " " + value.CrDate + " " + value.Userstamp + " " + value.Datestamp);
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            SentencePhr a = new SentencePhr();
            a.SttnId = "123";
            a.SentenId = "321";
            a.Seq = 2;
            a.IdxId = "123";
            a.CrUser = "321";
            a.CrDate = DateTime.Now;
            a.Userstamp = "123";
            a.Datestamp = DateTime.Now;
            dao.Insert(a);
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            SentencePhr value = dao.SearchById(22);

            value.SttnId = "xx";
            value.SentenId = "xx";
            value.Seq = 2;
            value.IdxId = "xx";
            value.CrUser = "xx";
            value.CrDate = DateTime.Now;
            value.Userstamp = "xx";
            value.Datestamp = DateTime.Now;
            dao.Update(value);
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            SentencePhr value = dao.SearchById(22);
            dao.Delete(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<SentencePhr> value = dao.SearchAll();
            foreach (SentencePhr item in value)
            {
                PrintResult(item);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSentence()
        {
            IList<SentencePhr> value = dao.SearchSentence("S295","0013");
            foreach (SentencePhr item in value)
            {
                PrintResult(item);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            SentencePhr value = dao.SearchById(1);
            PrintResult(value);
        }
    }
}
