﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class BcAreaPointDAOTest : ConnectionTest
    {
        private BcAreaPointDAO dao;

        public BcAreaPointDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<BcAreaPointDAO>("BcAreaPoint");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(BcAreaPoint value)
        {
            Console.WriteLine(value.BcAreaSttn + " " + value.BcAreaId + " " + value.PointId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            BcAreaPoint value = dao.SearchPK("S295", "0201", "02");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchBcAreaId()
        {
            IList<BcAreaPoint> values = dao.Search("S295","0201");
            foreach (BcAreaPoint value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<BcAreaPoint> values = dao.SearchSttn("S295");
            foreach (BcAreaPoint value in values)
            {
                PrintResult(value);
            }
        }
    }

}
