﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ProcessDAOTest : ConnectionTest
    {
        private ProcessDAO dao;

        // protected PASession session;

        public ProcessDAOTest()
        {
            this.dao = this.session.CreateDao<ProcessDAO>("Process");
        }

        public static void PrintResult(Process value)
        {
            Console.WriteLine(value.Id + " " + value.ExeSttn + " " + value.ProcName + " " + value.QueName + " " + value.ProcId + " " + value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.ExeSeq + " " + value.ExeFile + " " + value.RunFlag + " " + value.InitFlag);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Process value = dao.SearchPK(1);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSttnProcName()
        {
            IList<Process> values = dao.Search("S295", "ASDRVS2950002");
            foreach (Process value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Process> values = dao.Search("S295");
            foreach (Process value in values)
            {
                PrintResult(value);
            }
        }
    }
}
