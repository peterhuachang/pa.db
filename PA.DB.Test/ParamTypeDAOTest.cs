﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ParamTypeDAOTest : ConnectionTest
    {
        private ParamTypeDAO dao;

        // protected PASession session;

        public ParamTypeDAOTest()
        {
            this.dao = this.session.CreateDao<ParamTypeDAO>("ParamType");
        }

        public static void PrintResult(ParamType value)
        {
            Console.Write(value.SttnId + " " + value.TypeName + " " + value.Cdesc + " " + value.Edesc + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
            Console.WriteLine();
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            ParamType value = dao.SearchPK("S295", "ACK_TIME");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<ParamType> values = dao.Search("S295");
            foreach (ParamType value in values)
            {
                PrintResult(value);
            }
        }
    }
}
