﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PlatformDAOTest : ConnectionTest
    {
        private PlatformDAO dao;

        public PlatformDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PlatformDAO>("Platform");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Platform value)
        {
            Console.WriteLine(value.SttnId + " " + value.PlatformNo + " " + value.VoiceSource + " " + value.DevId);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Platform value = dao.SearchPK("S205", "1A");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Platform> values = dao.Search("S205");
            foreach (Platform value in values)
            {
                PrintResult(value);
            }
        }
    }
}
