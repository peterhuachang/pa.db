﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VPointDDAOTest : ConnectionTest
    {
        private VPointDDAO dao;

        public VPointDDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VPointDDAO>("VPointD");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(VPointD value)
        {
            Console.WriteLine(value.SttnId + " " + value.PointId);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VPointD value = dao.SearchPK("S295", "05");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VPointD> values = dao.SearchSttn("S295");
            foreach (VPointD value in values)
            {
                PrintResult(value);
            }
        }
    }
}
