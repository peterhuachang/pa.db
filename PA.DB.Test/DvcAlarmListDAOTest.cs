﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DvcAlarmListDAOTest : ConnectionTest
    {
        private DvcAlarmListDAO dao;

        public DvcAlarmListDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DvcAlarmListDAO>("DvcAlarmList");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(DvcAlarmList value)
        {
            Console.WriteLine(value.DvcType + " " + value.AlarmId + " " + value.AlarmFlag + " " + value.Cdesc + " "+value.Edesc+" " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            DvcAlarmList value = dao.SearchPK("CC","50",0);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<DvcAlarmList> values = dao.SearchAll();
            foreach (DvcAlarmList value in values)
            {
                PrintResult(value);
            }
        }
    }
}

