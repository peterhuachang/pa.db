﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class AsAfDAOTest : ConnectionTest
    {
        public string sttnId ;

        private AsAfDAO dao;

        public AsAfDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<AsAfDAO>("AsAf");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(AsAf value)
        {
            Console.WriteLine(value.SttnId + " " + value.AsId + " " + value.Seq + " " + value.AfId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            AsAf value = dao.SearchPK(this.sttnId, "0001", 4);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchByAfId()
        {
            AsAf value = dao.SearchByAfId(this.sttnId, "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<AsAf> values = dao.SearchSttn(this.sttnId);
            foreach (AsAf value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchAF()
        {
            IList<AsAf> values = dao.Search(this.sttnId, "0001");
            foreach(AsAf value in values)
            {
                PrintResult(value);
            }
        }
    }
}
