﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class AutoSentenceDAOTest : ConnectionTest
    {
        private AutoSentenceDAO dao;

        public AutoSentenceDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<AutoSentenceDAO>("AutoSentence");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(AutoSentence value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.Timing + " " + value.SentenceSeq + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            AutoSentence value = dao.SearchPK("S270", "0001", 1, 1);

            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSentence()
        {
            IList<AutoSentence> values = dao.Search("S270", "0001", 1);
            foreach (AutoSentence value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<AutoSentence> values = dao.SearchSttn("S270");
            foreach (AutoSentence value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            try
            {
                AutoSentence pa = new AutoSentence();
                pa.SttnId = "S270";
                pa.PaId = "3210";
                pa.Timing = 1;
                pa.Lang = "1";
                pa.Cname = "in";
                pa.Ename = "in";
                pa.SentenceSeq = 1;
                pa.CrUser = "321";
                pa.CrDate = DateTime.Now;
                //pa.UserStamp = "123";
                //pa.DateStamp = DateTime.Now;
                dao.Insert(pa);
            }
            catch(Exception ex)
            {

            }
           
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            try
            {
                AutoSentence value = dao.SearchPK("S270", "3210", 1, 1);
                value.Cname = "update";
                value.Ename = "update";
                value.UserStamp = "ooooo";
                value.DateStamp = DateTime.Now;
                dao.Update(value);

            }
            catch(Exception ex)
            {

            }
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            AutoSentence value = dao.SearchPK("S270", "3210", 1, 1);
            dao.Delete(value);
        }
    }
}
