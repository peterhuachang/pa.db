﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PudDAOTest : ConnectionTest
    {
        private PudDAO dao;

        public PudDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PudDAO>("Pud");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Pud value)
        {
            Console.WriteLine(value.SttnId + " " + value.PuId + " " + value.IpV + " " + value.IpFV + " " + value.OpV + " " + value.OpLoad + " " + value.IpFrequence + " " + value.Battery + " " + value.Temperature + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Pud value = dao.SearchPK("S295", "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Pud> values = dao.SearchSttn("S295");
            foreach (Pud value in values)
            {
                PrintResult(value);
            }
        }
    }
}
