﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VParamTypeValueDAOTest : ConnectionTest
    {

        private VParamTypeValueDAO dao;

        // protected PASession session;

        public VParamTypeValueDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VParamTypeValueDAO>("VParamTypeValue");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintResult(VParamTypeValue value)
        {
            Console.WriteLine(value.Id + " " + value.SttnId + " " + value.ParamType + " " + value.ParamId + " " + value.ParamVal + " " + value.TypeCdesc + " " + value.TypeEdesc + " " + value.ValueCdesc + " " + value.ValueEdesc);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VParamTypeValue> values = dao.SearchSttn("S295");
            foreach (VParamTypeValue value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VParamTypeValue value = dao.SearchPK(1, "S295", "ACK_TIME");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchParamType()
        {
            IList<VParamTypeValue> values = dao.Search("S295", "LOCATION");
            foreach (VParamTypeValue value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchParam()
        {
            VParamTypeValue value = dao.Search("S295", "ACK_TIME", "AS");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchId()
        {
            VParamTypeValue value = dao.SearchId(5);
            PrintResult(value);
        }
    }
}
