﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PaCardDAOTest : ConnectionTest
    {
        private PaCardDAO dao;

        public PaCardDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PaCardDAO>("PaCard");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(PaCard value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.CardId + " " + value.Cdesc + " " + value.Edesc + " " + value.Meno + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            PaCard value = dao.SearchPK("S295", "0001", "A");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<PaCard> values = dao.SearchSttn("S295");
            foreach (PaCard value in values)
            {
                PrintResult(value);
            }
        }
    }
}
