﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class BcAreaDAOTest : ConnectionTest
    {
        private BcAreaDAO dao;

        public BcAreaDAOTest()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("zh-TW");
            try
            {
                this.dao = this.session.CreateDao<BcAreaDAO>("BcArea");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(BcArea value)
        {
            Console.WriteLine(value.SttnId + " " + value.BcAreaSttn + " " + value.BcAreaId + " ," + value.NameI18N + " " + value.Ename + " " + value.VoiceSource + " " + value.PlatForm + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            BcArea value = dao.SearchPK("S295", "S295", "0202");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnIdVoiceSource()
        {
            IList<BcArea> values = dao.Search("S295", "GM");
            foreach (BcArea value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<BcArea> values = dao.Search("S295"); 
            foreach (BcArea value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchAreaVoice()
        {
            IList<BcArea> values = dao.Search("0203", "GM");
            foreach (BcArea value in values)
            {
                PrintResult(value);
            }
        }
    }
}
