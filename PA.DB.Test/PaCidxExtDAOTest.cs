﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PaCidxExtDAOTest : ConnectionTest
    {
        private PaCidxExtDAO dao;

        public PaCidxExtDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PaCidxExtDAO>("PaCidxExt");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(PaCidxExt value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.IdxId + " " +  value.CardId + " " + value.Lang);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            PaCidxExt value = dao.SearchPK("S270", "0001", "A079");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchCard()
        {
            IList<PaCidxExt> values = dao.SearchByCardId("S270", "0001", "A");
            foreach (PaCidxExt value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<PaCidxExt> values = dao.SearchBySttnId("S270");
            foreach (PaCidxExt value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            PaCidxExt pa = new PaCidxExt();
            pa.SttnId = "123";
            pa.PaId = "321";
            pa.IdxId = "123";
            pa.CardId = "A";
            pa.Lang = "0";
            dao.Insert(pa);
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            PaCidxExt pa = dao.SearchPK("123", "321", "123");
            pa.Lang = "3";

            dao.Update(pa);
            IList<PaCidxExt> values = dao.SearchByCardId("S295", "0001", "A");
            foreach (PaCidxExt value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            PaCidxExt value = dao.SearchPK("123", "321", "123");
            dao.Delete(value);
        }
    }
}
