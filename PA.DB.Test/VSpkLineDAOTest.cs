﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VSpkLineDAOTest : ConnectionTest
    {
        private VSpkLineDAO dao;

        public VSpkLineDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VSpkLineDAO>("VSpkLine");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintResult(VSpkLine value)
        {
            Console.Write(value.SttnId + " " + value.ZoneId + " " + value.SpId + " " + value.SlId + " " + value.SttnCname + " " + value.SttnEname + " " + value.ZoneCname + " " + value.ZoneEname + " " + value.ZoneBcStatus + " " + value.SsId + " " + value.CbId + " " + value.BcPortId + " " + value.SlBcStatus);
            Console.WriteLine();
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<VSpkLine> values = dao.SearchAll();
            foreach (VSpkLine value in values)
            {
                PrintResult(value);
            }
        }
        
        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VSpkLine value = dao.SearchPK("S295", "0511");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnZone()
        {
            IList<VSpkLine> values = dao.Search("S295", "0001");
            foreach (VSpkLine value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VSpkLine> values = dao.SearchBySttnId("S295");
            foreach (VSpkLine value in values)
            {
                PrintResult(value);
            }
        }
    }
}
