﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SchedPattnSetDAOTest : ConnectionTest
    {
        private SchedPattnSetDAO dao;

        // protected PASession session;

        public SchedPattnSetDAOTest()
        {
            this.dao = this.session.CreateDao<SchedPattnSetDAO>("SchedPattnSet");
        }

        [NUnit.Framework.Test]
        public void SerachAll()
        {
            IList<SchedPattnSet> values = this.dao.SearchAll();
            foreach (SchedPattnSet s in values)
            {
                Console.WriteLine(s.Pkey);
            }
        }

        [NUnit.Framework.Test]
        public void SearchPKey()
        {
            SchedPattnSet value = this.dao.SearchByPkey("S295", 1);
            Console.WriteLine(value.SttnId + " " + value.Pkey + " " + value.PattnId + " " + value.Ename + " " + value.Cname);
        }

        [NUnit.Framework.Test]
        public void Search()
        {
            IList<SchedPattnSet> values = this.dao.Search("S295","0001","PA","0001");
            foreach (SchedPattnSet s in values)
            {
                Console.WriteLine(s.Pkey);
            }
        }

        [NUnit.Framework.Test]
        public void SearchAllPattern()
        {
            IList<SchedPattnSet> values = this.dao.SearchAllPattern("S295");
            foreach (SchedPattnSet s in values)
            {
                Console.WriteLine(s.PattnId);
            }
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            SchedPattnSet sched = new SchedPattnSet();
            sched.SttnId = "S295";
            sched.Pkey = 100;
            sched.PattnId = "Week";
            sched.VoiceSource = "PA";
            sched.DvcId = "0001";
            sched.Cname = "測試";
            sched.Ename = "Test";
            sched.StartTime = "";
            sched.EndTime = "";
            sched.EndTimeA = "";
            sched.Interval = 1;
            sched.Duration = 1;
            sched.Repeat = 1;
            sched.CrUser = "paadm";
            sched.CrDate = DateTime.Now;
            sched.Userstamp = "g";
            sched.Datestamp = DateTime.Now;
            dao.Insert(sched);

            Console.WriteLine(sched.Pkey);
        }

        [NUnit.Framework.Test]
        public void TestUpate()
        {
            SchedPattnSet sched = this.dao.SearchByPkey("S295",19);

            sched.SttnId = "S295";

            sched.PattnId = "0003";
            sched.VoiceSource = "FM";
            sched.DvcId = "0001";
            sched.Cname = "aaaa";
            sched.Ename = "Test";
            sched.StartTime = "wqfw";
            sched.EndTime = "";
            sched.EndTimeA = "";
            sched.Interval = 1;
            sched.Duration = 1;
            sched.Repeat = 1;
            sched.CrUser = "paadm";
            sched.CrDate = DateTime.Now;
            sched.Userstamp = "g";
            sched.Datestamp = DateTime.Now;
            dao.Update(sched);
        }
    }
}
