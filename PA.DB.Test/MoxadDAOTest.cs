﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class MoxadDAOTest : ConnectionTest
    {
        private MoxadDAO dao;

        public MoxadDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<MoxadDAO>("Moxad");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Moxad value)
        {
            Console.WriteLine(value.SttnId + " " + value.MoxaId + " " + value.MoxaPort + " " + value.IfType + " " + value.DvcType + " " + value.DvcId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Moxad value = dao.SearchPK("S295", "0001", "1001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Moxad> values = dao.SearchSttn("S295");
            foreach (Moxad value in values)
            {
                PrintResult(value);
            }
        }
    }
}
