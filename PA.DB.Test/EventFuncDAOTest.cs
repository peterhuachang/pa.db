﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class EventFuncDAOTest : ConnectionTest
    {
        private EventFuncDAO dao;

        public EventFuncDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<EventFuncDAO>("EventFunc");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(EventFunc value)
        {
            Console.WriteLine(value.SttnId + " " + value.EventName + " " + value.EventStep + " " + value.FuncName + " " + value.GoNext);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<EventFunc> values = dao.SearchSttn("S295");
            foreach (EventFunc value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            EventFunc value = dao.SearchPK("S295", "GMBCDiscACtl", 5);
            PrintResult(value);
        }
    }
}
