﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VDeviceDAOTest : ConnectionTest
    {

        private VDeviceDAO dao;

        // protected PASession session;

        public VDeviceDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VDeviceDAO>("VDevice");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintResult(VDevice value)
        {
            Console.WriteLine(value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.Labeling + " " + value.DtlCdesc + " " + value.DtlEdesc + " " + value.IpAddr + " " + value.PmcGrpNum + " " + value.PmcPntNum + " " + value.LocId + " " + value.MuId + " " + value.TypeCdesc + " " + value.TypeEdesc);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VDevice> values = dao.SearchBySttn("S295");
            foreach (VDevice value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VDevice value = dao.SearchPK("S295", "AF", "0030");
            PrintResult(value);

        }

        [NUnit.Framework.Test]
        public void TestSearchDeviceType()
        {
            IList<VDevice> values = dao.Search("S295", "AF");
            foreach (VDevice value in values)
            {
                PrintResult(value);
            }
        }
    }
}
