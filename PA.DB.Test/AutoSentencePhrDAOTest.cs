﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class AutoSentencePhrDAOTest : ConnectionTest
    {
        private AutoSentencePhrDAO dao;

        public AutoSentencePhrDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<AutoSentencePhrDAO>("AutoSentencePhr");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(AutoSentencePhr value)
        {
            Console.WriteLine(value.SttnId + " " + value.PaId + " " + value.PhraseSeq + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            IList<AutoSentencePhr> value = dao.Search("S260", "0001", 1, 1);

            foreach (AutoSentencePhr phr in value)
            {
                PrintResult(phr);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSentence()
        {
            IList<AutoSentencePhr> values = dao.Search("S260", "0001", 1, 1,"1");
            foreach (AutoSentencePhr value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSttnId()
        {
            IList<AutoSentencePhr> values = dao.SearchSttn("S260");
            foreach (AutoSentencePhr value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            AutoSentencePhr pa = new AutoSentencePhr();
            pa.SttnId = "S260";
            pa.PaId = "3210";
            pa.Timing = 1;
            pa.Lang = "1";
            pa.SentenceSeq = 1;
            pa.PhraseType = 1;
            pa.CrUser = "321";
            pa.CrDate = DateTime.Now;
            pa.UserStamp = "123";
            pa.DateStamp = DateTime.Now;
            pa.Phrase = "123123";

            for (int a = 1; a < 3; a++)
            {
                pa.PhraseSeq = a;
                dao.Insert(pa);
            }
        }

        //[NUnit.Framework.Test]
        //public void TestUpdate()
        //{
        //    AutoSentencePhr pa = dao.SearchPK("S260", "3210", 1, "1", 1, 1);
        //    pa.Phrase = "update";
        //    pa.UserStamp = "user00";
        //    pa.DateStamp = DateTime.Now;

        //    dao.Update(pa);
        //}

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            AutoSentencePhr value = dao.SearchPK("S260", "3210", 1, "1", 1, 2);
            dao.Delete(value);
        }
    }
}
