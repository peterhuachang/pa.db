﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    public class PaCidxHistDAOTest : ConnectionTest
    {
        private PaCidxHistDAO dao;

        public PaCidxHistDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<PaCidxHistDAO>("PaCidxHist");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            PaCidxHist value = dao.SearchPK(1);
            Console.Write(value.UpdateSeq + " " + value.SqlText);

        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            List<PaCidxHist> result = new List<PaCidxHist>();
            result.AddRange(dao.SearchAll());
            foreach (PaCidxHist p in result)
            {

                Console.Write(p.UpdateSeq + " " + p.SqlText );
                Console.WriteLine();
            }

        }
    }
}
