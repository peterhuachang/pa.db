﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class AdswDAOTest : ConnectionTest
    {
        private AdswDAO dao;

        // protected PASession session;

        public AdswDAOTest()
        {
            this.dao = this.session.CreateDao<AdswDAO>("Adsw");
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            Adsw a = new Adsw();
            a.SttnId = "ssss";
            a.AdswId = "b";
            a.ChannelId = 2;
            a.Type = "c";
            a.DvcType = "d";
            a.DvcId = "e";
            a.CrUser = "f";
            a.CrDate = DateTime.Now;
            a.Userstamp = "g";
            a.Datestamp = DateTime.Now;
            dao.Insert(a);
        }


        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            List<Adsw> ad = new List<Adsw>();
            ad.AddRange(dao.SearchAll());
            foreach (Adsw a in ad)
            {

                Console.WriteLine(a);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchID()
        {
            Console.WriteLine(dao.SearchById(1));
        }
    }
}
