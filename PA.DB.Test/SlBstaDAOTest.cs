﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SlBstaDAOTest : ConnectionTest
    {
        private SlBstaDAO dao;

        public SlBstaDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<SlBstaDAO>("SlBsta");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(SlBsta value)
        {
            Console.WriteLine(value.SttnId + " " + value.SlId + " " + value.SsId + " " + value.SpId + " " + value.CbId + " " + value.BcPointId + " " + value.BcStatus + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            SlBsta value = dao.SearchPK("S295", "0116");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<SlBsta> values = dao.SearchSttn("S295");
            foreach (SlBsta value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSpId()
        {
            IList<SlBsta> values = dao.SearchSPId("S270", "0001");
            foreach (SlBsta value in values)
            {
                PrintResult(value);
            }
        }
    }
}
