﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VDvcMoxaDAOTest : ConnectionTest
    {
        private VDvcMoxaDAO dao;

        public VDvcMoxaDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VDvcMoxaDAO>("VDvcMoxa");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintResult(VDvcMoxa value)
        {
            Console.WriteLine(value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.Labeling + " " + value.Cdesc + " " + value.Edesc + " " + value.IpAddr + " " + value.PmcGrpNum + " " + value.PmcPntNum + " " + value.LocId + " " + value.MuId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp + " " + value.MoxaPort + " " + value.IfType + " " + value.MoxaId);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            VDvcMoxa value = dao.SearchPK("S295", "PA", "0003");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VDvcMoxa> values = dao.SearchSttn("S295");
            foreach (VDvcMoxa value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchDvcType()
        {
            IList<VDvcMoxa> values = dao.Search("S295", "SC");
            foreach (VDvcMoxa value in values)
            {
                PrintResult(value);
            }
        }
    }
}
