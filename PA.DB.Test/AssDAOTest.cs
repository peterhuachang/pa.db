﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class AssDAOTest : ConnectionTest
    {
        private AssDAO dao;

        public AssDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<AssDAO>("Ass");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Ass value)
        {
            Console.WriteLine(value.SttnId + " " + value.AsIdp + " " + value.IdNum + " " + value.AsIdc + " " + value.AsStatus + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Ass value = dao.SearchPK("S295", "0003", "001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Ass> values = dao.SearchSttn("S295");
            foreach (Ass value in values)
            {
                PrintResult(value);
            }
        }
    }
}
