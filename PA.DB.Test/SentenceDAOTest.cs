﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SentenceDAOTest : ConnectionTest
    {
        private SentenceDAO dao;

    public SentenceDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<SentenceDAO>("Sentence");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

    public static void PrintResult(Sentence value)
        {
            Console.WriteLine(value.Id + " " +value.SttnId + " " +  value.SentenId + " " + value.PaId + " " + value.ClassId + " " + value.Cname + " " + value.Ename + " " +value.SentenLen+" "+ value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

    [NUnit.Framework.Test]
    public void TestInsert()
    {
        Sentence value = new Sentence();
        value.SttnId = "S333";
        value.SentenId = "0055";
        value.PaId = "0008";
        value.ClassId = "0008";
        value.Cname = "cname";
        value.Ename = "ename";
        value.SentenLen = 50;
        value.CrUser = "paaaaa";
        value.CrDate = DateTime.Now;
        value.UserStamp = "paaaaa";
        value.DateStamp = DateTime.Now;
        dao.Insert(value);
    }

    [NUnit.Framework.Test]
    public void TestUpdate()
    {
        Sentence value = dao.SearchBySentenceId("0001");

        value.SttnId = "A1";
        value.SentenId = "b22";
        value.PaId = "3";
        value.ClassId = "vv";
        value.Cname = "vv";
        value.Ename = "vv";
        value.SentenLen = 50;
        value.CrUser = "vv";
        value.CrDate = DateTime.Now;
        value.UserStamp = "vv";
        value.DateStamp = DateTime.Now;
        dao.Update(value);
    }

    [NUnit.Framework.Test]
    public void TestDelete()
    {
        Sentence value = dao.SearchBySentenceId("0001");
        dao.Delete(value);
    }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<Sentence> value = dao.SearchAll();
            foreach (Sentence item in value)
            {
                PrintResult(item);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Sentence value = dao.SearchBySentenceId("0001");

            PrintResult(value);
            
        }

        [NUnit.Framework.Test]
        public void TestSearchByClassId()
        {
            IList<Sentence> value = dao.Search("S295", "0001");
            foreach (Sentence item in value)
            {
                PrintResult(item);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchByPaClass()
        {
            IList<Sentence> value = dao.Search("S295", "0002", "0001");
            foreach (Sentence item in value)
            {
                PrintResult(item);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchSentence()
        {
            Sentence s = dao.SearchSentence("S295", "0023", "0001");
            PrintResult(s);
        }
    }
}
