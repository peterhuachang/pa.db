﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DvcAlarmHistDAOTest : ConnectionTest
    {
        private DvcAlarmHistDAO dao;

        public DvcAlarmHistDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DvcAlarmHistDAO>("DvcAlarmHist");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(DvcAlarmHist value)
        {
            Console.WriteLine(value.Id+" "+value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.AlarmId + " " + value.AlarmFlag + " " + value.AlarmTime + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            DvcAlarmHist value = dao.SearchPK(1);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<DvcAlarmHist> values = dao.Search("S295");
            foreach (DvcAlarmHist value in values)
            {
                PrintResult(value);
            }
        }
    }
}
