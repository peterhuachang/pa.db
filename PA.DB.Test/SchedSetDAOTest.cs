﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SchedSetDAOTest : ConnectionTest
    {
        private SchedSetDAO dao;

        public SchedSetDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<SchedSetDAO>("SchedSet");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        [NUnit.Framework.Test]
        public void TestInsert()
        {
            SchedSet s = new SchedSet();
            s.SttnId = "S295";
            s.SchedDate = DateTime.Now;
            s.PattnId = "0001";
            s.CrUser = "paadmin";
            s.CrDate = DateTime.Now;
            s.UserStamp = "paadmin";
            s.DateStamp = DateTime.Now;
            this.dao.Insert(s);

        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            SchedSet s = this.dao.SearchById(100);
            s.SttnId = "S295";
            s.SchedDate = DateTime.Now;
            s.PattnId = "0002";
            s.CrUser = "paadmin";
            s.CrDate = DateTime.Now;
            s.UserStamp = "paadmin";
            s.DateStamp = DateTime.Now;
            this.dao.Update(s);
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            SchedSet s = new SchedSet();
            s.SttnId = "S295";
            s.SchedDate = new DateTime(2014, 10, 15);
            this.dao.Delete(s);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<SchedSet> values = this.dao.SearchAll();
            foreach (SchedSet s in values)
            {
                Console.WriteLine(s.Id + " " + s.SttnId + " " + s.SchedDate + " " + s.PattnId + " " + s.CrUser + " " + s.CrDate + " " + s.UserStamp + " " + s.DateStamp);
            }
        }
    }
}
