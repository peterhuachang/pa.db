﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class TimetableDAOTest : ConnectionTest
    {
        private TimetableDAO timetableDao;

        public TimetableDAOTest()
        {
            this.timetableDao = this.session.CreateDao<TimetableDAO>("Timetable");
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Timetable value = this.timetableDao.SearchPK("S260","0110");
            Console.WriteLine(value.TrainNo);
        }

        [NUnit.Framework.Test]
        public void TestInsert()
        {
            TitleSentence sentence = new TitleSentence();
            sentence.Id = 100;
            sentence.SttnId = "S295";
            sentence.PKey = 100;
            sentence.SentenSeq = 1;
            sentence.SentenId = "";
            sentence.CrUser = "paadm";
            sentence.CrDate = DateTime.Now;
            sentence.CrUser = "paadm";
            sentence.Datestamp = DateTime.Now;
            //this.timetableDao.Insert(sentence);
        }

        [NUnit.Framework.Test]
        public void TestDelete()
        {
            TitleSentence s = new TitleSentence();
            s.SttnId = "S295";
            s.PKey = 1;
            //this.timetableDao.Delete(s);
        }

        [NUnit.Framework.Test]
        public void TestUpdate()
        {
            Timetable value = this.timetableDao.SearchPK("S260", "0110");

            value.DelayTime = 100;
            this.timetableDao.Update(value);
        }
    }
}
