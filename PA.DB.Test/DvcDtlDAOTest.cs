﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DvcDtlDAOTest : ConnectionTest
    {
        private DvcDtlDAO dao;

        public DvcDtlDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DvcDtlDAO>("DvcDtl");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(DvcDtl value)
        {
            Console.WriteLine(value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.Labeling + " " + value.Cdesc + " " + value.Edesc + " " + value.IpAddr + " " + value.PmcGrpNum + " " + value.PmcPntNum + " " + value.LocId + " " + value.MuId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<DvcDtl> values = dao.SearchSttn("S295");
            foreach (DvcDtl value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPA()
        {
            IList<DvcDtl> values = dao.SearchManualPAs("S295");
            foreach (DvcDtl value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            DvcDtl value = dao.SearchPK("S295", "AM", "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchDvcType()
        {
            IList<DvcDtl> values = dao.Search("S295", "AF");
            foreach (DvcDtl value in values)
            {
                PrintResult(value);
            }
        }
    }
}
