﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class MsgDAOTest : ConnectionTest
    {
        private MsgDAO dao;

        public MsgDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<MsgDAO>("Msg");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Msg value)
        {
            Console.WriteLine(value.MsgId + " " + value.Cdesc + " " + value.Edesc + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<Msg> values = dao.SearchAll();
            foreach (Msg value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Msg value = dao.SearchPK("E_OP509");
            PrintResult(value);
        }
    }
}
