﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class BackUpTest : ConnectionTest
    {
        private BackUpDAO b;

        public BackUpTest()
        {
            this.b = this.session.CreateDao<BackUpDAO>("BackUp");
        }

        [NUnit.Framework.Test]
        public void TestSearchTable()
        {
            List<string> cc = this.b.Generate("pa_tb_msg");
            foreach (string c in cc)
            {
                Console.WriteLine(c);
            }
        }

        [NUnit.Framework.Test]
        public void Testss()
        {
            List<string> a = this.b.SearchAllTable();
            foreach (string s in a)
            {
                Console.WriteLine(s);
                List<string> cc = this.b.Generate(s);
                foreach (string c in cc)
                {
                    Console.WriteLine(c);
                }
            }
        }

        [NUnit.Framework.Test]
        public void TestReadFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(@"C:\PASystem\ConfigData_Backup\Test.txt"))
                {
                    String line = sr.ReadToEnd();
                    this.b.ExecuteCommand(line);
                    Console.WriteLine(line);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}
