﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DddDAOTest : ConnectionTest
    {
        private DddDAO dao;

        public DddDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DddDAO>("Ddd");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Ddd value)
        {
            Console.WriteLine(value.SttnId + " " + value.DdId + " " + value.SttnIdT + " " + value.DdIdT + " " + value.CnnName + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Ddd value = dao.SearchPK("S295", "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Ddd> values = dao.SearchSttn("S295");
            foreach (Ddd value in values)
            {
                PrintResult(value);
            }
        }
    }
}
