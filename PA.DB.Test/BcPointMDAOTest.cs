﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class BcPointMDAOTest : ConnectionTest
    {
        private BcPointMDAO dao;

        public BcPointMDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<BcPointMDAO>("BcPointM");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(BcPointM value)
        {
            Console.WriteLine(value.SttnId + " " + value.PointId + " " + value.VoiceSource + " " + value.Cdesc + " " + value.Edesc + " " + value.DevId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            BcPointM value = dao.SearchPK("S295", "02", "GM", "0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<BcPointM> values = dao.SearchSttn("S295");
            foreach (BcPointM value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchDevArea()
        {
            IList<BcPointM> values = dao.SearchDevArea("PA","0002");
            foreach (BcPointM value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchVoiceSource()
        {
            IList<BcPointM> values = dao.Search("S295", "GM");
            foreach (BcPointM value in values)
            {
                PrintResult(value);
            }
        }
    }
}
