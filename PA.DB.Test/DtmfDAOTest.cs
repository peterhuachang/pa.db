﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DtmfDAOTest : ConnectionTest
    {
        private DtmfDAO dao;

        public DtmfDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DtmfDAO>("Dtmf");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Dtmf value)
        {
            Console.WriteLine(value.SttnId + " " + value.AdswId + " " + value.DtmfId + " " + value.LSttnId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void SearchPK()
        {
            Dtmf value = dao.SearchPK("S295", "0001", "0003", "S295");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void SearchSttn()
        {
            IList<Dtmf> values = dao.SearchSttn("S295");
            foreach (Dtmf value in values)
            {
                PrintResult(value);
            }
        }
    }
}
