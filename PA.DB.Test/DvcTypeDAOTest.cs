﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DvcTypeDAOTest : ConnectionTest
    {
        private DvcTypeDAO dao;

        public DvcTypeDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DvcTypeDAO>("DvcType");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(DvcType value)
        {
            Console.WriteLine(value.TypeName + " " + value.Cdesc + " " + value.Edesc + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<DvcType> values = dao.SearchAll();
            foreach (DvcType value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            DvcType value = dao.SearchPK("CD");
            PrintResult(value);
        }
    }
}
