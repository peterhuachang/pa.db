﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VDvcStatusDaoTest : ConnectionTest
    {
        private VDvcStatusDAO dao;

        public VDvcStatusDaoTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VDvcStatusDAO>("VDvcStatus");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(VDvcStatus value)
        {
            Console.WriteLine(value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.Labeling + " " + value.Cdesc + " " + value.Edesc + " " + value.IpAddr + " " + value.PmcGrpNum + " " + value.PmcPntNum + " " + value.LocId + " " + value.MuId + " " + value.AlarmId + " " + value.AlarmFlag + " " +value.StatusCname+" "+value.StatusEname+" "+ value.AlarmTime);
        }

        [NUnit.Framework.Test]
        public void TestSearchDvcGup()
        {
            IList<VDvcStatus> values = dao.SearchGrp("S2 5", "0108");
            foreach (VDvcStatus value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchDvc()
        {
            IList<VDvcStatus> values = dao.SearchBySttn("S205");
            foreach (VDvcStatus value in values)
            {
                PrintResult(value);
            }
        }

         [NUnit.Framework.Test]
        public void TestSearchSL()
        {
            IList<VDvcStatus> values = dao.SearchSttnDevType("S205","SL");
            foreach (VDvcStatus value in values)
            {
                PrintResult(value);
            }
        }

         [NUnit.Framework.Test]
         public void TestSearchSP()
         {
             IList<VDvcStatus> values = dao.SearchSttnDevType("S295", "SP");
             foreach (VDvcStatus value in values)
             {
                 PrintResult(value);
             }
         }
    }
}