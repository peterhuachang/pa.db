﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ParamValDAOTest : ConnectionTest
    {
        private ParamValDAO dao;

        public ParamValDAOTest()
        {
            this.dao = this.session.CreateDao<ParamValDAO>("ParamVal");
        }

        public static void PrintResult(ParamVal value)
        {
            Console.Write(value.Id + " " + value.SttnId + " " + value.ParamType + " " + value.ParamId + " " + value.ValueName + " " + value.Cdesc + " " + value.Edesc + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
            Console.WriteLine();
        }

        [NUnit.Framework.Test]
        public void TestSearchParamTypeId()
        {
            ParamVal value = dao.SearchParamTypeId("ACK_TIME", "AS");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            ParamVal value = dao.SearchPK(1);
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK1()
        {
            ParamVal value = dao.SearchPK("S295","CC_GM","0001");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<ParamVal> values = dao.SearchSttn("S295");
            foreach (ParamVal value in values)
            {
                PrintResult(value);
            }
        }
    }
}
