﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class SttnDAOTest : ConnectionTest
    {
        private SttnDAO dao;
        private Timer timer;

        // protected PASession session;

        public SttnDAOTest()
        {
            this.timer = null;
            this.dao = this.session.CreateDao<SttnDAO>("Sttn");
        }

        public static void PrintResult(Sttn value)
        {
            Console.Write(value.SttnId + " " + value.Cname + " " + value.Ename + " " + value.Type + " " + value.TakeroverFlag + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            IList<Sttn> values = dao.Search();
            foreach (Sttn value in values)
            {
                PrintResult(value);
            }
        }

        //[NUnit.Framework.Test]
        public void TestSearchPK(object state)
        {
            Sttn value = dao.SearchPK("S205");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        private void TimerTest()
        {
            this.timer = new Timer(new System.Threading.TimerCallback(TestSearchPK), new object(), 1, 100);
        }
    }
}
