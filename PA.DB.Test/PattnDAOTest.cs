﻿using PA.DB.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class PattnDAOTest : ConnectionTest
    {
        private PattnDAO dao;

        // protected PASession session;

        public PattnDAOTest()
        {
            this.dao = this.session.CreateDao<PattnDAO>("Pattn");
        }


        [NUnit.Framework.Test]
        public void TestSearchAll()
        {
            List<Pattn> ad = new List<Pattn>();
            ad.AddRange(dao.SearchAll());
            foreach (Pattn p in ad)
            {

                Console.Write(p.Id + " " + p.SttnId + " " + p.PattnId + " " + p.Ename + " " + p.Cname + " " + p.ColorDef + " " + p.CrUser + " " + p.CrDate + " " + p.Userstamp + " " + p.Datestamp);
                Console.WriteLine();
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchID()
        {
            Pattn p = dao.SearchById(1);
            Console.Write(p.Id + " " + p.SttnId + " " + p.PattnId + " " + p.Ename + " " + p.Cname + " " + p.ColorDef + " " + p.CrUser + " " + p.CrDate + " " + p.Userstamp + " " + p.Datestamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPattn()
        {
            Pattn p = dao.SearchPattn("S295","0001");
            Console.Write(p.Id + " " + p.SttnId + " " + p.PattnId + " " + p.Ename + " " + p.Cname + " " + p.ColorDef + " " + p.CrUser + " " + p.CrDate + " " + p.Userstamp + " " + p.Datestamp);
        }
    }
}
