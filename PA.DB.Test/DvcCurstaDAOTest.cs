﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class DvcCurstaDAOTest : ConnectionTest
    {
        private DvcCurstaDAO dao;

        public DvcCurstaDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<DvcCurstaDAO>("DvcCursta");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(DvcCursta value)
        {
            Console.WriteLine(value.SttnId + " " + value.DvcType + " " + value.DvcId + " " + value.AlarmId + " " + value.AlarmFlag + " " + value.AlarmTime + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            DvcCursta value = dao.SearchByPK("S295", "AF", "0001", "50");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<DvcCursta> values = dao.SearchSttn("S295");
            foreach (DvcCursta value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchDvcType()
        {
            IList<DvcCursta> values = dao.Search("S295", "AF");
            foreach (DvcCursta value in values)
            {
                PrintResult(value);
            }
        }
    }
}
