﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class ScdDAOTest : ConnectionTest
    {
        private ScdDAO dao;

        public ScdDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<ScdDAO>("Scd");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public static void PrintResult(Scd value)
        {
            Console.WriteLine(value.SttnId + " " + value.ScId + " " + value.BitId + " " + value.BitType + " " + value.DvcType + " " + value.DvcId + " " + value.PointId + " " + value.CrUser + " " + value.CrDate + " " + value.UserStamp + " " + value.DateStamp);
        }

        [NUnit.Framework.Test]
        public void TestSearchPK()
        {
            Scd value = dao.SearchPK("S295", "0001", 1, "I");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<Scd> values = dao.SearchSttn("S295");
            foreach (Scd value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchScId()
        {
            IList<Scd> values = dao.Search("S295", "0001");
            foreach (Scd value in values)
            {
                PrintResult(value);
            }
        }
    }
}
