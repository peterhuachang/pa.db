﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PA.DB.DAO
{
    [NUnit.Framework.TestFixture]
    public class VBcAreaDAOTest : ConnectionTest
    {
        private VBcAreaDAO dao;

        // protected PASession session;

        public VBcAreaDAOTest()
        {
            try
            {
                this.dao = this.session.CreateDao<VBcAreaDAO>("VBcArea");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void PrintResult(VBcArea value)
        {
            Console.WriteLine(value.PointId + " " + value.SttnId + " " + value.BcAreaSttn + " " + value.BcAreaId + " " + value.AreaCname + " " + value.AreaEname + " " + value.VoiceSource + " " + value.PlatForm);
        }

        [NUnit.Framework.Test]
        public void TestSearchSttn()
        {
            IList<VBcArea> values = dao.SearchBySttn("S295");
            foreach (VBcArea value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchPkey()
        {
            VBcArea value = dao.SearchPK("10", "S295", "0201");
            PrintResult(value);
        }

        [NUnit.Framework.Test]
        public void TestSearchAreaId()
        {
            IList<VBcArea> values = dao.Search("S295", "S295", "0201");
            foreach (VBcArea value in values)
            {
                PrintResult(value);
            }
        }

        [NUnit.Framework.Test]
        public void TestSearchByPointId()
        {
            IList<VBcArea> values = dao.SearchByPointId("S295", "S295", "11");
            foreach (VBcArea value in values)
            {
                PrintResult(value);
            }
        }
    }
}
